---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Corrixir varios problemas de busca relacionados coa hora de modificación
- Iter de PostingDB: Non asegurarse de que MDB_NOTFOUND é verdadeiro
- Balooctl status: evitar mostrar «Indexación de contidos» sobre cartafoles
- StatusCommand: Mostrar o estado correcto dos cartafoles
- SearchStore: Xestionar correctamente valores de termo baleiros (fallo 356176)

### Iconas de Breeze

- actualizacións e engadidos de iconas
- Iconas de estado de 22 px de tamaño tamén para 32 px xa que se necesitan na área de notificacións
- Cambiouse o valor de fixo a adaptábel nos cartafoles de 32 px de «Breeze escuro»

### Módulos adicionais de CMake

- Facer global o módulo de CMake de KAppTemplate
- Silenciar os avisos de CMP0063 con KDECompilerSettings
- ECMQtDeclareLoggingCategory: Incluír &lt;QDebug&gt; co ficheiro xerado
- Corrixir avisos CMP0054

### KActivities

- Simplificouse a carga de QML para KCM (fallo 356832)
- Solución temporal para o fallo de SQL de Qt que non limpa as conexións correctamente (fallo 348194)
- Fusionouse un complemento que executa aplicacións ao cambiar o estado da actividade
- Migrar de KService a KPluginLoader
- Migrar os complementos ao uso de kcoreaddons_desktop_to_json()

### KBookmarks

- Inicializar completamente o valor de devolución de DynMenuInfo

### KCMUtils

- KPluginSelector::addPlugins: corrixir a aserción de se o parámetro «config» é o predeterminado (fallo 352471)

### KCodecs

- Evitar desbordar deliberadamente un búfer completo

### KConfig

- Asegurarse de que o grupo se desescapa correctamente en kconf_update

### KCoreAddons

- Engadir KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)
- Engadir KPluginMetaData::copyrightText(), extraInformation() e otherContributors()
- Engadir KPluginMetaData::translators() e KAboutPerson::fromJson()
- Corrixir use-after-free no analizador de ficheiros de escritorio
- Permitir construír KPluginMetaData a partir dunha ruta JSON
- desktoptojson: facer que a falta dun ficheiro de tipo de servizo sexa un erro para o binario
- converter nun erro chamar a kcoreaddons_add_plugin sen SOURCES

### KDBusAddons

- Adaptar ao dbus-in-secondary-thread de Qt 5.6

### KDeclarative

- [DragArea] Engadir a propiedade dragActive
- [KQuickControlsAddons MimeDatabase] Expoñer o comentario de QMimeType

### KDED

- kded: adaptar ao D-Bus con fíos de Qt 5.6: o messageFilter debe causar a carga de módulos no fío principal

### Compatibilidade coa versión 4 de KDELibs

- kdelibs4support require kded (para kdedmodule.desktop)
- Corrixir un aviso de CMP0064 definindo a política CMP0054 como NEW (nova)
- Non exportar símbolos que tamén existen en KWidgetsAddons

### KDESU

- Non deixar fd en memoria ao crear un sócket

### KHTML

- Windows: retirar a dependencia de kdewin

### KI18n

- Documentar a regra do primeiro argumento para plurais en QML
- Reducir os cambios de tipo non desexados
- Permitir usar valores «double» como índice para as chamadas i18np*() en QML

### KIO

- Corrixir kiod de cara ao D-Bus con fíos de Qt 5.6: messageFilter ten que agardar ata que se cargue o módulo para devolver
- Cambiar o código de erro ao pegar ou mover dentro dun subdirectorio
- Corrixir un problema de bloqueo de emptyTrash
- Corrixir o botón incorrecto en KUrlNavigator para URL remotos
- KUrlComboBox: corrixir a devolución dunha ruta absoluta desde urls()
- kiod: desactivar a xestión de sesións
- Engadir completado automático da entrada «.» que ofrece todos os ficheiros e cartafoles agochados* (fallo 354981)
- ktelnetservice: corrixir a comprobación de que en argc hai un desprazamento de un, parche de Steven Bromley

### KNotification

- [Notificar mediante xanela emerxente] Enviar tamén o identificador do evento
- Definir un motivo predeterminado non baleiro para a inhibición do salvapantallas (fallo 334525)
- Engadir un consello para saltar o agrupamento de notificacións (fallo 356653)

### KNotifyConfig

- [KNotifyConfigWidget] Permitir seleccionar un evento concreto

### Infraestrutura de paquetes

- Permitir fornecer os metadatos en JSON

### KPeople

- Corrixir unha posíbel eliminación dupla en DeclarativePersonData

### KTextEditor

- Realce de sintaxe para pli: engadíronse funcións incluídas e rexións despregábeis

### Infraestrutura de KWallet

- kwalletd: Corrixir unha fuga de FILE*

### KWindowSystem

- Engadir unha variante de xcb para métodos estáticos KStartupInfo::sendFoo

### NetworkManagerQt

- facer que funcione con versións anteriores de NM

### Infraestrutura de Plasma

- [ToolButtonStyle] Indicar sempre activeFocus
- Usar a marca SkipGrouping para a notificación «eliminouse un trebello» (fallo 356653)
- Xestionar correctamente as ligazóns simbólicas nas rutas de paquetes
- Engadir HiddenStatus para que os plasmoides poidan agocharse
- Deixar de redirixir xanelas cando o elemento está desactivado ou agochado. (fallo 356938)
- Non emitir statusChanged se non cambiou
- Corrixir os identificadores de elementos para orientación oriental
- Contedor: non emitir appletCreated cun miniaplicativo nulo (fallo 356428)
- [Containment Interface] Corrixir un desprazamento errático de alta precisión
- Ler a propiedade X-Plasma-ComponentTypes de KPluginMetada como unha lista de cadeas
- [Miniaturas de xanela] Non quebrar se a composición está desactivada
- Permitir que os contedores sobrepoñan CompactApplet.qml

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
