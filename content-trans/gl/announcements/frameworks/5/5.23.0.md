---
aliases:
- ../../kde-frameworks-5.23.0
date: 2016-06-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Facer posíbel de verdade determinar os fornecedores a partir do URL recibido
- Fornecer asistentes de QDebug para algunhas clases de Attica
- Corrixir a redirección de URL absolutos (fallo 354748)

### Baloo

- Corrixir o uso de espazos no escravo de KIO de etiquetas (fallo 349118)

### Iconas de Breeze

- Engadir unha opción de CMake para construír un recurso binario de Qt a partir do directorio das iconas
- Moitas iconas novas ou actualizadas
- actualizar a icona de desconectar da rede para que se diferencie máis da de estar conectado (fallo 353369)
- actualizar as iconas de montar e desmontar (fallo 358925)
- engadir algúns avatares de plasma-desktop/kcms/useraccount/pics/sources
- retirar a icona de Chromium porque a icona predeterminada de Chromium axústase ben (fallo 363595)
- aclarar as iconas de Konsole (fallo 355697)
- engadir iconas de correo para Thunderbird (fallo 357334)
- engadir unha icona de chave pública (fallo 361366)
- retirar process-working-kde porque deberían usarse as iconas de Konqueror (fallo 360304)
- actualizar as iconas de Krusader segundo (fallo 359863)
- cambiar de nome as iconas dos micrófonos segundo D1291 (fallo D1291)
- engadir algunhas iconas de tipo MIME de script (fallo 363040)
- engadir funcionalidade de activar e desactivar teclados virtuais e áreas táctiles nas OSD

### Integración de infraestruturas

- Retirar dependencias innecesarias e xestión de traducións

### KActivities

- Engadir a propiedade runningActivities ao Consumer

### Ferramentas de Doxygen de KDE

- Cambios internos significativos da xeración da documentación da API

### KCMUtils

- Usar QQuickWidget para os KCM de QML (fallo 359124)

### KConfig

- Evitar omitir a comprobación de KAuthorized

### KConfigWidgets

- Permitir usar a sintaxe do novo estilo de connect con KStandardAction::create()

### KCoreAddons

- Imprimir o complemento que falle ao notificar un aviso de reinterpretación
- [kshareddatacache] Corrixir un uso incorrecto de &amp; para evitar lecturas non aliñadas
- Kdelibs4ConfigMigrator: non analizar de novo se non se migrou nada
- krandom: engadir un caso de proba para detectar o fallo 362161 (fallo de semente automática)

### KCrash

- Comprobar o tamaño da ruta de sócket de dominio de UNIX antes de copiar nela

### KDeclarative

- Permitir o estado seleccionado
- Agora a importación de KCMShell pode usarse para consultar se se permite abrir un KCM

### Compatibilidade coa versión 4 de KDELibs

- Avisar de que KDateTimeParser::parseDateUnicode non está codificado
- K4TimeZoneWidget: ruta correcta para as imaxes de bandeiras

### KDocTools

- Engadir entidades usadas habitualmente para teclas a en/user.entities
- Actualizar o modelo de man-docbook
- Actualizar o modelo de libro, o modelo de manual e o modelo de engadir arcticle
- Só chamar a kdoctools_create_handbook para index.docbook (fallo 357428)

### KEmoticons

- Engadir compatibilidade con emojis a KEmoticon e iconas de Emoji One
- Engadir a posibilidade de tamaños de iconas personalizados

### KHTML

- Corrixir unha fuga de memoria potencial da que informou Coverity e simplificar o código
- O número de valores separados por comas da propiedade «background-image» determina o número de capas
- Corrixir a análise de background-position na declaración do atallo
- Non crear unha fontFace nova se non hai unha fonte válida

### KIconThemes

- Non facer que KIconThemes dependa de Oxygen (fallo 360664)
- Concepto de estado de seleccionado para as iconas
- Usar as cores do sistema para iconas monocroma

### KInit

- Corrixir unha condición de carreira na que o ficheiro que contén a cookie de X11 ten permisos incorrectos durante un anaco
- Corrixir os permisos de /tmp/xauth-xxx-_y

### KIO

- Dar mensaxes de erro máis claras cando se da un URL sen esquema a KRun(URL) (fallo 363337)
- Engadir KProtocolInfo::archiveMimetypes()
- usar o modo de iconas seleccionado na barra lateral do diálogo de abrir un ficheiro
- kshorturifilter: corrixir unha regresión pola que non se engadía mailto: como prefixo cando non había ningún cliente de correo instalado

### KJobWidgets

- Definir a marca «dialog» correcta para o diálogo de trebello de progreso

### KNewStuff

- Non inicializar KNS3::DownloadManager coas categorías incorrectas
- Estender a API pública de KNS3::Entry

### KNotification

- usar QUrl::fromUserInput para construír un URL de son (fallo 337276)

### KNotifyConfig

- usar QUrl::fromUserInput para construír un URL de son (fallo 337276)

### KService

- Corrixir as aplicacións asociadas con tipos MIME con caracteres en maiúsculas
- Converter a minúsculas a clave de busca de tipos MIME, para que non se distingan maiúsculas de minúsculas
- Corrixir as notificacións de ksycoca cando a base de datos xa non existe

### KTextEditor

- Corrixir a codificación predeterminada a UTF-8 (fallo 362604)
- Corrixir as posibilidades de configuración de cores do estilo predeterminado «Erro»
- Buscar e substituír: corrixir a substitución de cor de fondo (regresión introducida na versión 5.22) (fallo 363441)
- O novo esquema de cores «Breeze Escuro», consultar https://kate-editor.org/?post=3745
- KateUndoManager::setUndoRedoCursorOfLastGroup(): pasar Cursor como referencia constante
- sql-postgresql.xml mellorar o realce de sintaxe ignorando os corpos de función de varias liñas
- Engadir salientado de sintaxe para Elixir e Kotlin
- Realce de sintaxe de VHDL en ktexteditor: engadir compatibilidade con funcións dentro das declaracións da arquitectura
- vimode: non quebrar cando se fornece un intervalo dunha orde que non existe (fallo 360418)
- Retirar correctamente os caracteres compostos ao usar configuracións rexionais índicas

### KUnitConversion

- Corrixir a descarga de tipos de cambio de divisa (fallo 345750)

### Infraestrutura de KWallet

- Migración de KWalletd: corrixir a xestión de erros, evita que a migración ocorra en cada arranque.

### KWayland

- [cliente] Non comprobar a versión de recurso de PlasmaWindow
- Introducir un evento de estado inicial no protocolo de xanela de Plasma
- [servidor] Causar un erro se unha solicitude transitoria intenta usarse a si mesma como pai
- [servidor] Xestionar correctamente o caso no que unha PlasmaWindow non está asociada antes de asociarlle un cliente
- [servidor] Xestionar correctamente o destrutor de SlideInterface
- Engadir a posibilidade de eventos táctiles no protocolo e a interface de entrada falsa
- [servidor] Estandarizar a xestión da solicitude de destrutor dos recursos
- Codificar as interfaces wl_text_input e zwp_text_input_v2
- [servidor] Evitar unha eliminación dupla dos recursos da retrochamada en SurfaceInterface
- [servidor] Engadir unha comprobación de punteiro nulo de recurso a ShellSurfaceInterface
- [servidor] Comparar ClientConnection en vez de wl_client en SeatInterface
- [servidor] Mellorar a xestión cando os clientes desconectan
- server/plasmawindowmanagement_interface.cpp - corrixir o aviso de -Wreorder
- [cliente] Engadir un punteiro de contexto a connects en PlasmaWindowModel
- Moitas correccións relacionadas coa destrución

### KWidgetsAddons

- Usar o efecto de icona seleccionada para a páxina KPageView actual

### KWindowSystem

- [XCB de plataforma] Respectar o tamaño de icona da solicitude (fallo 362324)

### KXMLGUI

- Facer clic dereito na barra de menú dunha aplicación non volverá deixar saltar

### NetworkManagerQt

- Reverter «retirar a compatibilidade con WiMAX en NM ≥ 1.2.0» porque rompe a ABI

### Iconas de Oxygen

- Sincronizar as iconas meteorolóxicas con Breeze
- Engadir iconas actualizadas

### Infraestrutura de Plasma

- Engadir compatibilidade de Cantata coa área de notificacións (fallo 363784)
- Estado seleccionado para Plasma::Svg e IconItem
- DaysModel: restabelecer m_agendaNeedsUpdate cando o complemento envía novos eventos
- Actualizar as iconas de son e rede para obter un mellor contraste (fallo 356082)
- Marcar downloadPath(const QString &amp;ficheiro) como obsoleto en favor de downloadPath()
- [miniatura de icona] Solicitude do tamaño de icona preferido (fallo 362324)
- Agora os plasmoides saben se os trebellos están bloqueados polas restricións do usuario ou do sistema
- [ContainmentInterface] Non intentar mostrar un QMenu baleiro
- Usar SAX para o substituto de folla de estilos de Plasma::Svg
- [DialogShadows] Gardar en caché o acceso a QX11Info::display()
- restaurar as iconas do tema Air de Plasma de KDE4
- Cargar de novo o esquema de cores seleccionado cando as cores cambien

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
