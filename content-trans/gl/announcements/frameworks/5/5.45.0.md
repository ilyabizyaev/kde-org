---
aliases:
- ../../kde-frameworks-5.45.0
date: 2018-04-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Definir explicitamente o tipo de contido como datos de formulario

### Baloo

- Simplificar os termos operator&amp;&amp; e ||
- Non obter o identificador de documento das entradas de resultado saltadas
- Non obter mtime da base de datos repetidamente ao ordenar
- Non exportar databasesanitizer de maneira predeterminada
- baloodb: Engadir unha mensaxe experimental
- Introducir a ferramenta de interface de liña de ordes baloodb
- Introducir unha clase desinfectante
- [FileIndexerConfig] Atrasar o completado dos cartafoles ata que se usen de verdade
- src/kioslaves/search/CMakeLists.txt - ligar con Qt5Network tras os cambios en KIO
- balooctl: checkDb tamén debería verificar o último URL coñecido do documentId
- balooctl monitor: Continuar para agardar polo servizo

### Iconas de Breeze

- engadir a icona window-pin (fallo 385170)
- cambiar o nome das iconas de 64 px que se engadiron para Elisa
- cambiar as iconas de 32 px para reprodución aleatoria e repetición
- Iconas que faltaban en mensaxes entre liñas (fallo 392391)
- Nova icona para o reprodutor de música Elisa
- Engadir iconas de estado de son e vídeo
- Retirar o marco arredor de iconas de accións de reprodución
- engadir iconas de media-playlist-append e play
- engadir view-media-album-cover para Babe

### Módulos adicionais de CMake

- Facer uso da infraestrutura raíz de CMake para detectar a cadea de ferramentas do compilador
- Documentación da API: corrixir que algunhas liñas de bloque de código tivesen liñas baleiras antes e despois
- Engadir ECMSetupQtPluginMacroNames
- Fornecer androiddeployqt con todas as rutas de prefixos
- Incluír «stdcpp-path» no ficheiro JSON
- Resolver ligazóns simbólicas en rutas de importación de QML
- Fornecer rutas de importación de QML a androiddeployqt

### Integración de infraestruturas

- kpackage-install-handlers/kns/CMakeLists.txt - ligar con Qt::Xml tras cambios en knewstuff

### KActivitiesStats

- Non asumir que SQLite funciona e non terminar ante erros

### Ferramentas de Doxygen de KDE

- Buscar primeiro qhelpgenerator-qt5 para a xeración de axuda

### KArchive

- karchive, kzip: intentar xestionar ficheiros duplicados un pouco mellor
- Usar nullptr para pasar un punteiro nulo a crc32

### KCMUtils

- Permitir solicitar programaticamente o módulo de configuración dun complemento
- Usar X-KDE-ServiceTypes de maneira consistente en vez de ServiceTypes
- Engadir X-KDE-OnlyShowOnQtPlatforms á definición de tipo de servidor de KCModule

### KCoreAddons

- KTextToHTML: volver cando url estea baleiro
- Limpar m_inotify_wd_to_entry antes de invalidar os punteiros de Entry (fallo 390214)

### KDeclarative

- Preparar QQmlEngine só unha vez en QmlObject

### KDED

- Engadir X-KDE-OnlyShowOnQtPlatforms á definición de tipo de servidor de KDEDModule

### KDocTools

- Engadir entidades para Elisa, Markdown, KParts, DOT e SVG a general.entities
- customization/ru: Corrixir a tradución de underCCBYSA4.docbook e underFDL.docbook
- Corrixir duplicados de lgpl-notice, gpl-notice e fdl-notice
- customization/ru: Traducir fdl-notice.docbook
- cambiar como se escribe KWave a petición do mantedor

### KFileMetaData

- taglibextractor: facer cambios internos para mellorar a lexibilidade

### KGlobalAccel

- Non usar unha aserción para validar se se usa correctamente desde D-Bus (fallo 389375)

### KHolidays

- holidays/plan2/holiday_in_en-gb - actualizar o ficheiro de festivos de India (fallo 392503)
- Este paquete non se actualizou. Pode que sexa un problema co script
- Modificáronse os ficheiros de vacacións de Alemaña (fallo 373686)
- Formatar o README.md como espera a ferramenta (cunha sección introdutoria)

### KHTML

- evitar solicitar un protocolo baleiro

### KI18n

- Asegurarse de que ki18n pode construír as súas propias traducións
- Non chamar a PythonInterp.cmake en KF5I18NMacros
- Permitir xerar ficheiros PO en paralelo
- Crear un construtor para KLocalizedStringPrivate

### KIconThemes

- Precisar o comentario de exportación de KIconEngine
- Evitar un erro de tempo de execución de asan

### KInit

- Eliminar que IdleSlave teña autorización temporal

### KIO

- Asegurarse de que o modelo se define cando se chama resetResizing
- pwd.h non existe en Windows
- Retirar de maneira predeterminada as entradas «Gardado este mes» e «Gardado o mes pasado»
- Facer que KIO se constrúa en Android
- Desactivar temporalmente a instalación do asistente e a política de KAuth de ioslave de ficheiro
- Xestionar os diálogos de confirmación de operación privilexiada en SlaveBase en vez de en KIO::Job
- Mellorar a consistencia da interface de usuario de «Abrir con» mostrando sempre a aplicación superior entre liñas
- Corrixir a quebra cando un dispositivo indica que está listo para lectura tras rematar o traballo
- Realzar os elementos seleccionados ao mostrar o cartafol superior desde o diálogo de abrir e gardar (fallo 392330)
- Engadir compatibilidade con ficheiros agochados de NTFS
- Usar X-KDE-ServiceTypes de maneira consistente en vez de ServiceTypes
- Corrixir a aserción en concatPaths ao pegar unha ruta completa no editor de liña de KFileWidget
- [KPropertiesDialog] Permitir o separador de suma de comprobación para calquera ruta local (fallo 392100)
- [KFilePlacesView] Chamar a KDiskFreeSpaceInfo só cando sexa necesario
- FileUndoManager: non eliminar ficheiros locais non existentes
- [KProtocolInfoFactory] Non baleirar a caché se acaba de construírse
- Tampouco intentar atopar unha icona para un URL relativo (p. ex. «~»)
- Usar o URL de elemento correcto para o menú contextual «Crear un novo» (fallo 387387)
- Corrixir máis casos de parámetro incorrecto para findProtocol
- KUrlCompletion: volver anticipadamente se o URL é incorrecto como «:/»
- Non intentar atopar unha icona para un URL baleiro

### Kirigami

- Iconas máis grandes no modo móbil
- Forzar un tamaño de contido no elemento de estilo de fondo
- Engadir o tipo InlineMessage e a páxina de exemplo da aplicación Gallery
- mellor coloración selectiva baseada en heurísticas
- facer que funcione a carga de SVG locais
- permitir tamén o método de carga de iconas de Android
- usar unha estratexia de coloración similar aos estilos distintos anteriores
- [Tarxeta] Usar a versión propia de «findIndex»
- matar as transferencias de rede se cambiamos de icona durante a execución
- primeiro prototipo de reciclador delegado
- Permitir aos clientes de OverlaySheet omitir o botón de pechar incluído
- Compoñentes de tarxetas
- Corrixir o tamaño de ActionButton
- Facer que as passiveNotifications duren máis para que os usuarios poidan lelas
- Retirar a dependencias innecesaria de QQC1
- Disposición de ToolbarApplicationHeader
- Permitir mostrar o título a pesar de ter accións contextuais

### KNewStuff

- Votar de verdade ao premer estrelas na vista de lista (fallo 391112)

### Infraestrutura KPackage

- Intentar corrixir a construción en FreeBSD
- Usar Qt5::rcc en vez de buscar o executábel
- Usar NO_DEFAULT_PATH para asegurarse de que se colle a orde correcta
- Buscar tamén executábeis de rcc con prefixo
- definir o compoñente para a xeración correcta de qrc
- Corrixir a xeración de paquetes binarios de rcc
- Xerar o ficheiro rcc cada vez, en tempo de instalación
- Facer que os compoñentes org.kde. inclúan un URL de doazón
- Desmarcar kpackage_install_package como obsoleto para plasma_install_package

### KPeople

- Expoñer PersonData::phoneNumber a QML

### Kross

- Non hai por que depender de kdoctools

### KService

- Documentación da API: usar X-KDE-ServiceTypes de maneira consistente en vez de ServiceTypes

### KTextEditor

- Permitir construír KTextEditor co gcc 4.9 do NDK de Android
- evitar un erro de tempo de execución de Asan: o expoñente de movemento -1 é negativo
- optimización de TextLineData::attribute
- Non calcular attribute() dúas veces
- Reverter «Corrección: a vista salta cando se activa o desprazamento alén da fin do documento» (fallo 391838)
- non ensuciar o historial do portapapeis con duplicados

### KWayland

- Engadir unha interface de acceso remoto a KWayland
- [servidor] Engadir compatibilidade coa semántica de marco da versión 5 de Pointer (fallo 389189)

### KWidgetsAddons

- KColorButtonTest: retirar código cunha tarefa pendente
- ktooltipwidget: restar as marxes do tamaño dispoñíbel
- [KAcceleratorManager] Só definir iconText() se cambiou de verdade (fallo 391002)
- ktooltipwidget: Evitar mostrar fóra da pantalla
- KCapacityBar: definir o estado QStyle::State_Horizontal
- Sincronizar con cambios en KColorScheme
- ktooltipwidget: corrixir a colocación de consellos (fallo 388583)

### KWindowSystem

- Engadir «SkipSwitcher» á API
- [xcb] Corrixir o código de _NET_WM_FULLSCREEN_MONITORS (fallo 391960)
- Reducir o tempo de conxelación de plasmashell

### ModemManagerQt

- cmake: non marcar libnm-util como atopado cando se atopa ModemManager

### NetworkManagerQt

- Exportar os directorios de inclusión de NetworkManager
- Comezar a requirir NM 1.0.0
- dispositivo: definir StateChangeReason e MeteredStatus como Q_ENUM
- Corrixir a conversión de marcas de AccessPoint en capacidades

### Infraestrutura de Plasma

- Modelos de fondo de escritorio: definir a cor de fondo para asegurar o contraste co contido do texto de exemplo
- Engadir un modelo para fondo de escritorio de Plasma con extensión de QML
- [ToolTipArea] Engadir o sinal «aboutToShow»
- windowthumbnail: Usar cambio de escala que respecte a gamma
- windowthumbnail: usar o filtro de textura de mapas MIP (fallo 390457)
- Retirar entradas X-Plasma-RemoteLocation sen usar
- Modelos: retirar un X-Plasma-DefaultSize que non se usa dos metadatos do miniaplicativo
- Usar X-KDE-ServiceTypes de maneira consistente en vez de ServiceTypes
- Modelos: retirar entradas de X-Plasma-Requires-* sen usar dos metadatos de miniaplicativo
- retirar as áncoras dos elementos que estean nunha disposición
- Reducir o tempo de conxelación de plasmashell
- cargar previamente só tras emitir o contedor uiReadyChanged
- Corrixir unha lista despregábel (fallo 392026)
- Corrixir o cambio de escala de texto con factores de escala non enteiros cando se define PLASMA_USE_QT_SCALING=1 (fallo 356446)
- novas iconas para dispositivos desconectados e desactivados
- [Diálogo] Permitir definir outputOnly para o diálogo NoBackground
- [ToolTip] Comprobar o nome de ficheiro no xestor de KDirWatch
- Desactivar o aviso de obsolescencia de kpackage_install_package de momento
- [Tema Breeze de Plasma] Aplicar currentColorFix.sh ás iconas de reprodución cambiadas
- [Tema Breeze de Plasma] Engadir iconas de estado de reprodución con círculos
- Retirar os marcos arredor de botóns de reprodución
- [Window Thumbnail] Permitir usar a textura de atlas
- [Diálogo] Retirar as chamadas a KWindowSystem::setState, agora obsoletas
- Permitir texturas Atlas en FadingNode
- Corrixir un fragmento de FadingMaterial con perfil do núcleo

### QQC2StyleBridge

- corrixir a renderización cando se desactiva
- mellor disposición
- funcionalidade experimental de pnemónicos automáticos
- Asegurarse de que temos en conta o tamaño do elemento ao aplicarlle un estilo
- Corrixir a renderización de fontes fóra de HiDPI e os factores de escala enteiros (fallo 391780)
- corrixir as cores das iconas con conxuntos de cores
- corrixir as cores das iconas para os botóns de ferramentas

### Solid

- Agora Solid pode consultar as baterías, por exemplo as de mandos de xogo sen fíos
- Usar enumeracións de UP introducidas recentemente
- engadir dispositivos de entrada de xogos e outros a Battery
- Engadir unha enumeración de dispositivos de batería
- [UDevManager] Tamén buscar cámaras de maneira explícita
- [UDevManager] Filtrar xa por subsistema antes de consultar (fallo 391738)

### Sonnet

- Non obrigar a usar o cliente predeterminado, escoller un compatíbel coa linguaxe solicitada.
- Incluír as cadeas de substitución na lista de suxestións
- crear NSSpellCheckerDict::addPersonal()
- NSSpellCheckerDict::suggest() devolve unha lista de suxestións
- inicializar o idioma de NSSpellChecker no construtor de NSSpellCheckerDict
- crear a categoría de rexistro NSSpellChecker
- NSSpellChecker require AppKit
- Mover NSSpellCheckerClient::reliability() fóra da liña
- usar o código de plataforma de Mac preferido
- Usar o directorio correcto para buscar trigramas no directorio de construción de Windows

### Realce da sintaxe

- Permitir construír completamente o proxecto ao usar compilación cruzada
- Cambiar o deteño do xerador de sintaxe de CMake
- Optimizar os realces de Bash, Cisco, Clipper, Coffee, Gap, Haml e Haskell
- Engadir salientado de sintaxe para ficheiros de MIB

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
