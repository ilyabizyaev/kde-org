---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE Ships Applications 19.04.
layout: application
release: applications-19.04.0
title: KDE publica a versión 19.04.0 das aplicacións de KDE
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

The KDE community is happy to announce the release of KDE Applications 19.04.

Our community continuously works on improving the software included in our KDE Application series. Along with new features, we improve the design, usability and stability of all our utilities, games, and creativity tools. Our aim is to make your life easier by making KDE software more enjoyable to use. We hope you like all the new enhancements and bug fixes you'll find in 19.04!

## What's new in KDE Applications 19.04

Resolvéronse máis de 150 fallos. Estas correccións recuperan funcionalidades desactivadas, normalizan os atallos, e resolven quebras, facendo as aplicacións de KDE máis fáciles de usar e permitíndolle ser máis produtivo.

### Xestión de ficheiros

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> is KDE's file manager. It also connects to network services, such as SSH, FTP, and Samba servers, and comes with advanced tools to find and organize your data.

Novas funcionalidades:

+ We have expanded thumbnail support, so Dolphin can now display thumbnails for several new file types: <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a> files, <a href='https://phabricator.kde.org/D18738'>.epub and .fb2 eBook</a> files, <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a> files, and <a href='https://phabricator.kde.org/D19679'>PCX</a> files. Additionally, thumbnails for text files now show <a href='https://phabricator.kde.org/D19432'>syntax highlighting</a> for the text inside the thumbnail. </li>
+ You can now choose <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>which split view pane to close</a> when clicking the 'Close split' button. </li>
+ This version of Dolphin introduces <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>smarter tab placement</a>. When you open a folder in a new tab, the new tab will now be placed immediately to the right of the current one, instead of always at the end of the tab bar. </li>
+ <a href='https://phabricator.kde.org/D16872'>Tagging items</a> is now much more practical, as tags can be added or removed using the context menu. </li>
+ We have <a href='https://phabricator.kde.org/D18697'>improved the default sorting</a> parameters for some commonly-used folders. By default, the Downloads folder is now sorted by date with grouping enabled, and the Recent Documents view (accessible by navigating to recentdocuments:/) is sorted by date with a list view selected. </li>

Entre as correccións de fallo están:

+ When using a modern version of the SMB protocol, you can now <a href='https://phabricator.kde.org/D16299'>discover Samba shares</a> for Mac and Linux machines. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>Re-arranging items in the Places panel</a> once again works properly when some items are hidden. </li>
+ After opening a new tab in Dolphin, that new tab's view now automatically gains <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>keyboard focus</a>. </li>
+ Dolphin now warns you if you try to quit while the <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>terminal panel is open</a> with a program running in it. </li>
+ We fixed many memory leaks, improving Dolphin's overall performance.</li>

The <a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> allows other KDE applications to read audio from CDs and automatically convert it into other formats.

+ The AudioCD-KIO now supports ripping into <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ We made <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>CD info text</a> really transparent for viewing. </li>

### Edición de vídeo

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

This is a landmark version for KDE's video editor. <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> has gone through an extensive re-write of its core code as more than 60%% of its internals has changed, improving its overall architecture.

Entre as melloras están:

+ A cronoloxía reescribiuse para usar QML.
+ Cando pon un fragmento na cronoloxía, o son e o vídeo van sempre a pistas independentes.
+ Agora a cronoloxía permite navegación co teclado: os fragmentos, as composicións e os fotogramas clave poden moverse co teclado. Ademais, a altura das pistas pode axustarse.
+ In this version of Kdenlive, the in-track audio recording comes with a new <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>voice-over</a> feature.
+ Melloramos a funcionalidade de copiar e pegar: funciona entre distintas xanelas de proxecto. Tamén melloramos a xestión de fragmentos proxy, e agora os fragmentos poden eliminarse de maneira individual.
+ Coa versión 19.04 volve a compatibilidade con pantallas de monitores BlackMagic externos e tamén hai novas guías predefinidas para monitores.
+ Melloramos a xestión de fotogramas clave, dándolle unha aparencia e fluxo de traballo máis consistentes. Tamén melloramos o titulador facendo que os botóns de aliñar se axusten a zonas seguras, engadindo guías configurábeis e cores de fondo, e mostrando os elementos que faltan.
+ Corriximos o fallo da cronoloxía que descolocaba ou retiraba fragmentos, que ocorría ao mover un grupo de fragmentos.
+ Corriximos o fallo de imaxes JPG que renderizaba as imaxes como pantallas brancas en Windows. Tamén corriximos os fallos que afectaban á captura de pantalla en Windows.
+ Apart from all of the above, we have added many small usability enhancements that will make using Kdenlive easier and smoother.

### Oficina

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a> is KDE's multipurpose document viewer. Ideal for reading and annotating PDFs, it can also open ODF files (as used by LibreOffice and OpenOffice), ebooks published as ePub files, most common comic book files, PostScript files, and many more.

Entre as melloras están:

+ To help you ensure your documents are always a perfect fit, we've added <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>scaling options</a> to Okular's Print dialog.
+ Okular now supports viewing and verifying <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>digital signatures</a> in PDF files.
+ Thanks to improved cross-application integration, Okular now supports editing LaTeX documents in <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ Improved support for <a href='https://phabricator.kde.org/D18118'>touchscreen navigation</a> means you will be able to move backwards and forwards using a touchscreen when in Presentation mode.
+ Users who prefer manipulating documents from the command-line will be able to perform smart <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>text search</a> with the new command-line flag that lets you open a document and highlight all occurrences of a given piece of text.
+ Okular now properly displays links in <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>Markdown documents</a> that span more than one line.
+ The <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>trim tools</a> have fancy new icons.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>KMail</a> is KDE's privacy-protecting email client. Part of the <a href='https://kde.org/applications/office/kontact/'>Kontact groupware suite</a>, KMail supports all email systems and allows you to organize your messages into a shared virtual inbox or into separate accounts (your choice). It supports all kinds of message encryption and signing, and lets you share data such as contacts, meeting dates, and travel information with other Kontact applications.

Entre as melloras están:

+ Ademais gramática! Esta versión de KMail inclúe compatibilidade con languagetools (corrector gramatical) e grammalecte (corrector gramatical para francés).
+ Phone numbers in emails are now detected and can be dialed directly via <a href='https://community.kde.org/KDEConnect'>KDE Connect</a>.
+ KMail now has an option to <a href='https://phabricator.kde.org/D19189'>start directly in system tray</a> without opening the main window.
+ Melloramos a compatibilidade co complemento de Markdown.
+ Obter mensaxes de correo mediante IMAP xa non deixa de funcionar cando falla a autorización.
+ Tamén fixemos moitas correccións na infraestrutura de KMail, Akonadi, para mellorar a súa estabilidade e o seu rendemento.

<a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> is Kontact's calendar component, managing all your events.

+ Recurrent events from <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> are again synchronized correctly.
+ The event reminder window now remembers to <a href='https://phabricator.kde.org/D16247'>show on all desktops</a>.
+ We modernized the look of the <a href='https://phabricator.kde.org/T9420'>event views</a>.

<a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> is Kontact's brand new travel assistant that will help you get to your location and advise you on your way.

- Hai un novo extractor xenérico de tíckets RCT2 (os que usan p. ex. compañías de tren como DSB, ÖBB, SBB e NS).
- Mellorouse de maneira significativa a detección e desambiguación de nomes de aeroportos.
- Engadirmos novos extractores personalizados para fornecedores outrora incompatíbeis (p. ex. BCD Travel, NH Group), e melloramos as variacións de formato e idioma dos fornecedores xa compatíbeis (p. ex. SNCF, Easyjet, Booking.com, Hertz).

### Desenvolvemento

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> is KDE's full-featured text editor, ideal for programming thanks to features such as tabs, split-view mode, syntax highlighting, a built-in terminal panel, word completion, regular expressions search and substitution, and many more via the flexible plugin infrastructure.

Entre as melloras están:

- Agora Kate pode mostrar todos os caracteres de espazo invisíbeis, non só algúns.
- Pode activar e desactivar facilmente o axuste de liña usando a entrada de menú de seu en cada documento, sen ter que cambiar a configuración global predeterminada.
- Os menús de contexto de ficheiro e separador inclúen agora unha serie de útiles novas accións, como cambiar de nome, eliminar, abrir o cartafol contedor, copiar a ruta do ficheiro, comparar [con outro ficheiro aberto], e propiedades.
- Esta versión de Kate publícase con máis complementos activados de maneira predeterminada, incluída a popular e útil funcionalidade de terminal incrustado.
- Ao saír, Kate xa non lle pide que confirme os ficheiros que outro proceso modificou no disco, como un cambio de control de código.
- Agora a vista de árbore de complemento mostra correctamente todos os elementos de menú das entradas de Git que teñen diérese no nome.
- Ao abrir varios ficheiros usando a liña de ordes, os ficheiros ábrense en novos separadores na orde indicada na liña de ordes.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Konsole</a> is KDE's terminal emulator. It supports tabs, translucent backgrounds, split-view mode, customizable color schemes and keyboard shortcuts, directory and SSH bookmarking, and many other features.

Entre as melloras están:

+ Realizáronse moitas melloras na xestión de lapelas que lle axudarán a ser máis produtivo. Poden crearse novas lapelas facendo clic central en partes baleiras da barra de lapelas, e hai unha opción que permite pechar as lapelas co clic central. Os botóns de pechar móstranse nas lapelas de maneira predeterminada, e as iconas mostraranse só ao usar un perfil cunha icona personalizada. Por último, pero non por iso menos importante, o atallo Ctrl+Tab permite cambiar rapidamente entre a lapela actual e a anterior.
+ The Edit Profile dialog received a huge <a href='https://phabricator.kde.org/D17244'>user interface overhaul</a>.
+ Agora o esquema de cores Breeze úsase como esquema de cores predeterminado de Konsole, e melloramos o seu contraste e consistencia co tema de sistema Breeze.
+ Resolvemos os problemas ao mostrar texto en letra grosa.
+ Agora Konsole mostra correctamente o cursor de estilo barra baixa.
+ We have improved the display of <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>box and line characters</a>, as well as of <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>Emoji characters</a>.
+ Agora os atallos de cambio de perfil cambian o perfil do separador actual en vez de abrir un novo separador co outro perfil.
+ Agora os separadores inactivos que reciben notificacións e cambian a cor do texto do seu título volven restabelecer a cor do texto do título do separador á normal ao activar o separador.
+ Agora a funcionalidade de «Variar o fondo de cada separador» funciona cando a cor de fondo de base é moi escura ou negra.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> is a computer-aided translation system that focuses on productivity and quality assurance. It is targeted at software translation, but also integrates external conversion tools for translating office documents.

Entre as melloras están:

- Agora Lokalize permite ver a fonte da tradución cun editor personalizado.
- Melloramos a posición dos trebellos de doca e como se garda e restaura a configuración.
- Agora ao filtrar mensaxes presérvase a posicións nos ficheiros .po.
- Corriximos varios fallos da interface de usuario (comentarios de desenvolvedores, conmutador de expresións regulares nas substitucións masivas, número de mensaxes baleiras sen verificar, etc.).

### Utilidades

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> is an advanced image viewer and organizer with intuitive and easy-to-use editing tools.

Entre as melloras están:

+ The version of Gwenview that ships with Applications 19.04 includes full <a href='https://phabricator.kde.org/D13901'>touchscreen support</a>, with gestures for swiping, zooming, panning, and more.
+ Another enhancement added to Gwenview is full <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>High DPI support</a>, which will make images look great on high-resolution screens.
+ Improved support for <a href='https://phabricator.kde.org/D14583'>back and forward mouse buttons</a> allows you to navigate between images by pressing those buttons.
+ You can now use Gwenview to open image files created with <a href='https://krita.org/'>Krita</a> – everyone’s favorite digital painting tool.
+ Gwenview now supports large <a href='https://phabricator.kde.org/D6083'>512 px thumbnails</a>, allowing you to preview your images more easily.
+ Gwenview now uses the <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>standard Ctrl+L keyboard shortcut</a> to move focus to the URL field.
+ You can now use the <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>Filter-by-name feature</a> with the Ctrl+I shortcut, just like in Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> is Plasma's screenshot application. You can grab full desktops spanning several screens, individual screens, windows, sections of windows, or custom regions using the rectangle selection feature.

Entre as melloras están:

+ We have extended the Rectangular Region mode with a few new options. It can be configured to <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>auto-accept</a> the dragged box instead of asking you to adjust it first. There is also a new default option to remember the current <a href='https://phabricator.kde.org/D19117'>rectangular region</a> selection box, but only until the program is closed.
+ You can configure what happens when the screenshot shortcut is pressed <a href='https://phabricator.kde.org/T9855'>while Spectacle is already running</a>.
+ Spectacle allows you to change the <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>compression level</a> for lossy image formats.
+ Save settings shows you what the <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>filename of a screenshot</a> will look like. You can also easily tweak the <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>filename template</a> to your preferences by simply clicking on placeholders.
+ Spectacle xa non mostra tanto a opción «Pantalla completa (todos os monitores)» como a opción «Pantalla actual» nos computadores cunha única pantalla.
+ Agora o texto de axuda do modo de rexión rectangular móstrase no medio da pantalla principal, en vez de dividirse entre pantallas.
+ En Wayland, Spectacle só inclúe as funcionalidades que funcionan.

### Xogos e educación

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Our application series includes numerous <a href='https://games.kde.org/'>games</a> and <a href='https://edu.kde.org/'>educational applications</a>.

<a href='https://kde.org/applications/education/kmplot'>KmPlot</a> is a mathematical function plotter. It has a powerful built-in parser. The graphs can be colorized and the view is scalable, allowing you to zoom to the level you need. Users can plot different functions simultaneously and combine them to build new functions.

+ You can now zoom in by holding down Ctrl and using the <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>mouse wheel</a>.
+ This version of Kmplot introduces the <a href='https://phabricator.kde.org/D17626'>print preview</a> option.
+ Root value or (x,y) pair can now be copied to <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>clipboard</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> is a miniature golf game.

+ We have restored <a href='https://phabricator.kde.org/D16978'>sound support</a>.
+ Kolf xa non usa kdelibs4.
