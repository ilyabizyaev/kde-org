---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE Ships Applications 19.08.
layout: application
release: applications-19.08.0
title: KDE publica a versión 19.08.0 das aplicacións de KDE
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

The KDE community is happy to announce the release of KDE Applications 19.08.

Esta publicación forma parte da aposta de KDE por fornecer de maneira continuada versións melloradas dos programas que entregamos aos nosos usuarios. As novas versións das aplicacións traen máis funcionalidades e software mellor deseñado que aumenta a facilidade de uso e a estabilidade de aplicacións como Dolphin, Konsole, Kate, Okular e todo o resto das súas utilidades favoritas de KDE. O noso obxectivo é asegurar que vostede segue sendo produtivo, e que usar o software de KDE lle resulte máis fácil e satisfactorio.

We hope you enjoy all the new enhancements you'll find in 19.08!

## What's new in KDE Applications 19.08

More than 170 bugs have been resolved. These fixes re-implement disabled features, normalize shortcuts, and solve crashes, making your applications friendlier and allowing you to work and play smarter.

### Dolphin

Dolphin é o explorador de ficheiros e cartafoles de KDE que agora pode iniciar desde calquera lugar usando o novo atallo global <keycap>Meta + E</keycap>. Tamén hai unha nova funcionalidade que minimiza o desorde do seu escritorio. Cando Dolphin xa está en execución, se abre cartafoles con outras aplicacións, eses cartafoles abriranse nun novo separador da xanela existente en vez de nunha nova xanela de Dolphin. Teña en conta que este comportamento está agora activado de maneira predeterminada, pero pode desactivarse.

O panel de información (situado á dereita do panel principal de Dolphin) mellorouse en varios aspectos. Por exemplo, pode escoller reproducir automaticamente ficheiros multimedia ao realzalos no panel principal, e agora pode seleccionar e copiar o texto que se mostra no panel. Se quere cambiar a información que mostra o panel, pode facelo no propio panel, xa que Dolphin non abre unha xanela aparte cando escolle configurar o panel.

Tamén resolvemos moitas das pequenas incomodidades e fallos, asegurándonos de que a súa experiencia ao usar Dolphin é moito máis fluída en xeral.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`A nova funcionalidade de marcadores de Dolphin` caption=`A nova funcionalidade de marcadores de Dolphin` width="600px" >}}

### Gwenview

Gwenview is KDE's image viewer, and in this release the developers have improved its thumbnail viewing feature across the board. Gwenview can now use a \"Low resource usage mode\" that loads low-resolution thumbnails (when available). This new mode is much faster and more resource-efficient when loading thumbnails for JPEG images and RAW files. In cases when Gwenview cannot generate a thumbnail for an image, it now displays a placeholder image rather than re-using the thumbnail of the previous image. The problems Gwenview had with displaying thumbnails from Sony and Canon cameras have also been solved.

Ademáis dos cambios no que respecta ás miniaturas, Gwenview tamén recibiu un novo menú de «Compartir» que permite enviar imaxes a varios lugares, e carga e mostra correctamente os ficheiros de lugares remotos aos que se accede mediante KIO. A nova versión de Gwenview tamén mostra moitos máis metadatos EXIF de imaxes en cru.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Novo menú de «Compartir» de Gwenview` caption=`Novo menú de «Compartir» de Gwenview` width="600px" >}}

### Okular

Os desenvolvedores introduciron moitas melloras nas anotacións de Okular, o visor de documentos de KDE. Ademais de mellorar a interface de usuario dos diálogos de configuración das anotacións, agora as anotacións de liña poden ter varias decoracións visuais ao final, o que lles permite converterse en frechas, por exemplo. A outra cousa que pode facer coas anotacións é despregalas e pregalas todas á vez.

O compatibilidade de Okular con documentos EPub tamén recibiu un empurrón nesta versión. Okular xa non quebra ao intentar cargar ficheiros ePub en mal estado, e mellorou significativamente o seu rendemento con ficheiros ePub grandes. A lista de cambios desta publicación inclúe bordos de páxina mellorados e a ferramenta de artesán do modo de presentación en modo de PPP altos.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Configuración da ferramenta de anotacións de Okular coa nova opción de fin da liña` caption=`Configuración da ferramenta de anotacións de Okular coa nova opción de fin da liña` width="600px" >}}

### Kate

Thanks to our developers, three annoying bugs have been squashed in this version of KDE's advanced text editor. Kate once again brings its existing window to the front when asked to open a new document from another app. The \"Quick Open\" feature sorts items by most recently used, and pre-selects the top item. The third change is in the \"Recent documents\" feature, which now works when the current configuration is set up not to save individual windows’ settings.

### Konsole

O cambio máis evidente en Konsole, a aplicación de emulador de terminal de KDE, é a aceleración da funcionalidade de teselado. Agora pode dividir o panel principal como queira, tanto verticalmente como horizontalmente. Os paneis aniñados poden dividirse de novo como queira. Esta versión tamén permite arrastrar e soltar paneis, permitíndolle cambiar facilmente a disposición para adaptala ao seu fluxo de traballo.

Ademais, a xanela de configuración redeseñouse para que resulte máis clara e fácil de usar.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle é a aplicación de capturas de pantalla de KDE e con cada versión recibe máis e máis funcionalidades interesantes. Esta versión non é unha excepción, xa que agora Spectacle ven con varias funcionalidades que regulan a súa funcionalidade de demora. Ao facer unha captura de pantalla con demora, Spectacle mostrará o tempo restante no título da súa xanela, Esta información tamén pode verse no elemento do xestor de tarefas.

Still on the Delay feature, Spectacle's Task Manager button will also show a progress bar, so you can keep track of when the snap will be taken. And, finally, if you un-minimize Spectacle while waiting, you will see that the “Take a new Screenshot” button has turned into a \"Cancel\" button. This button also contains a progress bar, giving you the chance to stop the countdown.

Gardar capturas de pantalla tamén ten unha nova funcionalidade. Tras gardar unha captura de pantalla, agora Spectacle mostra unha mensaxe na aplicación que lle permite abrir a captura de pantalla ou o cartafol que a contén.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

Kontact, a colección de programas de correo electrónico, calendario, contatos e funcionalidades para grupos en xeral de KDE, trae cariñas con cores de Unicode e compatibilidade con Markdown no editor de correo electrónico. A nova versión non só lle permitirá mellorar o aspecto das súas mensaxes, senón que grazas á integración con correctores gramaticais como LanguageTool e Grammalecte axudaralle a comprobar e corrixir o seu texto.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Selector de cariñas` caption=`Selector de cariñas` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Integración de correctores gramaticais con KMail` caption=`Integración de correctores gramaticais con KMail` width="600px" >}}

Ao planificar eventos, os correos electrónicos de invitación de KMail xa non se eliminan tras responder a eles. Agora pódese mover un evento dun calendario a outro desde o editor de eventos de KOrganizer.

Por último, pero non por iso menos importante, agora KAddressBook pode enviar mensaxes SMS a contactos mediante KDE Connect, fornecendo unha integración máis cómoda dos seus dispositivos de escritorio e móbiles.

### Kdenlive

The new version of Kdenlive, KDE's video editing software, has a new set of keyboard-mouse combos that will help you become more productive. You can, for example, change the speed of a clip in the timeline by pressing CTRL and then dragging on the clip, or activate the thumbnail preview of video clips by holding Shift and moving the mouse over a clip thumbnail in the project bin. Developers have also put a lot of effort into usability by making 3-point editing operations consistent with other video editors, which you will surely appreciate if you're switching to Kdenlive from another editor.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
