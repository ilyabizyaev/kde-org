---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE выпускает KDE Applications 17.08.1
layout: application
title: KDE выпускает KDE Applications 17.08.1
version: 17.08.1
---
September 7, 2017. Today KDE released the first stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello, KDE games, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.36.
