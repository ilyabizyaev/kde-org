---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE levererar Program 15.08.0.
layout: application
release: applications-15.08.0
title: KDE levererar KDE-program 15.08.0
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin med det nya utseendet - nu baserad på KDE Ramverk 5` class="text-center" width="600px" caption=`Dolphin med det nya utseendet - nu baserad på KDE Ramverk 5`>}}

19:e augusti, 2015. Idag ger KDE ut KDE-program 15.08.

I den här utgåvan har totalt 107 program konverterats till <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Ramverk 5</a>. Gruppen strävar efter att ge skrivbordet och dessa program den bästa möjliga kvalitet. Alltså räknar vi med att du skickar oss dina kommentarer.

I den här utgåvan finns flera nytillskott till listan över KDE Ramverk 5-baserade program, inklusive <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>Kontact-sviten</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Teknisk förhandsgranskning av Kontact-sviten

Under de senaste antal månaderna har KDE:s grupp för personlig informationshantering (PIM) lagt mycket möda på att konvertera Kontact till Qt 5 och KDE Ramverk 5. Dessutom har prestanda för dataåtkomst avsevärt förbättrats genom ett optimerat kommunikationslager. KDE:s grupp för personlig informationshantering arbetar hård med att ytterligare finslipa Kontact-sviten och ser fram emot din återkoppling. För mer och detaljerad information om vad som har ändrats i KDE PIM se <a href='http://www.aegiap.eu/kdeblog/'>Laurent Montels blogg</a>.

### Kdenlive och Okular

Den här utgåvan av Kdenlive inkluderar många rättningar i Dvd-guiden, samt ett stort antal felrättningar och andra funktioner, vilket omfattar integrering av några större omstruktureringar. Mer information om ändringar i Kdenlive finns i de omfattande <a href='https://kdenlive.org/discover/15.08.0'>ändringsloggarna</a>. Och Okular stöder nu övergången Tona i presentationsläge.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku med ett teckenspel` class="text-center" width="600px" caption=`KSudoku med ett teckenspel`>}}

### Dolphin, utbildning och spel

Dolphin har konverterats väl till KDE Ramverk 5. Marble har förbättrat stöd för <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> samt bättre stöd för kommentarer, redigering och KML-överlagring.

Det har gjorts ett överraskande stort antal incheckningar i Ark, inklusive många smårättningar. Kstars har fått många incheckningar, inklusive förbättringar av den plana ADU-algoritmen och kontroll av värden utanför gränser, nu sparas meridianvändning, guidningsavvikelse och automatisk HFR-fokuseringsgräns i sekvensfilen, och stöd för teleskophastighet och återgång har lagts till. Ksudoku har just blivit bättre. Incheckningar omfattar: Tillägg av grafiskt gränssnitt och hanteringsmodul för inmatning av Mattedoku- och mördarsudokuspel, och tillägg av en ny lösningsmodul baserad på Donald Knuths algoritm Dansande Länkar (DLX).

### Övriga utgåvor

TIllsammans med den här utgåvan publiceras Plasma 4 i LTS-version för den sista gången som version 4.11.22.
