---
aliases:
- ../announce-applications-18.08-beta
date: 2018-07-20
description: KDE levererar Program 18.08 Beta.
layout: application
release: applications-18.07.80
title: KDE levererar betaversion av KDE-program 18.08
---
20:e juli, 2018. Idag ger KDE ut betautgåvan av de nya versionerna av KDE-program. Med beroenden och funktioner frysta, fokuserar KDE-grupperna nu på att rätta fel och ytterligare finputsning.

Titta i <a href='https://community.kde.org/Applications/18.08_Release_Notes'>gemenskapens versionsfakta</a> för information om komprimerade arkiv och kända problem. Ett fullständigare meddelande kommer att vara tillgängligt för den slutliga utgåvan.

Utgåva 18.08 av KDE Program behöver en omfattande utprovning för att behålla och förbättra kvaliteten och användarupplevelsen. Verkliga användare är väsentliga för att upprätthålla hög kvalitet i KDE, eftersom utvecklare helt enkelt inte kan prova varje möjlig konfiguration. Vi räknar med dig för att hjälpa oss hitta fel tidigt, så att de kan krossas innan den slutliga utgåvan. Överväg gärna att gå med i gruppen genom att installera betaversionen och <a href='https://bugs.kde.org/'>rapportera eventuella fel</a>.
