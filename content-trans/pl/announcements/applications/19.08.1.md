---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE wydało Aplikacje 19.08.1.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE wydało Aplikacje 19.08.1
version: 19.08.1
---
{{% i18n_date %}}

Dzisiaj KDE wydało pierwszą aktualizację stabilności <a href='../19.08.0'> dla  Aplikacji KDE 19.08</a> . To wydanie zawiera tylko poprawki błędów i  aktualizacje tłumaczeń, zapewniając bezpieczną i przyjemną aktualizację  dla wszystkich.

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Kdenlive, Konsole, Step, among others.

Wśród ulepszeń znajdują się:

- Several regressions in Konsole's tab handling have been fixed
- Dolphin again starts correctly when in split-view mode
- Deleting a soft body in the Step physics simulator no longer causes a crash
