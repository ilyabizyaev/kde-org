---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE wydało Aplikacje 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE wydało Aplikacje KDE 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../18.12.0'> Aplikacji KDE 18.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than twenty recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize, Umbrello, among others.

Wśród ulepszeń znajdują się:

- Loading of .tar.zstd archives in Ark has been fixed
- Dolphin no longer crashes when stopping a Plasma activity
- Switching to a different partition can no longer crash Filelight
