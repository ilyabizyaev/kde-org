---
aliases:
- ../../kde-frameworks-5.65.0
date: 2019-12-14
layout: framework
libCount: 70
qtversion: 5.12
---
Novo módulo: KQuickCharts -- um módulo em QtQuick que oferece gráficos em alta performance.

### Ícones do Brisa

- Alinhamento ao pixel do 'color-picker'
- Adição de novos ícones do Baloo
- Adição de novos ícones de pesquisa nas preferências
- Uso de um conta-gotas para os ícones 'color-picker' (erro 403924)
- Adição do ícone da categoria "todas as aplicações"

### Módulos Extra do CMake

- Limpeza do transporte do 'extra-cmake-modules' do EBN
- ECMGenerateExportHeader: adição da opção NO_BUILD_SET_DEPRECATED_WARNINGS_SINCE
- Uso explícito do 'lib' para as pastas do 'systemd'
- Adição da pasta de instalação das unidades do 'systemd'
- KDEFrameworkCompilerSettings: activar todos os avisos de objectos obsoletos do Qt &amp; KF

### Integração da Plataforma

- Definição condicional do SH_ScrollBar_LeftClickAbsolutePosition com base na definição do 'kdeglobals' (erro 379498)
- Definição do nome e da versão da aplicação na ferramenta 'knshandler'

### Ferramentas de Doxygen do KDE

- Correcção das importações de módulos com o Python3

### KAuth

- Instalação de ficheiro .pri para o KAuthCore

### KBookmarks

- Descontinuação do KonqBookmarkMenu e do KonqBookmarkContextMenu
- Migração das classes apenas usadas na KonqBookmarkMenu em conjunto com ela

### KCalendarCore

- Uso de contingência do fuso-horário do sistema na criação do calendário com um inválido
- Calendário em Memória: evitar a duplicação de código
- Uso do QDate como chave no 'mIncidencesForDate' no MemoryCalendar
- Tratamento de incidências em fusos-horários distintos no MemoryCalendar

### KCMUtils

- [KCMultiDialog] Remoção do tratamento mais especial das margens; é feito agora no KPageDialog
- KPluginSelector: uso do novo KAboutPluginDialog
- Adição de guarda para o Kirigami em falta (erro 405023)
- Desactivação do botão de reposição das predefinições se o KCModule assim o indicar
- Fazer com que o KCModuleProxy comece a tomar conta do estado predefinido
- Tornar o KCModuleQml compatível com o sinal defaulted()

### KCompletion

- Reorganização do KHistoryComboBox::insertItems

### KConfig

- Documentação da configuração das Notificações
- Só cria uma configuração da sessão quando repõe de facto uma
- kwriteconfig: adição de opção para remover
- Adição do KPropertySkeletonItem
- Preparação do KConfigSkeletonItem para permitir a herança da sua classe privada

### KConfigWidgets

- [KColorScheme] Mudança da ordem da correspondência de cores da decoração DecorationRole para um enumerado
- [KColorScheme] Correcção de erro no comentário do NShadeRoles
- [KColorScheme/KStatefulBrush] Mudança de números fixos para itens enumerados
- [KColorScheme] Adição de itens aos enumerados ColorSet e Role para o número total de itens
- Registo do KKeySequenceWidget no KConfigDialogManager
- Ajuste do KCModule para também canalizar a informação sobre as predefinições

### KCoreAddons

- Descontinuação do KAboutData::fromPluginMetaData, agora que existe o KAboutPluginDialog
- Adição de um aviso descritivo quando o inotify_add_watch devolvia ENOSPC (erro 387663)
- Adição de teste do erro "bug-414360" - não é um erro do 'ktexttohtml' (erro 414360)

### KDBusAddons

- Inclusão de API para implementar argumentos '--replace' de forma genérica

### KDeclarative

- limpeza do protocolo de transferência EBN do kdeclarative
- Adaptação às alterações no 'kconfigwidgets'
- tornar visíveis o cabeçalho e o rodapé quando tiverem conteúdo
- suporte para os 'qqmlfileselectors'
- Permitir a desactivação do comportamento de gravação automática no ConfigPropertyMap

### KDED

- Remoção da dependência do 'kdeinit' no 'kded'

### Suporte para a KDELibs 4

- remoção do 'kgesturemap' não usado do 'kaction'

### KDocTools

- Trabalho no Catalão: Adição de itens em falta

### KI18n

- Reposição do I18N_NOOP2

### KIconThemes

- Descontinuação do método de topo UserIcon, que não é mais usado

### KIO

- Adição de novo protocolo para os pacotes 7z
- [CopyJob] Ao criar ligações, ter também em consideração o 'https' para o ícone 'text-html'
- [KFileWidget] Evitar a invocação do 'slotOk' logo a seguir à mudança do URL (erro 412737)
- [kfilewidget] Carregamento dos ícones pelo nome
- KRun: não substituir a aplicação preferida pelo utilizador ao abrir os ficheiros locais *.*html e co. (erro 399020)
- Reparação da consulta do 'proxy' de FTP/HTTP no caso de não existir nenhum 'proxy'
- IOSlave de FTP: Correcção da passagem de parâmetros ProxyUrls
- [KPropertiesDialog] oferta de uma forma de apresentar o destino de uma ligação simbólica (erro 413002)
- [IOSlave Remote] Adição do Nome Visível ao remote:/ (erro 414345)
- Correcção das opções de 'proxy' do HTTP (erro 414346)
- [KDirOperator] Adição do atalho Backspace para a acção 'recuar'
- Quando o 'kioslave5' não for encontrado nos locais do 'libexec', tentar a $PATH
- [Samba] Melhoria da mensagem de aviso sobre o nome NetBIOS
- [DeleteJob] Uso de uma rotina numa tarefa separada para executar a operação de E/S em si (erro 390748)
- [KPropertiesDialog] Tornar o texto da data de criação seleccionável com o rato (erro 413902)

Módulos obsoletos:

- Descontinuação das classes KTcpSocket e KSsl*
- Remoção dos últimos vestígios da KSslError na TCPSlaveBase
- Migração dos meta-dados do 'ssl_cert_errors' do KSslError para o QSslError
- Descontinuação da variante baseada no KTcpSocket do construtor do KSslErrorUiData
- [http kio slave] uso do QSslSocket em vez do KTcpSocket (obsoleto)
- [TcpSlaveBase] migração do KTcpSocket (obsoleto) para o QSslSocket

### Kirigami

- Correcção das margens do ToolBarHeader
- Não estoirar quando a origem do ícone está em branco
- MenuIcon: correcção dos avisos quando a área não está inicializada
- Ter em conta uma legenda mnemónica para voltar atrás para o ""
- Correcção das acções da InlineMessage que estão sempre colocadas no menu estendido
- Correcção do fundo predefinido dos cartões (erro 414329)
- Ícone: resolução de problemas de tarefas quando a origem é 'http'
- correcções de navegação do teclado
- i18n: extracção das mensagens também do código em C++
- Correcção da posição do comando do projecto CMake
- Uso de apenas uma instância do QmlComponentsPool por motor (erro 414003)
- Mudança do ToolBarPageHeader para usar o comportamento de fecho do ícone do ActionToolBar
- ActionToolBar: Mudar automaticamente para apenas ícones nas acções marcadas como KeepVisible
- Adição de uma propriedade 'displayHint' no Action
- adição da interface de DBus na versão estática
- Reversão do "ter em conta a velocidade de arrastamento quando terminar uma torção"
- FormLayout: Correcção da altura da legenda se o modo panorâmico estiver desactivado
- não mostrar uma pega por omissão quando não é uma janela modal
- Reversão da "Garantia que o 'topContent' do GlobalDrawer fica sempre no topo"
- Uso de uma RowLayout para a formatação do ToolBarPageHeader
- Centrar na vertical e à esquerda as acções no ActionTextField (erro 413769)
- monitorização dinâmica do 'irestore' no modo da tablete
- substituição do SwipeListItem
- suporte para a propriedade 'actionsVisible'
- início da migração do SwipeListItem para o SwipeDelegate

### KItemModels

- Descontinuação do KRecursiveFilterProxyModel
- KNumberModel: tratamento ordeiro de um 'stepSize' igual a 0
- Exposição do KNumberModel no QML
- Adição de nova classe KNumberModel que é um modelo de números entre dois valores
- Exposição do KDescendantsProxyModel no QML
- Adição da importação de 'qml' do KItemModels

### KNewStuff

- Adição de algumas ligações amigáveis para "comunicar erros aqui"
- Correcção da sintaxe de 'i18n' para evitar erros durante a execução (erro 414498)
- Mudança do KNewStuffQuick::CommentsModel para o SortFilterProxy nas revisões
- Definição correcta dos argumentos de 'i18n' num único passo (erro 414060)
- Estas funções existem @since 5.65, não da 5.64
- Adição do OBS aos gravadores de ecrãs (erro 412320)
- Correcção de algumas ligações inválidas, actualização das ligações para https://kde.org/applications/
- Correcção das traduções do $GenericName
- Apresentação de um indicador "A carregar mais..." quando carregar os dados da janela
- Apresentação de mais algumas reacções úteis no NewStuff::Page enquanto o motor está a carregar (erro 413439)
- Adição de um componente sobreposto para as reacções da actividade do item (erro 413441)
- Só apresentar o DownloadItemsSheet se existir mais que um item a transferir (erro 413437)
- Uso do cursor da mão a apontar no caso das delegações com 'click' simples (erro 413435)
- Correcção dos formatos dos cabeçalhos para os componentes EntryDetails e Page (erro 413440)

### KNotification

- Fazer com que a documentação reflicta que o 'setIconName' é preferível ao 'setPixmap' sempre que possível
- Localização do ficheiro de configuração do documento no Android

### KParts

- Marcação adequada do BrowserRun::simpleSave como obsoleto
- BrowserOpenOrSaveQuestion: extracção do enumerado AskEmbedOrSaveFlags do BrowserRun

### KPeople

- Permitir a activação da ordenação do QML

### kquickcharts

Novo módulo. O módulo Quick Charts oferece um conjunto de gráficos que poderão ser usados nas aplicações em QtQuick. Pretendem ser usados para a apresentação simples de dados, assim como para a apresentação contínua de dados em alto volume. Os gráficos usam um sistema chamado campos de distância para o seu desenho acelerado, o que oferece formas de usar o GPU para desenhar as formas 2D sem perda de qualidade.

### KTextEditor

- KateModeManager::updateFileType(): validação dos modos e actualização do menu da barra de estado
- Verificação dos modos do ficheiro de configuração da sessão
- LGPLv2+ após o OK de Svyatoslav Kuzmich
- reposição do pré-formato dos ficheiros

### KTextWidgets

- Descontinuação do kregexpeditorinterface
- [kfinddialog] Remoção do uso do sistema de 'plugins' do kregexpeditor

### KWayland

- [servidor] Não usar a implementação própria do 'dmabuf'
- [servidor] Mudança para 'duplo-buffer' das propriedaes no 'xdg-shell'

### KWidgetsAddons

- [KSqueezedTextLabel] Adição de ícone para a acção "Copiar todo o texto"
- Unificação do tratamento de margens do KPageDialog no KPageDialog propriamente dito (erro 413181)

### KWindowSystem

- Ajuste da contagem após a adição do _GTK_FRAME_EXTENTS
- Adição do suporte para o _GTK_FRAME_EXTENTS

### KXMLGUI

- Eliminação do suporte não usado e defeituoso do KGesture
- Adição do KAboutPluginDialog para ser usado com o KPluginMetaData
- Possibilidade também de invocar a lógica de reposição da sessão quando as aplicações são lançadas manualmente (erro 413564)
- Adição de propriedade em falta no KKeySequenceWidget

### Ícones do Oxygen

- Ligações simbólicas do 'microphone' para 'audio-input-microphone' em todos os tamanhos (erro 398160)

### Plataforma do Plasma

- mudança da gestão de 'backgroundhints' no Applet
- uso do selector de ficheiros no interceptor
- mais uso do ColorScope
- vigiar também as alterações da janela
- suporte para a remoção do fundo e da sombra automática por parte do utilizador
- suporte para selectores de ficheiros
- suporte para os selectores de ficheiros em qml
- remoção de coisas antigas do 'qgraphicsview'
- não apagar e recriar o 'wallpaperinterface' se não for necessário
- Verificação no MobileTextActionsToolBar se o 'controlRoot' é indefinido antes de o usar
- Adição do hideOnWindowDeactivate ao PlasmaComponents.Dialog

### Purpose

- inclusão do comando do 'cmake' que será usado

### QQC2StyleBridge

- [TabBar] Uso da cor da janela em vez da cor do botão (erro 413311)
- associar as propriedades 'enabled' à 'enabled' da vista
- [ToolTip] Basear o tempo-limite no tamanho do texto
- [ComboBox] Não escurecer o Popup
- [ComboBox] Não indicar o foco quando a área de valores estiver aberta
- [ComboBox] Acompanhamento da 'focusPolicy'

### Solid

- [udisks2] correcção da detecção de mudança de discos nas unidades ópticas externas (erro 394348)

### Sonnet

- Desactivação da infra-estrutura do 'ispell' com o 'mingw'
- Implementação da infra-estrutura do ISpellChecker para o Windows &gt;= 8
- Suporte de compilação multi-plataforma do 'parsetrigrams'
- incorporação do 'trigrams.map' na biblioteca dinâmica

### Sindicância

- Correcção do Erro 383381 - A obtenção do URL da fonte a partir de um canal do Youtube não funciona mais (erro 383381)
- Extracção do código para que se possa corrigir o processamento do mesmo (erro 383381)
- o 'atom' tem suporte para ícones (para que se possa usar o ícone específico no Akregator)
- Conversão para aplicações reais do 'qtest'

### Realce de Sintaxe

- Actualizações da versão final do CMake 3.16
- reStructuredText: Correcção dos caracteres antecedentes dos literais incorporados
- rst: Adição de suporte para as hiperligações autónomas
- JavaScript: passagem das palavras-chave do TypeScript e outras melhorias
- JavaScript/TypeScript React: mudança de nome das definições de sintaxe
- LaTeX: correcção do separador da barra invertida (\) em algumas palavras-chave (erro 413493)

### ThreadWeaver

- Uso de URL com encriptação no transporte

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
