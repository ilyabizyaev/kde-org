---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: O KDE Lança as Aplicações do KDE 17.04.3
layout: application
title: O KDE Lança as Aplicações do KDE 17.04.3
version: 17.04.3
---
13 de Julho de 2017. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../17.04.0'>Aplicações do KDE 17.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 25 correcções de erros registadas incluem as melhorias nos módulos 'kdepim', no 'dolphin', no 'dragonplayer', no 'kdenlive', no 'umbrello', entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.34.
