---
date: 2013-08-14
hidden: true
title: La plataforma KDE 4.11 proporciona un mejor rendimiento
---
La plataforma de KDE 4 lleva sin nuevas funcionalidades desde la versión 4.9. Por tanto, esta versión solo incluye la solución de varios errores y mejoras de rendimiento.

El motor semántico de almacenamiento y de búsqueda Nepomuk ha experimentado una enorme optimización de rendimiento, como un conjunto de optimizaciones de lectura que hace que la lectura de datos sea hasta 6 veces más rápida. La indexación se realiza de forma más inteligente, ya que se ha dividido en dos etapas. En la primera etapa se obtiene la información general (como el tipo y el nombre de archivo) de manera inmediata; la información adicional como las etiquetas de los medios, la información del autor y demás se extrae en una segunda etapa, que es más lenta. La operación de mostrar los metadatos del contenido recién creado o del contenido recién descargado es mucho más rápida. Además, los desarrolladores de Nepomuck han mejorado el sistema de copia de respaldo y de recuperación de Nepomuck se han mejorado. Por último, pero no por ello menos importante, ahora se ha dotado al sistema con nuevos indexadores para numerosos formatos de documento como odf o docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Funcionalidades semánticas en acción en Dolphin` width="600px">}}

El formato de almacenamiento optimizado de Nepomuck y el indexador de correo, que se ha reescrito completamente, hacen que sea necesario reindexar algunos contenidos del disco duro. Como consecuencia, la ejecución de la reindexación consumirá una cantidad inusual de rendimiento de cálculo durante un periodo de tiempo determinado, dependiendo de la cantidad de contenido que se necesite reindexar. La primera vez que se inicie sesión, se ejecutará una conversión automática de la base de datos de Nepomuck.

Se han solucionado más errores menores, los cuales <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'> se pueden encontrar en el archivo de registro de git</a>.

#### Instalación de la plataforma de desarrollo de KDE

El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href='http://windows.kde.org'>KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href='http://mac.kde.org/'>KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no se utilizan. <a href='http://plasma-active.org'>Plasma Active</a> es una experiencia de usuario para una amplia variedad de dispositivos, como tabletas y otro tipo de hardware móvil.

Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a>, y también en <a href='/download'>CD-ROM</a> o con cualquiera de los <a href='/distributions'>principales sistemas GNU/Linux y UNIX</a> de la actualidad.

##### Paquetes

Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 4.11.0 para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. <br />

##### Ubicación de los paquetes

Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha sido notificado, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki de la Comunidad</a>.

La totalidad del código fuente de %[1] se puede <a href='/info/4/4.11.0'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar el software de KDE 4.11.0 en la <a href='/info/4/4.11.0#binary'>Página de información sobre 4.11.0</a>.

#### Requisitos del sistema

Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, como la 4.8.4. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.<br />Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.

## También se han anunciado hoy:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Los espacios de trabajo de Plasma 4.11 continúan refinando la experiencia de usuario</a>

Preparándose para un mantenimiento a largo plazo, los espacios de trabajo de Plasma proporcionan más mejoras a la funcionalidad básica con una barra de tareas más suave, un elemento gráfico más inteligente para la batería y un mezclador de sonido mejorado. La introducción de KScreen aporta a los espacios de trabajo una gestión inteligente de varios monitores y las mejoras de rendimiento a gran escala combinadas con pequeños ajustes de ergonomía hacen que la experiencia general sea más agradable.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Las aplicaciones de KDE 4.11 proporcionan un enorme paso adelante en la gestión de información personal y mejoras en general</a>

Esta versión supone enormes mejoras para KDE PIM, las cuales proporcionan un mejor rendimiento y muchas nuevas funcionalidades. Kate mejora el rendimiento de los desarrolladores de Python y Javascript con nuevos complementos; Dolphin es más rápido y las aplicaciones educativas incorporan varias funcionalidades nuevas.
