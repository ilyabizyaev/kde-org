---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE lanza las Aplicaciones de KDE 18.04.3
layout: application
title: KDE lanza las Aplicaciones de KDE 18.04.3
version: 18.04.3
---
Hoy, 12 de julio de 2018, KDE ha lanzado la tercera actualización de estabilización para las <a href='../18.04.0'>Aplicaciones 18.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones y será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en Kontact, Ark, Cantor, Dolphin, Gwenview y KMag, entre otras aplicaciones.

Las mejoras incluyen:

- Se ha restaurado la compatibilidad con los servidores IMAP que no anuncian sus capacidades.
- Ark ya puede extraer archivos comprimidos ZIP que carecen de entradas correctas para las carpetas.
- Las notas de pantalla KNotes vuelven a seguir el puntero del ratón mientras se mueven.
