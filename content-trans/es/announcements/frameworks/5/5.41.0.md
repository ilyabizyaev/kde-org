---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Strip down and re-write the baloo tags KIO slave (bug 340099)

### BluezQt

- Do not leak rfkill file descriptors (bug 386886)

### Iconos Brisa

- Se han añadido los tamaños de iconos que faltaban (error 384473).
- add install and uninstall icons for discover

### Módulos CMake adicionales

- Add the description tag to the generated pkgconfig files
- ecm_add_test: Use proper path sep on Windows
- Añadir «FindSasl2.cmake» a ECM.
- Only pass the ARGS thing when doing Makefiles
- Add FindGLIB2.cmake and FindPulseAudio.cmake
- ECMAddTests: set QT_PLUGIN_PATH so locally built plugins can be found
- KDECMakeSettings: more docu about the layout of the build dir

### Integración con Frameworks

- Support downloading the 2nd or 3rd download link from a KNS product (bug 385429)

### KActivitiesStats

- Start fixing libKActivitiesStats.pc: (bug 386933)

### KActivities

- Fix race that starts kactivitymanagerd multiple times

### KAuth

- Allow to only build the kauth-policy-gen code generator
- Add a note about calling the helper from multithreaded applications

### KBookmarks

- Do not show edit bookmarks action if keditbookmarks is not installed
- Port from deprecated KAuthorized::authorizeKAction to authorizeAction

### KCMUtils

- keyboard navigation in and out QML kcms

### KCompletion

- Do not crash when setting new line edit on an editable combo box
- KComboBox: Return early when setting editable to previous value
- KComboBox: Reuse the existing completion object on new line edit

### KConfig

- Don't look for /etc/kderc every single time

### KConfigWidgets

- Update default colors to match new colors in D7424

### KCoreAddons

- Input validation of SubJobs
- Warn about errors when parsing json files
- Install mimetype definitions for kcfg/kcfgc/ui.rc/knotify &amp; qrc files
- Add a new function to measure the length by text
- Fix KAutoSave bug on file with white space in it

### KDeclarative

- Permitir compilación en Windows.
- Hacer que se pueda compilar con QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY.
- [MouseEventListener] Allow accepting mouse event
- use a single QML engine

### KDED

- kded: remove dbus calls to ksplash

### KDocTools

- Se ha actualizado la traducción del portugués brasileño.
- Se ha actualizado la traducción del ruso.
- Se ha actualizado la traducción del ruso.
- Update customization/xsl/ru.xml (nav-home was missing)

### KEmoticons

- KEmoticons: port plugins to JSON and add support for loading with KPluginMetaData
- Do not leak symbols of pimpl classes, protect with Q_DECL_HIDDEN

### KFileMetaData

- The usermetadatawritertest requires Taglib
- If the property value is null, remove the user.xdg.tag attribute (bug 376117)
- Open files in TagLib extractor readonly

### KGlobalAccel

- Group some blocking dbus calls
- kglobalacceld: Avoid loading an icon loader for no reason
- generate correct shortcut strings

### KIO

- KUriFilter: filter out duplicate plugins
- KUriFilter: simplify data structures, fix memory leak
- [CopyJob] Don't start all over after having removed a file
- Fix creating a directory via KNewFileMenu+KIO::mkpath on Qt 5.9.3+ (bug 387073)
- Created an auxiliary function 'KFilePlacesModel::movePlace'
- Expose KFilePlacesModel 'iconName' role
- KFilePlacesModel: Avoid unnecessary 'dataChanged' signal
- Return a valid bookmark object for any entry in KFilePlacesModel
- Create a 'KFilePlacesModel::refresh' function
- Create 'KFilePlacesModel::convertedUrl' static function
- KFilePlaces: Created 'remote' section
- KFilePlaces: Add a section for removable devices
- Added baloo urls into places model
- Fix KIO::mkpath with qtbase 5.10 beta 4
- [KDirModel] Emit change for HasJobRole when jobs change
- Change label "Advanced options" &gt; "Terminal options"

### Kirigami

- Offset the scrollbar by the header size (bug 387098)
- bottom margin based on actionbutton presence
- don't assume applicationWidnow() to be available
- Don't notify about value changes if we are still in the constructor
- Replace the library name in the source
- Permitir colores en más lugares.
- color icons in toolbars if needed
- Considerar los colores de los iconos en los botones de las acciones principales.
- start for an "icon" grouped property

### KNewStuff

- Revert "Detach before setting the d pointer" (bug 386156)
- do not install development tool to aggregate desktop files
- [knewstuff] Do not leak ImageLoader on error

### Framework KPackage

- Properly do strings in the kpackage framework
- No tratar de generar «metadata.json» si no existe «metadata.desktop».
- fix kpluginindex caching
- Se ha mejorado la salida de los errores.

### KTextEditor

- Fix VI-Mode buffer commands
- Impedir la ampliación accidental.

### KUnitConversion

- Portar de QDom a QXmlStreamReader.
- Use https for downloading currency exchange rates

### KWayland

- Expose wl_display_set_global_filter as a virtual method
- Corregir kwayland-testXdgShellV6.
- Add support for zwp_idle_inhibit_manager_v1 (bug 385956)
- [server] Support inhibiting the IdleInterface

### KWidgetsAddons

- Evitar diálogo de contraseña inconsistente.
- Set enable_blur_behind hint on demand
- KPageListView: Update width on font change

### KWindowSystem

- [KWindowEffectsPrivateX11] Add reserve() call

### KXMLGUI

- Fix translation of toolbar name when it has i18n context

### Framework de Plasma

- The #warning directive is not universal and in particular is NOT supported by MSVC
- [IconItem] Use ItemSceneHasChanged rather than connect on windowChanged
- [Icon Item] Explicitly emit overlaysChanged in the setter rather than connecting to it
- [Dialog] Use KWindowSystem::isPlatformX11()
- Reduce the amount of spurious property changes on ColorScope
- [Icon Item] Emit validChanged only if it actually changed
- Suppress unnecessary scroll indicators if the flickable is a ListView with known orientation
- [AppletInterface] Emit change signals for configurationRequired and -Reason
- Use setSize() instead of setProperty width and height
- Fixed an issue where PlasmaComponents Menu would appear with broken corners (bug 381799)
- Fixed an issue where context menus would appear with broken corners (bug 381799)
- API docs: add deprecation notice found in the git log
- Synchronize the component with the one in Kirigami
- Search all KF5 components as such instead as separate frameworks
- Reduce spurious signal emissions (bug 382233)
- Add signals indicating if a screen was added or removed
- Instalar cosas relacionadas con «Switch».
- Don't rely in includes of includes
- Optimize SortFilterModel role names
- Remove DataModel::roleNameToId

### Prison

- Añadir el generador de código «Aztec».

### QQC2StyleBridge

- determine QQC2 version at build time (bug 386289)
- by default, keep the background invisible
- add a background in ScrollView

### Solid

- Faster UDevManager::devicesFromQuery

### Sonnet

- Hacer posible la compilación cruzada de Sonnet.

### Resaltado de sintaxis

- Añadir PKGUILD a la sintaxis de bash.
- JavaScript: include standard mime types
- debchangelog: Añadir «Bionic Beaver».
- Se ha actualizado el archivo de sintaxis de SQL (Oracle) (error 386221).
- SQL: move detecting comments before operators
- crk.xml: added &lt;?xml&gt; header line

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
