---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၈.၀ တင်ပို့ခဲ့သည်
layout: application
title: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၈.၀ တင်ပို့ခဲ့သည်
version: 18.08.0
---
August 16, 2018. KDE Applications 18.08.0 are now released.

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

### What's new in KDE Applications 18.08

#### စစ်စတမ်

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, has received various quality-of-life improvements:

- The 'Settings' dialog has been modernized to better follow our design guidelines and be more intuitive.
- Various memory leaks that could slow down your computer have been eliminated.
- အမှိုက်ပုံးကြည့်သောအခါ 'အသစ်ဖန်တီးမည်' မီနူးအိုင်တန်များ မရရှိနိုင်တော့ပါ။
- The application now adapts better to high resolution screens.
- The context menu now includes more useful options, allowing you to sort and change the view mode directly.
- Sorting by modification time is now 12 times faster. Also, you can now launch Dolphin again when logged in using the root user account. Support for modifying root-owned files when running Dolphin as a normal user is still work in progress.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Multiple enhancements for <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, are available:

- The 'Find' widget will now appear on the top of the window without disrupting your workflow.
- Support for more escape sequences (DECSCUSR & XTerm Alternate Scroll Mode) has been added.
- You can now also assign any character(s) as a key for a shortcut.

#### ဂရပ်ဖစ်

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 is a major release for <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer. Over the last months contributors worked on a plethora of improvements. Highlights include:

- Gwenview's statusbar now features an image counter and displays the total number of images.
- It is now possible to sort by rating and in descending order. Sorting by date now separates directories and archives and was fixed in some situations.
- Support for drag-and-drop has been improved to allow dragging files and folders to the View mode to display them, as well as dragging viewed items to external applications.
- Pasting copied images from Gwenview now also works for applications which only accept raw image data, but no file path. Copying modified images is now supported as well.
- Image resize dialog has been overhauled for greater usability and to add an option for resizing images based on percentage.
- Red Eye Reduction tool's size slider and crosshair cursor were fixed.
- Transparent background selection now has an option for 'None' and can be configured for SVGs as well.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

Image zooming has become more convenient:

- Enabled zooming by scrolling or clicking as well as panning also when the Crop or Red Eye Reduction tools are active.
- Middle-clicking once again toggles between Fit zoom and 100% zoom.
- Added Shift-middle-clicking and Shift+F keyboard shortcuts for toggling Fill zoom.
- Ctrl-clicking now zooms faster and more reliably.
- Gwenview now zooms to the cursor's current position for Zoom In/Out, Fill and 100% zoom operations when using the mouse and keyboard shortcuts.

Image comparison mode received several enhancements:

- Fixed size and alignment of the selection highlight.
- Fixed SVGs overlapping the selection highlight.
- For small SVG images, the selection highlight matches the image size.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

A number of smaller enhancements was introduced to make your workflow even more enjoyable:

- Improved the fade transitions between images of varying sizes and transparencies.
- Fixed the visibility of icons in some floating buttons when a light color scheme is used.
- When saving an image under a new name, the viewer does not jump to an unrelated image afterwards.
- When the share button is clicked and kipi-plugins are not installed, Gwenview will prompt the user to install them. After the installation they are immediately displayed.
- Sidebar now prevents getting hidden accidentally while resizing and remembers its width.

#### ရုံးသုံးကိရိယာများ

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, features some improvements in the travel data extraction engine. It now supports UIC 918.3 and SNCF train ticket barcodes and Wikidata-powered train station location lookup. Support for multi-traveler itineraries was added, and KMail now has integration with the KDE Itinerary app.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, the personal information management framework, is now faster thanks to notification payloads and features XOAUTH support for SMTP, allowing for native authentication with Gmail.

#### ပညာရေး

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's frontend to mathematical software, now saves the status of panels (\"Variables\", \"Help\", etc.) for each session separately. Julia sessions have become much faster to create.

User experience in <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, our graphing calculator, has been significantly improved for touch devices.

#### Utilities

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Contributors to <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, focused on improving the Rectangular Region mode:

- In Rectangular Region mode, there is now a magnifier to help you draw a pixel-perfect selection rectangle.
- You can now move and resize the selection rectangle using the keyboard.
- The user interface follows the user's color scheme, and the presentation of the help text has been improved.

To make sharing your screenshots with others easier, links for shared images are now automatically copied to the clipboard. Screenshots can now be automatically saved in user-specified sub-directories.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, our webcam recorder, was updated to avoid crashes with newer GStreamer versions.

### Bug Stomping

More than 120 bugs have been resolved in applications including the Kontact Suite, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello and more!

### ပြောင်းလဲမှုမှတ်တမ်းအပြည့်အစုံ
