---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE випущено збірку програм 14.12.
layout: application
title: KDE випущено збірку програм KDE 14.12
version: 14.12.0
---
17 грудня 2014 року. Сьогодні KDE випущено збірку програм KDE версії 14.12. У цьому випуску ми реалізували нові можливості та виправили вади у понад 100 програмах. Більшість із цих програм засновано на платформі для розробки KDE 4; деякі ж уже портовано на новий набір бібліотек <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, ретельно поділених на модулі та заснованих на Qt5, найсвіжішій версії цієї популярної основи для побудови програм для багатьох програмних платформ.

Новою у цьому випуску є бібліотека <a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>libkface</a>. Цю бібліотеку призначено для виявлення та розпізнавання облич на фотографіях.

До цього випуску включено перші засновані на KDE Frameworks 5 версії <a href='http://www.kate-editor.org'>Kate</a> and <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, <a href='https://www.kde.org/applications/system/konsole/s'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/s'>Gwenview</a>, <a href='http://edu.kde.org/kalgebras'>KAlgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>KHangman</a>, <a href='http://edu.kde.org/kig'>Kig</a>, <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> і <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Деякі бібліотеки також готові до використання з KDE Frameworks 5: analitza і libkeduvocdocument.

Стабільну версію <a href='http://kontact.kde.org'>комплексу програм Kontact</a> 4.14 заморожено (виправленню підлягають лише значні вади). Розробники зараз сконцентрували зусилля на портуванні комплексу на KDE Frameworks 5.

Серед нових можливостей у цьому випуску:

+ <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> має нову версію для Android завдяки KDE Frameworks 5 і може <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>друкувати графіки поверхонь у просторі</a>.
+ У <a href='http://edu.kde.org/kgeography'>KGeography</a> тепер є карта індійського штату Біхар.
+ У програмі для перегляду документів <a href='http://okular.kde.org'>Okular</a> нової версії передбачено підтримку зворотного пошуку рядка у dvi за допомогою latex-synctex та трохи поліпшено підтримку документів у форматі ePub.
+ У <a href='http://umbrello.kde.org'>Umbrello</a> реалізовано дуже багато можливостей, які просто неможливо тут перелічити.

До квітневого випуску програм KDE 15.04 буде включено багато нових можливостей, а також інші програми, засновані на модульній архітектурі KDE Frameworks 5.
