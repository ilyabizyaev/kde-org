---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE rilascia KDE Applications 18.04.0
layout: application
title: KDE rilascia KDE Applications 18.04.0
version: 18.04.0
---
19 aprile 2018. KDE rilascia KDE Applications 18.04.0.

Lavoriamo continuamente per migliorare il software incluso nelle nostre serie di KDE Application, confidiamo che troverai utili tutti i miglioramenti e la risoluzione degli errori.

## Novità in KDE Applications 18.04

### Sistema

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

Il primo rilascio principale nel 2018 di <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, il gestore di file completo di KDE, contiene molti miglioramenti ai pannelli:

- Le sezioni del pannello «Risorse» possono essere ora nascoste, nel caso preferisca non visualizzarle, ed è ora disponibile una nuova sezione «Rete» in cui visualizzare le risorse remote.
- Il pannello «Terminale» può essere agganciato a qualsiasi lato della finestra, e se provi ad aprirlo senza che Konsole sia installato, Dolphin mostrerà un avviso e ti aiuterà a installarlo.
- È stato migliorato il supporto HiDPI per il pannello «Informazioni».

Sono state aggiornate anche la vista cartelle e i menu:

- La cartella Cestino ora mostra un pulsante «Svuota cestino».
- È stato aggiunto un elemento di menu «Mostra destinazione», che aiuta a trovare i percorsi dei collegamenti simbolici.
- È stata migliorata l'integrazione Git, dato che il menu di contesto per le cartelle git ora mostra due nuove azioni per «git log» e «git merge».

Altri miglioramenti includono:

- È stata introdotta una nuova scorciatoia che fornisce l'opzione per aprire la Barra del filtro semplicemente facendo clic su tasto barra (/).
- Ora puoi ordinare e organizzare le foto per data di scatto.
- Il trascinamento e rilascio di molti piccoli file in Dolphin è più veloce, e gli utenti possono ora annullare le operazioni di rinomina in serie.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

Per rendere più divertente il lavoro con la riga di comando, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'emulatore di terminale di KDE, ora può avere un aspetto più carino:

- Puoi scaricare schemi di colori tramite KNewStuff.
- La barra di scorrimento si fonde meglio con lo schema di colori attivo.
- Per impostazione predefinita ora la barra delle schede è mostrata solo quando necessario.

I collaboratori di Konsole non si sono fermati qui e hanno introdotto molte novità:

- È stata aggiunta una modalità di sola lettura e una proprietà del profilo per passare alla copiatura del testo come HTML.
- Sotto Wayland, Konsole ora supporta il menu trascina e rilascia.
- Sono stati introdotti diversi miglioramenti con riferimento al protocollo ZMODEM: Konsole ora può gestire l'indicatore B01 di caricamento zmodem, mostrerà l'avanzamento durante il trasferimento dei dati, il pulsante Annulla nella finestra di dialogo ora funziona e il trasferimento di file più grandi è supportato meglio leggendoli all'interno della memoria in parti da 1MB.

Altri miglioramenti includono:

- Lo scorrimento della rotellina del mouse wheel con libinput è stato sistemato, e ora è impedita la ripetizione in ciclo della cronologia durante lo scorrimento con la rotellina.
- Le ricerche sono aggiornate dopo che si modifica l'opzione per la ricerca con espressione regolare e quando si preme «Ctrl» + «Backspace» Konsole corrisponderà al comportamento xterm.
- È stata sistemata la scorciatoia «--background-mode».

### Multimedia

<a href='https://juk.kde.org/'>JuK</a>, il riproduttore musicale di KDE, supporta ora Wayland. Le nuove caratteristiche dell'interfaccia grafica ora includono la capacità di nascondere la barra del menu e di avere un'indicazione visiva della traccia correntemente in riproduzione. Se l'opzione di aggancio è disabilitata, JuK non si bloccherà più quando tenta di chiudersi tramite l'icona «chiudi» della finestra e l'interfaccia utente resterà visibile. Sono stati risolti anche errori riguardanti la riproduzione automatica inattesa di JuK quando si riprende dalla sospensione in Plasma 5 e la gestione della colonna della scaletta.

Per il rilascio 18.04, i collaboratori di <a href='https://kdenlive.org/'>Kdenlive</a>, l'editor video non lineare di KDE, si sono concentrati sul mantenimento:

- Il ridimensionamento delle clip non danneggia più le dissolvenze e i keyframe.
- Gli utenti di ridimensionamento su monitor HiDPI possono avvantaggiarsi di icone più nette.
- È stato risolto un possibile errore all'avvio con alcune configurazioni.
- MLT è ora richiesto in versione 6.6.0 o successiva, ed è stata migliorata la compatibilità.

#### Grafica

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Negli ultimi mesi, i collaboratori di <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, il visualizzatore e organizzatore di immagini di KDE, hanno lavorato su una marea di miglioramenti. Le novità includono:

- È stato aggiunto il supporto per i controller MPRIS: ora puoi controllare le presentazioni a schermo intero tramite KDE Connect, i tasti della tua tastiera e il plasmoide Media Player.
- I pulsanti attivi al passaggio del mouse sulle miniature ora possono essere disattivati.
- Lo strumento per il ritaglio è stato migliorato: ora le sue impostazioni vengono mantenute quando si passa a un'altra immagine, la forma del riquadro di selezione può essere bloccata premendo i tasti «Maiusc» o «Ctrl» e può essere anche bloccata alla proporzione dell'immagine attualmente visualizzata.
- In modalità a schermo intero, puoi ora uscire usando il tasto «Esc» e la tavolozza dei colori rispecchierà il tuo tema di colori attivo. Se esci da Gwenview in questa modalità, questa impostazione sarà memorizzata e anche la nuova sessione si avvierà a schermo intero.

L'attenzione ai dettagli è importante, dunque Gwenview è stato ripulito nelle aree seguenti:

- Gwenview visualizzerà nell'elenco «Cartelle recenti» percorsi per i file più leggibili, mostrerà il menu di contesto appropriato per gli elementi contenuti nell'elenco «File recenti» e non memorizzerà nulla quando si utilizza l'opzione «Disabilita cronologia».
- Il clic su una cartella nella barra laterale consente il passaggio tra le modalità di visualizzazione e di navigazione e quando si alterna tra le cartelle viene memorizzata l'ultima modalità utilizzata, permettendo così una navigazione più rapida nelle grandi raccolte d'immagini.
- La navigazione tramite tastiera è stata ulteriormente semplificata indicando correttamente il fuoco della tastiera nella modalità di navigazione.
- La funzionalità «Adatta alla larghezza» è stata sostituita da una più generica funzione «Riempi».

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Anche le piccole migliorie spesso possono rendere il lavoro dell'utente più divertente:

- Per migliorare la coerenza, le immagini SVG ora sono ingrandite come tutte le altre immagini quando l'opzione «Vista immagini → Ingrandisci le immagini più piccole» è attivata.
- Dopo aver modificato un'immagine o annullato le modifiche, la vista immagini e le miniature non perderanno più la sincronizzazione.
- Quando si rinominano le immagini, l'estensione del nome file sarà deselezionata per impostazione predefinita e la finestra di dialogo «Sposta/Copia/Collega» ora è impostata per mostrare la cartella attiva.
- Sono stati corretti tanti errori di visualizzazione, per es. nella barra degli URL, per la barra degli strumenti a schermo intero e per le animazioni dei suggerimenti delle miniature. Sono state anche aggiunte le icone mancanti.
- Per finire, ma non meno importante, il pulsante dello schermo intero mostrerà direttamente, al passaggio del mouse, l'immagine anziché il contenuto della cartella, e le impostazioni avanzate ora consentono maggior controllo sull'intento di resa dei colori ICC.

#### Ufficio

In <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, il visore di documenti universale di KDE, la resa PDF e l'estrazione del testo possono essere ora annullate se possiedi poppler versione 0.63 o superiore, il che significa che se hai un file PDF complesso, e cambi l'ingrandimento durante la resa, questa sarà annullata immediatamente invece di attendere la fine della resa.

Il supporto PDF JavaScript per AFSimple_Calculate è stato migliorato, e se possiedi poppler versione 0.64 o superiore, Okular supporterà le modifiche PDF JavaScript nello stato in sola lettura dei moduli.

La gestione dei messaggi di posta elettronica riguardanti le conferme di prenotazioni in <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, il completissimo programma di posta elettronica di KDE, è stata significativamente migliorata per supportare le prenotazioni dei treni e utilizza una banca dati degli aeroporti basata su Wikidata per mostrare i voli con le corrette informazioni riguardo i fusi orari. Per renderti le cose più semplici, è stato implementato un nuovo estrattore per messaggi di posta che non contengono dati di prenotazioni strutturati.

Altri miglioramenti includono:

- La struttura del messaggio può essere rivista tramite la nuova estensione «Esperto».
- Nell'editor Sieve è stata aggiunta un'estensione che permette di selezionare i messaggi dalla banca dati di Akonadi.
- È stata migliorata la ricerca del testo nell'editor, con supporto delle espressioni regolari.

#### Accessori

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

Il miglioramento dell'interfaccia utente di <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, il versatile strumento di KDE per le schermate, è stato uno degli obiettivi principali:

- La riga inferiore dei pulsanti è stata risistemata, e ora mostra un pulsante per aprire la finestra delle Impostazioni e un nuovo pulsante «Strumenti» che mostra i metodi per aprire la cartella delle schermate usate di recente e avviare un programma di registrazione dello schermo.
- La modalità di salvataggio per i file utilizzati di recente è ora ricordata per impostazione predefinita.
- La dimensione della finestre ora si adatta alla proporzione della schermata, col risultato di ottenere miniature della schermata più piacevoli e razionali dello spazio.
- La finestra delle Impostazioni è stato notevolmente semplificata.

In aggiunta, gli utenti saranno in grado di semplificare il loro lavoro con queste nuove caratteristiche:

- Quando si cattura una finestra specifica, il suo titolo può essere aggiunto automaticamente al nome del file della schermata.
- L'utente ora può scegliere se uscire automaticamente o no da Spectacle dopo qualsiasi operazione di salvataggio o copiatura.

Importanti correzioni di errori includono:

- Le operazioni di trascinamento e rilascio nelle finestre di Chromium ora funzionano come devono.
- L'uso ripetuto di scorciatoie di tastiera per salvare una schermata non aprono più una finestra di avviso per scorciatoia ambigua.
- Per le schermate di aree rettangolari, il bordo inferiore della selezione può essere regolato in modo più accurato.
- Affidabilità migliorata della cattura delle finestre toccando i bordi dello schermo quando la composizione è disattivata.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

Con <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, l'interfaccia per la cifratura universale e gestore dei certificati di KDE, le chiavi Curve 25519 EdDSA possono essere generate se usate con una versione recente di GnuPG. È stata aggiunta una vista «Blocco note» per le azioni di cifratura basate sul testo, ed è ora possibile firmare/cifrare direttamente nell'applicazione. Nella vista «Dettagli del certificato» ora trovi un'azione per l'esportazione che puoi utilizzare per esportare come testo per copiare e incollare. Inoltre, puoi importare il risultato tramite la nuova vista «Blocco note».

In <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, lo strumento grafico di KDE per la compressione e decompressione dei file e con supporto più formati, è ora possibile fermare la compressione o l'estrazione quando si usa il motore libzip per gli archivi ZIP.

### Applicazioni che si uniscono al programma di rilascio di KDE Applications

Il registratore webcam di KDE <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> e il programma per copie di sicurezza <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> ora seguono i rilasci delle Applications. È stato reintrodotto anche programma di messaggistica istantanea <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a>, dopo essere stato convertito a KDE Frameworks 5.

### Applicazioni che passano a un proprio programma di rilascio indipendente

L'editor esadecimale <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> ora segue un suo proprio ciclo di rilascio, dietro richiesta del suo responsabile.

### Ricerca e risoluzione degli errori

Gli oltre 170 errori corretti in applicazioni tra cui la suite Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello e altre!

### Elenco completo dei cambiamenti
