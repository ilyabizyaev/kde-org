---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: O KDE disponibiliza o KDE Applications 18.08.1
layout: application
title: O KDE disponibiliza o KDE Applications 18.08.1
version: 18.08.1
---
6 de Setembro de 2018. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../18.08.0'>Aplicações do KDE 18.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de uma dúzia de correções de erros registradas incluem melhorias no Kontact, Cantor, Gwenview, Okular, Umbrello, entre outros.

As melhorias incluem:

- O componente do KIO-MTP já não estoira quando o dispositivo já foi acedido por uma aplicação diferente
- O envio de e-mails no KMail agora usa a senha quando é indicada pela linha de comandos da senha
- O Okular agora recorda o modo da barra lateral depois de gravar os documentos PDF
