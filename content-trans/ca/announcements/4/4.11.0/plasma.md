---
date: 2013-08-14
hidden: true
title: Els espais de treball del Plasma 4.11 continuen refinant l'experiència d'usuari
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`Espais de treball del Plasma 4.11 de KDE` width="600px" >}}

En l'alliberament 4.11 dels espais de treball Plasma, la barra de tasques –un dels ginys més utilitzats– <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>s'ha adaptat al QtQuick</a>. La barra de tasques nova, encara que manté l'aspecte i funcionalitat de la seva contrapart, mostra un comportament més compatible i fluid. L'adaptació ha resolt certs errors existents. El giny de bateria (que anteriorment podia ajustar la lluminositat de la pantalla) ara també permet la lluminositat del teclat, i pot gestionar diverses bateries en dispositius perifèrics, com el ratolí i el teclat sense fil. Mostra la càrrega de la bateria per a cada dispositiu i avisa quan alguna està baixa. El menú Kickoff ara mostra les aplicacions recentment instal·lades durant alguns dies. I per acabar, però no menys important, les finestres emergents de notificació ara tenen un botó de configuració a on es pot canviar senzillament l'opció per aquest tipus de notificació particular.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Gestió millorada de les notificacions` width="600px" >}}

El KMix, el mesclador de so del KDE, ha rebut tasques significatives de rendiment i estabilitat, i també una <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>implementació completa del control de reproducció multimèdia</a> basada en l'estàndard MPRIS2.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`La miniaplicació de bateria redissenyada en acció` width="600px" >}}

## El gestor de finestres i de composició KWin

El gestor de finestres, el KWin, ha rebut actualitzacions significatives un altre cop, eliminant la tecnologia obsoleta i incorporant el protocol de comunicacions «XCB». El resultat és una gestió de les finestres més suau i ràpida. També s'ha introduït la implementació per l'OpenGL 3.1 i l'OpenGL ES 3.0. Aquest alliberament també incorpora la primera implementació experimental per al Wayland, el successor de l'X11. Això permet utilitzar el KWin amb l'X11 per sobre d'una pila del Wayland. Per a més informació sobre com usar aquest mode experimental vegeu <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>aquest article</a>. La interfície de creació de scripts del KWin també ha rebut nombroses millores, ara disposa de suport per a la configuració de la IU, animacions i efectes gràfics nous, i altres millores més petites. Aquest alliberament aporta un reconeixement millor de les multipantalles (inclosa una opció de vora lluent per les «cantonades sensibles»), s'ha millorat el mosaic ràpid (amb àrees de mosaic configurables) i la resolució habitual d'esmenes d'errors i optimitzacions. Vegeu <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>aquí</a> i <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>aquí</a> per a més detalls.

## Gestió de monitors i dreceres web

La configuració del monitor en l'arranjament del sistema s'ha <a href='http://www.afiestas.org/kscreen-1-0-released/'>substituït per la nova eina KScreen</a>. El KScreen aporta una implementació més intel·ligent del multimonitor als espais de treball Plasma, configurant automàticament les pantalles noves i recordant l'arranjament per als monitors configurats manualment. Té una interfície visual intuïtiva i gestiona el rearranjament dels monitors a través d'un senzill arrossegar i deixar anar.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`El KScreen nou de gestió de monitors` width="600px" >}}

Les dreceres web, la manera més senzilla de trobar ràpidament el que esteu cercant en la web, s'han polit i millorat. Moltes s'han actualitzat per utilitzar les connexions encriptades segures (TLS/SSL), s'han afegit noves dreceres web i s'han eliminat algunes dreceres obsoletes. També s'ha millorat el procés per afegir les vostres pròpies dreceres web. Trobeu més detalls <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>aquí</a>.

Aquest alliberament marca el final dels espais de treball Plasma 1, una part de les sèries de funcionalitats del KDE SC 4. Per facilitar la transició a la generació següent aquest alliberament tindrà suport durant dos anys com a mínim. Ara el desenvolupament de funcionalitats se centrarà en els espais de treball Plasma 2, les millores de rendiment i esmenes d'errors es concentraran en les sèries 4.11.

#### Instal·lació del Plasma

El programari KDE, incloses totes les seves biblioteques i les seves aplicacions, és disponible de franc d'acord amb les llicències de codi font obert (Open Source). El programari KDE s'executa en diverses configuracions de maquinari i arquitectures de CPU com ARM i x86, sistemes operatius i treballa amb qualsevol classe de gestor de finestres o entorn d'escriptori. A banda del Linux i altres sistemes operatius basats en UNIX, podeu trobar versions per al Windows de Microsoft de la majoria d'aplicacions del KDE al lloc web <a href='http://windows.kde.org'>programari de Windows</a> i versions per al Mac OS X d'Apple en el <a href='http://mac.kde.org/'>lloc web del programari KDE per al Mac</a>. Les compilacions experimentals d'aplicacions del KDE per a diverses plataformes mòbils com MeeGo, MS Windows Mobile i Symbian es poden trobar en la web, però actualment no estan suportades. El <a href='http://plasma-active.org'>Plasma Active</a> és una experiència d'usuari per a un espectre ampli de dispositius, com tauletes i altre maquinari mòbil.

El programari KDE es pot obtenir en codi font i en diversos formats executables des de <a href='https://download.kde.org/stable/4.11.0'>download.kde.org</a> i també es pot obtenir en <a href='/download'>CD-ROM</a> o amb qualsevol dels <a href='/distributions'>principals sistemes GNU/Linux i UNIX</a> que es distribueixen avui en dia.

##### Paquets

Alguns venedors de Linux/UNIX OS han proporcionat gentilment paquets executables de 4.11.0 per a algunes versions de la seva distribució, i en altres casos ho han fet els voluntaris de la comunitat. <br />

##### Ubicació dels paquets

Per a una llista actual de paquets executables disponibles dels quals ha informat l'equip de llançament del KDE, si us plau, visiteu el <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>wiki de la comunitat</a>.

El codi font complet per a 4.11.0 es pot <a href='/info/4/4.11.0'>descarregar de franc</a>. Les instruccions per a compilar i instal·lar el programari KDE, versió 4.11.0, estan disponibles a la <a href='/info/4/4.11.0#binary'>pàgina d'informació de la versió 4.11.0</a>.

#### Requisits del sistema

Per tal d'aprofitar al màxim aquestes versions, es recomana utilitzar una versió recent de les Qt, com la 4.8.4. Això és necessari per a assegurar una experiència estable i funcional, ja que algunes millores efectuades en el programari KDE s'ha efectuat realment en la infraestructura subjacent de les Qt.<br /> Per tal de fer un ús complet de les capacitats del programari KDE, també es recomana l'ús dels darrers controladors gràfics per al vostre sistema, ja que aquest pot millorar substancialment l'experiència d'usuari, tant per a les funcionalitats opcionals, com el rendiment i l'estabilitat en general.

## També s'ha anunciat avui:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Les aplicacions del KDE 4.11 fan un pas endavant en la gestió d'informació personal i millores globals</a>

Aquest llançament es distingeix per moltes millores en la pila del KDE PIM, que aporten un rendiment millor i moltes funcionalitats noves. El Kate millora la productivitat dels desenvolupadors en Python i Javascript amb connectors nous. El Dolphin ha esdevingut més ràpid i les aplicacions educatives tenen diverses funcionalitats noves.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> La plataforma KDE 4.11 proporciona un rendiment millor</a>

Aquest llançament de la plataforma KDE 4.11 continua enfocant-se en l'estabilitat. Les funcionalitats noves s'estan implementant amb el nostre futur llançament dels Frameworks 5.0 de KDE, però per al llançament estable s'ha restringit a optimitzar la infraestructura del Nepomuk.
