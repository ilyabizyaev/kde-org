---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE publie les applications de KDE en version 17.12.1
layout: application
title: KDE publie les applications de KDE en version 17.12.1
version: 17.12.1
---
11 Janvier 2018. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../17.12.0'>applications 17.12 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta, Umbrello et bien d'autres.

Les améliorations incluses sont :

- L'envoi de courriels avec Kontact a été corrigé pour certains serveurs « SMTP ».
- La frise temporelle de Gwenview et les recherches par étiquette ont été améliorées
- L'importation Java a été corrigée pour l'outil Umbrello de diagramme « UML ».
