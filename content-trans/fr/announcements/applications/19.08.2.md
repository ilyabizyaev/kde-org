---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE publie la version 19.08.2 des applications.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE publie la version 19.08.2 des applications.
version: 19.08.2
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../19.08.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues identifiés, portant sur des améliorations de Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize, Spectacle parmi bien d'autres.

Les améliorations incluses sont :

- La prise en charge de « HiDPI » a été amélioré dans Konsole et d'autres applications.
- Le basculement entre les différentes recherches dans Dolphin permet maintenant une mise à jour correcte des paramètres de recherche.
- KMail peut de nouveau enregistrer des messages directement vers des dossiers distants.
