---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE Ships Applications 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE Ships KDE Applications 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize, Umbrello 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- Ark에서 .tar.zstd 압축 파일을 불러오는 문제 수정
- Plasma 활동을 멈추었을 때 Dolphin이 더 이상 충돌하지 않음
- Filelight에서 다른 파티션으로 전환했을 때 더 이상 충돌하지 않음
