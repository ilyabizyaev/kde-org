---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: KDE, KDE Uygulamalar 16.04 Sürüm Adayını Gönderdi
layout: application
release: applications-16.03.90
title: KDE, KDE Uygulamalar 16.04 Sürüm Adayını Gönderdi
---
7 Nisan 2016. Bugün KDE, KDE Uygulamalarının yeni sürümlerinin yayın adayı yayınladı. Bağımlılık ve özellik donmalarıyla birlikte, KDE ekibinin odak noktası artık hataları düzeltmek ve daha fazla parlatmaktır.

With the various applications being based on KDE Frameworks 5, the KDE Applications 16.04 the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### KDE Uygulamalarını 16.04 Sürüm Adayı İkili Paketlerini Kurma

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 16.04 Release Candidate (internally 16.03.90) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### KDE Uygulamalarını Derleme 16.04 Sürüm Adayı

The complete source code for KDE Applications 16.04 Release Candidate may be <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-16.03.90'>KDE Applications Release Candidate Info Page</a>.
