---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE, KDE Uygulamaları 17.12.1'i Gönderdi
layout: application
title: KDE, KDE Uygulamaları 17.12.1'i Gönderdi
version: 17.12.1
---
January 11, 2018. Today KDE released the first stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Yaklaşık 20 kaydedilen hata düzeltmeleri diğerleri arasında Kontak, Dolphin, Dosya Feneri, Gwenview, K İndir, Okteta, Umbrello, iyileştirmeler içerir.

İyileştirmeler şunları içerir:

- Kontak'ta posta gönderme belirli SMTP sunucuları için düzeltildi
- Gwenview'in zaman çizelgesi ve etiket aramaları iyileştirildi
- Umbrello UML diyagram aracında JAVA içe aktarma işlemi düzeltildi
