---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE Uygulamalar 15.04.1.'i Gönderdi.
layout: application
title: KDE, KDE Uygulamaları 15.04.1'i Gönderdi
version: 15.04.1
---
12 Mayıs 2015. Bugün KDE, <a href='../15.04.0'>KDE Uygulamaları</a> 15.04 için ilk kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than 50 recorded bugfixes include improvements to kdelibs, kdepim, kdenlive, okular, marble and umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.19, KDE Development Platform 4.14.8 and the Kontact Suite 4.14.8.
