---
date: '2014-04-16'
description: KDE stelt Applicaties en Platform 4.13 beschikbaar
hidden: true
title: KDE Applications 4.13 profiteert van het nieuwe semantische zoeken, introductie
  van nieuwe mogelijkheden
---
De KDE gemeenschap is er trots op de nieuwste bijgewerkte KDE Applications aan te kondigen, die nieuwe functies en reparaties levert. Kontact (de persoonlijke informatiebeheerder) is onderwerp geweest van intensieve activiteit en heeft geprofiteerd van de verbeteringen in de semantische zoektechnology van KDE en brengt nieuwe functies. De documentviewer Okular en de geavanceerde tekstbewerker Kate kregen verbeteringen gerelateerd aan het interface aan verbetering van functies. In de hoek van het onderwijs en spelletjes introduceren we de nieuwe uitspraaktrainer van vreemde talen Artikulate; Marble (de wereldbol op het bureaublad) kreeg ondersteuning voor Zon, Maan en planeten, fietsroutes en zeemijlen. Palapeli (de legpuzzel-toepassing) heeft een sprong voorwaarts gemaakt naar niet eerder bereikte nieuwe dimensies en mogelijkheden.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## KDE Kontact introduceert nieuwe functies en meer snelheid

De Kontact-suite van KDE introduceert een serie functies in zijn verschillende componenten. KMail introduceert opslag in de Cloud en verbeterde ondersteuning voor sieve voor filtering aan de kant van de server, KNotes kan nu herinneringen genereren en introduceert zoekfuncties en er zijn vele verbeteringen aan de laag gegevenscache in Kontact, waarmee bijna alle bewerkingen sneller worden.

### Ondersteuning voor opslag in de Cloud

KMail introduceert ondersteuning voor opslagservices, dus kunnen grote bijlagen opgeslagen worden in cloudservices en ingevoegd worden met koppelingen in e-mailberichten. Ondersteunde opslagservices omvatten Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic en er is een generieke WebDav optie. Een hulpmiddel <em>storageservicemanager</em> helpt bij het beheer van bestanden op deze services.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Veel verbeterde ondersteuning van Sieve

Sieve Filters, een technologie die KMail filters op de server laat behandelen, kan nu ondersteuning voor vakantieberichten voor meerdere servers behandelen. Het hulpmiddel KSieveEditor stelt gebruikers in staat om sievefilters te bewerken zonder de server aan Kontact toe te voegen.

### Andere wijzigingen

De snelfilterbalk heeft een kleine verbetering aan het gebruikersinterface gekregen en profiteert zeer van de belangrijk verbeterde zoekmogelijkheden geïntroduceerd in de uitgave van KDE Development Platform 4.13. Zoeken is belangrijk sneller en betrouwbaarder geworden. De opsteller introduceert het inkorten van een URL, als aanvulling op de bestaande hulpmiddelen voor vertaling en tekstfragmenten.

Tags en annotaties van gegevens in PIM worden nu opgeslagen in Akonadi. In volgende versies zullen ze ook opgeslagen worden op servers (in IMAP of Kolab), waarmee het mogelijk wordt gemaakt om tags te delen over meerdere computers. Akonadi: ondersteuning van Google Drive API is toegevoegd. Er is ondersteuning voor zoeken met plug-ins van derden (wat betekent dat resultaten erg snel opgehaald kunnen worden) en serverzoeken (zoeken van items niet in een lokale service voor indexering).

### KNotes, KAddressbook

Belangrijk werk is uitgevoerd op KNotes, reparaties van een serie bugs en kleine vervelende dingen. De mogelijkheid om herinneringen in te stellen op notities is nieuw, evenals zoeken door notities. Lees meer <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>hier</a>. KAdressbook kreeg ondersteuning voor afdrukken: meer details <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>hier</a>.

### Prestatie verbeteringen

De prestaties van Kontact is in deze versie merkbaar verbeterd. Sommige verbeteringen zijn vanwege de integratie met de nieuwe versie van de infrastructuur voor <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Semantisch zoeken</a> en de laag voor gegevenscaching en het laden van gegevens in KMail zelf hebben eveneens een belangrijke hoeveelheid werk ontvangen. Opmerkelijk werk heeft plaatsgevonden om ondersteuning voor de PostgreSQL database te verbeteren. Meer informatie en details over aan prestatie gerelateerde wijzigingen is te vinden via deze koppelingen:

- Opslag optimalisaties: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>sprintrapport</a>
- versnellen en reductie van grootte van database: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>e-maillijst</a>
- optimalisatie in toegang tot mappen: <a href='https://git.reviewboard.kde.org/r/113918/'>review board</a>

### KNotes, KAddressbook

Belangrijk werk is uitgevoerd op KNotes, reparaties van een serie bugs en kleine vervelende dingen. De mogelijkheid om herinneringen in te stellen op notities is nieuw, evenals zoeken door notities. Lees meer <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>hier</a>. KAdressbook kreeg ondersteuning voor afdrukken: meer details <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>hier</a>.

## Okular verfijnt gebruikersinterface

Deze uitgave van de documentviewer Okular brengt een aantal verbeteringen. U kunt nu meerdere PDF-bestanden in één exemplaar van Okular openen dankzij ondersteuning voor tabbladen. Er is een nieuwe muismodus Vergrootglas en de DPI van het huidige scherm wordt gebruikt voor de weergave van de PDF, waardoor de documenten er beter uitzien. Een nieuwe knop Afspelen is ingevoegd in de presentatiemodus en er zijn verbeteringen aan de acties Zoeken en Ongedaan maken/Opnieuw.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate introduceert een verbeterde statusbalk, geanimeerd overeenkomende haakjes, verbeterde plug-ins

De laatste versie van de geavanceerde tekstbewerker Kate introduceert <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>geanimeerde overeenkomst van haakjes</a>, wijzigingen om <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>toetsenborden met ingeschakelde AltGr in vim-modus</a> laten werken en een serie verbeteringen in de plug-ins van Kate, speciaal op het gebied van ondersteuning van Python en de <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>build-plug-in</a>. Er is een nieuwe, veel <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>verbeterde statusbalk</a> die directe acties inschakelt zoals wijziging van de instellingen voor inspringen, codering en accentuering, een nieuwe tabbladbalk in elke weergave, ondersteuning voor code aanvullen voor <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>de programmeertaal D</a> en <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>veel meer</a>. Het team heeft <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>voor terugkoppeling gevraagd over wat verder te verbeteren in Kate</a> en is de aandacht enigszins aan het verschuiven naar het overbrengen naar Frameworks 5.

## Diverse mogelijkheden overal

Konsole brengt enige extra flexibiliteit door eigen stylesheets toe te staan voor instellen van posities van tabs. Profielen kunnen nu gewenste kolom- en rij-afmetingen opslaan. Zie meer <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>hier</a>.

Umbrello maakt het mogelijk om diagrammen te dupliceren en introduceert intelligente contextmenu's die hun inhoud aanpassen aan de geselecteerde widgets. Ondersteuning van ongedaan maken en visuele eigenschappen zijn eveneens verbeterd. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introduceert RAW voorbeeldweergave</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

De mixer voor geluid KMix introduceert afstandsbesturing via het inter-proces communicatieprotocol DBUS, (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>details</a>), toevoegingen aan het geluidsmenu en een nieuwe instellingendialoog (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>details</a>) en een serie van reparaties van bugs en kleinere verbeteringen.

Het interface  voor zoeken van Dolphin is gewijzigd om voordeel te trekken uit de nieuwe infrastructuur voor zoeken en ontving ook verdere verbeteringen aan de prestaties. Voor details, lees dit <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>overzicht van werk aan optimalisatie in het laatste jaar</a>.

KHelpcenter voegt alfabetische sortering toe voor reorganisatie van modules en categorieën om het gebruik makkelijker te maken.

## Spellen en toepassingen voor het onderwijs

De spellen en toepassingen voor het onderwijs van KDE werden bijgewerkt in deze uitgave. De legpuzzeltoepassing van KDE, Palapeli, heeft <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>slimme nieuwe functies</a> ontvangen waarmee het puzzelen met grote puzzels (tot 10.000 stukjes) veel gemakkelijker is voor diegenen die de uitdaging aangaan. KNavalBattle toont de posities van vijandige schepen nadat het spel is geëindigd, zodat u kunt zien waar u de verkeerde weg insloeg.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

De onderwijstoepassingen van KDE hebben nieuwe functies ontvangen. KStars kreeg een interface voor scripts via D-BUS en kan de API voor webservices van astrometry.net gebruiken om geheugengebruik te optimaliseren. Cantor accentuering van syntaxis gekregen in zijn bewerker voor scripts en zijn backends voor Scilab en Python 2 worden nu ondersteund in de editor. De toepassing Marble voor onderwijskundige landkaarten en navigatie bevat nu de posities van de <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Zon, Maan</a> en <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planeten</a> en maakt het mogelijk <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>filmpjes op te nemen bij virtuele trips</a>. Fietsroutes opvragen is verbeterd met de toevoeging van ondersteuning van cyclestreets.net. Nautische mijlen worden nu ondersteund en klikken op een <a href='http://en.wikipedia.org/wiki/Geo_URI'>Geo-URI</a> zal nu Marble openen.

#### KDE-toepassingen installeren

KDE software, inclusief al zijn bibliotheken en toepassingen, zijn vrij beschikbaar onder Open-Source licenties. KDE software werkt op verschillende hardware configuraties en CPU architecturen zoals ARM- en x86-besturingssystemen en werkt met elk soort windowmanager of bureaubladomgeving. Naast Linux en andere op UNIX gebaseerde besturingssystemen kunt u Microsoft Windows versies van de meeste KDE-toepassingen vinden op de site <a href='http://windows.kde.org'>KDE-software op Windows</a> en Apple Mac OS X versies op de site <a href='http://mac.kde.org/'>KDE software op Mac</a>. Experimentele bouwsels van KDE-toepassingen voor verschillende mobiele platforms zoals MeeGo, MS Windows Mobile en Symbian zijn te vinden op het web maar worden nu niet onderssteund. <a href='http://plasma-active.org'>Plasma Active</a> is een gebruikservaring voor een breder spectrum van apparaten, zoals tabletcomputers en andere mobiele hardware.

KDE software is verkrijgbaar in broncode en verschillende binaire formaten uit<a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> en kan ook verkregen worden op <a href='/download'>CD-ROM</a> of met elk van de <a href='/distributions'>belangrijke GNU/Linux en UNIX systemen</a> van vandaag.

##### Pakketten

Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van 4.13.0 voor sommige versies van hun distributie te leveren en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan.

##### Locaties van pakketten

Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Wiki van de gemeenschap</a>.

De complete broncode voor 4.13.0 kan <a href='/info/4/4.13.0'>vrij gedownload worden</a>. Instructies over compileren en installeren van KDE software 4.13.0 zijn beschikbaar vanaf de <a href='/info/4/4.13.0#binary'>4.13.0 informatiepagina</a>.

#### Systeemvereisten

Om het meeste uit deze uitgaven te halen, bevelen we aan om een recente versie van Qt, zoals 4.8.4, te gebruiken. Dit is noodzakelijk om een stabiele ervaring met hoge prestaties te verzekeren omdat sommige verbeteringen, die zijn gemaakt in KDE software, eigenlijk zijn gedaan in het onderliggende Qt-framework.

Om volledige gebruik te maken van de mogelijkheden van de software van KDE, bevelen we ook aan de laatste grafische stuurprogramma's voor uw systeem te gebruiken, omdat dit de gebruikerservaring aanzienlijk verhoogt, beiden in optionele functionaliteit en in algehele prestaties en stabiliteit.
