---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Verbeterde foutmeldingen

### BluezQt

- Geef een expliciete lijst met argumenten door. Dit voorkomt dat QProcess probeert om ons pad met een spatie via een shell te behandelen
- Gemiste wijzigingen in eigenschap onmiddellijk nadat een object is toegevoegd repareren (bug 377405)

### Breeze pictogrammen

- awk mime bijwerken omdat het een scripttaal is (bug 376211)

### Extra CMake-modules

- verborgen-zichtbaarheid herstellen in testen met Xcode 6.2
- ecm_qt_declare_logging_category(): meer unieke invoegbewaking voor header
- Bericht met "Gegenereerd. Niet bewerken" toevoegen of verbeteren en consistent maken
- Een nieuwe FindGperf module toevoegen
- Standaard pkgconfig pad voor installatie voor FreeBSD wijzigen

### KActivitiesStats

- kactivities-stats naar tier3 repareren

### KDE Doxygen hulpmiddelen

- Q_REQUIRED_RESULT niet als sleutelwoord beschouwen

### KAuth

- Verifieer dat wie ons aanroept ook echt is wie hij zegt hij is

### KCodecs

- Uitvoer van gperf genereren op moment van bouwen

### KCoreAddons

- Op de juiste manier per thread seeding in KRandom verzekeren
- Paden van QRC niet bewaken (bug 374075)

### KDBusAddons

- De pid in het dbus pad niet meenemen wanneer op flatpak

### KDeclarative

- Stuur signaal MouseEventListener::pressed consistent uit
- MimeData object niet lekken (bug 380270)

### Ondersteuning van KDELibs 4

- Het hebben van spaties in de paden naar CMAKE_SOURCE_DIR behandelen

### KEmoticons

- Qt5::DBus wordt alleen privé gebruikt repareren

### KFileMetaData

- /usr/bin/env gebruiken om python2 te lokaliseren

### KHTML

- Uitvoer van kentities gperf genereren op moment van bouwen
- Uitvoer van doctypes gperf genereren op moment van bouwen

### KI18n

- Handleiding voor Programmeurs uitbreiden met notities over invloed van setlocale()

### KIO

- Gewerkt aan een probleem waar bepaalde elementen in toepassingen (bijv. bestandsweergave in Dolphin) ontoegankelijk worden in een opzet met meerdere schermen met hoge dpi (bug 363548)
- [RenameDialog] forceer platte tekstformaat
- Identificeer PIE binaire bestanden (application/x-sharedlib) als uitvoerbare bestanden (bug 350018)
- core: expose GETMNTINFO_USES_STATVFS in configuratieheader
- PreviewJob: skip mappen op afstand. Te duur om te bekijken (bug 208625)
- PreviewJob: schoon leeg tijdelijk bestand op wanneer get() mislukt (bug 208625)
- Gedetailleerde boomstructuurweergave versnellen door te veel in grootte wijzigen van kolommen

### KNewStuff

- Een enkele QNAM gebruiken (en een cache op schijf) voor HTTP jobs
- Interne cache voor gegevens van provider bij initialisatie

### KNotification

- KSNI's die geen service kunnen registreren onder flatpak repareren
- Naam toepassing gebruiken in plaats van pid bij aanmaken van SNI dbus-service

### KPeople

- Geen symbolen uit privé bibliotheken exporteren
- Exporteren van symbolen voor KF5PeopleWidgets en KF5PeopleBackend repareren
- #warning naar GCC beperken

### KWallet Framework

- kwalletd4 vervangen nadat migratie is beëindigd
- Gereed-zijn signaleren van migratieagent
- Timer voor migratie-agent alleen starten indien noodzakelijk
- Zo vroeg mogelijk op uniek exemplaar van toepassing controleren 

### KWayland

- requestToggleKeepAbove/below toevoegen
- QIcon::fromTheme in hoofdthread houden
- Pid changedSignal in Client::PlasmaWindow verwijderen
- Pid aan plasma vensterbeheerprotocol toevoegen

### KWidgetsAddons

- KViewStateSerializer: crash repareren wanner weergave is verdwenen vóór statusserializer (bug 353380)

### KWindowSystem

- Betere reparatie voor NetRootInfoTestWM in pad met spaties

### KXMLGUI

- Stel het hoofdvenster in als ouder van alleenstaande pop-up-menu's
- Bij bouwen van hiërarchieën in menu's, maak hun containers ouder hiervan

### Plasma Framework

- systeemvakpictogram voor VLC toevoegen
- Plasmoid-sjablonen: de afbeelding gebruiken die onderdeel is van het pakket (opnieuw)
- Sjabloon voor Plasma QML Applet met QML extensie toevoegen
- Plasmashellsurf opnieuw maken bij vertonen, weggooien bij verborgen

### Accentuering van syntaxis

- Haskell: "julius" quasiquoter accentueren met Normal##Javascript regels
- Haskell: hamlet accentuering inschakelen ook voor "shamlet" quasiquoter

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
