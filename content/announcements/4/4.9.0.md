---
aliases:
- ../4.9
custom_about: true
custom_contact: true
date: '2012-12-05'
description: KDE Ships 4.9.4 Workspaces, Applications and Platform.
title: KDE Release 4.9 – in memory of Claire Lotion
---

<p>
<img src="/stuff/clipart/klogo-official-oxygen-128x128.png" class="float-left m-2" />KDE is delighted to announce its latest set of releases, providing major updates to <a href="./plasma">KDE Plasma Workspaces</a>, <a href="./applications">KDE Applications</a>, and the <a href="./platform">KDE Platform</a>. Version 4.9 provides many new features, along with improved stability and performance.
</p>
<p>
This release is dedicated to the memory of KDE contributor <a href="http://dot.kde.org/2012/05/20/remembering-claire-lotion">Claire Lotion</a>. Claire's vibrant personality and enthusiasm were an inspiration to many in our community, and her pioneering work on the format, scope and frequency of our developer meetings changed the way we go about implementing our mission today. Through these and other activities she left a notable mark on the software we are able to release to you today, and we are grateful for and humbled by her efforts.</p>

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-desktop.png">
	<img src="/announcements/4/4.9.0/kde49-desktop-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

<p>
The KDE Quality Team was set up earlier this year with a goal to improve the general levels of quality and stability in KDE software. Special attention was given to identifying and fixing regressions from previous releases. This was a top priority because it ensures improvement with each release.
</p>
<p>
The Team also set up a more rigorous testing process for releases starting with beta versions. New testing volunteers received training; and several testing intensive days were held. Rather than traditional exploratory testing, testers were assigned to focus on specific areas that had changed since previous releases. For several critical components, full testing checklists were developed and used. The team found many important issues early and worked with developers to make sure they were fixed. The Team itself reported over 160 bugs with the beta and RC releases, many of which have now been fixed. Other beta and RC users added considerably to the number of bugs reported. These efforts are important because they allow developers to concentrate on fixing issues.
</p>
<p>
As a result of the efforts of the KDE Quality Team, the 4.9 Releases are the best ever.
</p>
<p>
One particular bugfix deserves special attention. An Okular bug reported in 2007 had gotten nearly 1100 votes; it was important to many users. They complained about making annotations and not being able to save or print them. With the assistance of many commenters and people on the Okular IRC channel, Fabio D’Urso implemented a solution that allows Okular PDF document annotations to be saved and printed. The fix required some work on KDE libraries and attention to overall design to ensure that non-PDF documents worked right. This was Fabio’s first KDE dev experience, which came about when he encountered the problem and decided to do something about it.
</p>
<h2><a href="./plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.9" /> Plasma Workspaces 4.9 – Core Improvements</a></h2>
<p>
Highlights for Plasma Workspaces include substantial improvements to the Dolphin File Manager, Konsole X Terminal Emulator, Activities, and the KWin Window Manager. Read the complete <a href="./plasma">'Plasma Workspaces Announcement'.</a>
</p>
<h2><a href="./applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.9"/>New and Improved KDE Applications 4.9</a></h2>
<p>
New and improved KDE Applications released today include Okular, Kopete, KDE PIM, educational applications and games. Read the complete <a href="./applications">'KDE Applications Announcement'</a>
</p>
<h2><a href="./platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.9"/> KDE Platform 4.9</a></h2>
<p>
Today’s KDE Platform release includes bugfixes, other quality improvements, networking, and preparation for Frameworks 5
</p>
<h2>Spread the Word and See What's Happening: Tag as "KDE"</h2>
<p>
KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with "KDE". This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.9 releases of KDE software.
</p>
<h2>Release Parties</h2>
<p>
As usual, KDE community members organize release parties all around the world. Several have already been scheduled and more will come later. Find <a href="http://community.kde.org/Promo/Events/Release_Parties/4.9">a list of parties here</a>. Anyone is welcome to join! There will be a combination of interesting company and inspiring conversations as well as food and drinks. It's a great chance to learn more about what is going on in KDE, get involved, or just meet other users and contributors.
</p>
<p>
We encourage people to organize their own parties. They're fun to host and open for everyone! Check out <a href="http://community.kde.org/Promo/Events/Release_Parties">tips on how to organize a party</a>.
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.9/&amp;title=KDE%20releases%20version%204.9%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="/announcements/4.9/" data-text="#KDE releases version 4.9 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.9/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.9%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="/announcements/4.9/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde49"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde49"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde49"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde49"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2>About these release announcements</h2>
<p>
These release announcements were prepared by Jos Poortvliet, Carl Symons, Valorie Zimmerman, Jure Repinc, David Edmundson, and other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.
</p>

<h4>Support KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">Supporting Member programme</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.</p>
<br />
<p>&nbsp;</p>
