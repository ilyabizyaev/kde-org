---
custom_about: true
custom_contact: true
hidden: true
title: Applications Leap Forward
---

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../applications">
                <img src="/announcements/4/4.4.0/images/applications-32.png" />
                Previous Page: Basic Applications
                </a>
        </td>
        <td align="right" width="50%">
                <a href="../edu_games">Next page: Educational Applications and Games
                <img src="/announcements/4/4.4.0/images/education-32.png" /></a>
        </td>
    </tr>
</table>


<h2>KDE Personal Informational Management</h2>

<p>
The KDE PIM team strives to provide an application suite to manage personal information, including mail, time, people and more. The main result is KDE Kontact, our personal information manager which gathers several applications in one convenient interface. A major focus of the team for the KDE PIM 4.4 release has been on the underlying infrastructure, Akonadi, however there has also been time to work on a variety of smaller features. Some of those are described below.</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kontact.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kontact.png" class="img-fluid">
	</a> <br/>
	<em>Kontact gathers the KDE PIM applications in one convenient interface</em>
</div>
<br/>

<h3>Send KMail to your friends</h3>

<p>
Thomas McGuire has added an archive function, allowing you to save a folder of email messages (such as your inbox and all its subfolders) into a single compressed archive file. To archive a folder, you right-click the folder, and choose "Archive Folder." A popup menu appears, giving you a choice of archive format (tar.gz, tar.bz2 etc.) and the save-to location. To restore from an archive, you click the File Menu, and choose "Import Messages". This allows other email clients to read these messages after extracting them, because of the way the archive is structured.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kmail_archive.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kmail_archive.png" class="img-fluid">
	</a> <br/>
	<em>Archiving folders in Kmail</em>
</div>
<br/>

<p>
KMail now has a more powerful tagging feature, thanks to Jonathan Armond. You can search your email messages with tags in addition to searching by content, sender, or other criteria. You can also use filters to add tags automatically to messages that match your rules.
</p>

<h3>Gather your contacts in KAddressBook</h3>

<p>
Tobias Konig has entirely rewritten KAddressBook. The new KAddressBook is the first of the three central PIM applications to use the Akonadi framework to access and organize contact information. Nepomuk connects with Akonadi, making this information available to other Nepomuk-aware applications. The move to the new Akonadi technology might introduce some issues which only appear when it is deployed on a large scale, so we caution users to be careful. However, much time has been spend on making sure no data will get lost and we expect these features to mature by the time the distributions start shipping KDE PIM 4.4 in their stable versions over the coming months.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kaddressbook.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kaddressbook.png" class="img-fluid">
	</a> <br/>
	<em>Adding a contact in KAddressBook</em>
</div>
<br/>


<h3>Wake up with KAlarm</h3>

<p>
KAlarm, the KDE PIM tool to quickly set and manage alarms, became a bit more flexible. Davit Jarvie made sure you can set it to only provide audio notification if you so desire.
</p>

<h2>Network Applications</h2>

<p>
The KDE applications for personal information management are nicely complemented thanks to the team working on networking tools. Helping you to connect and take advantage of the web, they developed a chat application, easy to use remote desktop tools, a powerful download manager and more.
</p>

<h3>Chat with Kopete</h3>
<p>
Kopete is a multi-protocol chat client, so you can chat with anyone, no matter what network they are on - MSN, ICQ, Jabber and many more networks are supported. Thiago Herrmann improved the MSN support in Kopete by adding various features like support for ink, voice clips, proxies and emoticons. He also improved the contact list. Thanks to Alex Fiestas, Kopete can now use your webcam device directly to take a picture and use that as the avatar for your I.M. account. ICQ now works with Rich Text like bold, italics and other properties thanks to the work by Roman Jarosz and Pali Rohár added support for Google Audio and Video Chat back in Kopete.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_kopete.png">
	<img src="/announcements/4/4.4.0/thumbs/44_kopete.png" class="img-fluid">
	</a> <br/>
	<em>Kopete in action</em>
</div>
<br/>


<h3>KGet stuff from the web</h3>

<p>
KGet is a surprisingly powerful download manager. While this application has been around for ages, doing its work in the background and making sure your downloads finish properly even when interupted, lately new features have been added. This includes bittorrent support, multi-source downloading and many usability improvements.
</p>

<p>
Matthias Fuchs worked on multisource-downloading. This means you can add multiple mirrors to a transfer. Transfers can be verified and repaired, renamed and moved on the filesystem while downloading. The Internet can be automatically searched for checksums to verify the integrity of downloads. The MetalinkCreator makes it possible to create your own metalinks--files that contain information about multiple sources where a certain file can be downloaded. This makes it possible for others to download your files from several locations simultaneously for faster download.
</p>

<h3>Remote desktops with Krfb and Krdc</h3>

<p>
George Goldberg has taken up maintainership for Krfb, a tool for Desktop Sharing. Most major bugs have been resolved that prevented it from running properly in previous Software Compilations.
</p>

<p>
Urs Wolfer and Tony Murray made some work flow improvements to the KDE Remote Desktop Client (Krdc) that make creating new connections and managing multiple connections easier. Now you can create new connections more quickly and change between remote connections without ever leaving the full screen view.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/44_krdc.png">
	<img src="/announcements/4/4.4.0/thumbs/44_krdc.png" class="img-fluid">
	</a> <br/>
	<em>Krdc bookmarks support</em>
</div>
<br/>


<h3>Konquer the Web</h3>

<p>
Konqueror, KDE's web browser has received many improvements in both its interface and its rendering engine, KHTML. Session management has been enhanced by Eduardo Robles Elvira making it much easier to pick up a browsing session where you left it. You can now go to File -> Sessions and save or open sessions. David Faure build upon the work by Pino Toscano to bring back the history sidebar, a much loved and dearly missed feature from the KDE 3.x days. David also worked on a variety of other features, including re-introducing the "create symbolic link" in the "Create new" menu in the filebrowsing part (this also benefits Dolphin) and improved tab bar handling. And if Konqueror happened to experience a crash and offers to recover the open webpages, it now shows the titles of those pages.
</p>
<p>
Another improvement in Konqueror comes in KHTML's handling of WYSIWYG (What You See Is What You Get) interfaces, common in blogging websites. Vyacheslav Tokarev has been working hard on fixing issues to make the code more stable and add a few missing features. Extensive refactoring of the code has fixed crashes and will make it easier to develop in the future.
</p>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="../applications">
                <img src="/announcements/4/4.4.0/images/applications-32.png" />
                Previous Page: Basic Applications
                </a>
        </td>
        <td align="right" width="50%">
                <a href="../edu_games">Next page: Educational Applications and Games
                <img src="/announcements/4/4.4.0/images/education-32.png" /></a>
        </td>
    </tr>
</table>