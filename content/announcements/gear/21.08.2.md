---
date: 2021-10-07
appCount: 120
image: true
layout: gear
---
+ kmail: Fix an  infinite SSL error dialog loop [Commit,](http://commits.kde.org/kdepim-runtime/edb7f6fdea2c9f44085a042531f56223f3fd8a2f) [Commit,](http://commits.kde.org/kimap/7ee241898bc225237b3475f6c109ffc55a4a74c0) [Commit,](http://commits.kde.org/ksmtp/fca378d55e223944ce512c9a8f8b789d1d3abcde) fixes [#423424](https://bugs.kde.org/423424)
+ konqueror: Make it compatible with KIO 5.86.0 and don't open every URL in a new window [Commit,](http://commits.kde.org/konqueror/8506c585594d9d0cfc0ebe8b869ca05ff7610fa7) fixes [#442636](https://bugs.kde.org/442636)
+ libksane: Fix multi page detection with certain scanners [Commit](http://commits.kde.org/libksane/a90b83faea5beaaab346ee6eff2f151581783beb)
