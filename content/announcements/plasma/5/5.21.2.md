---
date: 2021-03-02
changelog: 5.21.1-5.21.2
layout: plasma
youtube: ahEWG4JCA1w
asBugfix: true
---

+ Discover: Flatpak: Support querying by mimetype. [Commit.](http://commits.kde.org/discover/9c694b41283e026e4c938e768aa39f93e669f5a3) Fixes bug [#433291](https://bugs.kde.org/433291)
+ KDE GTK Config: Support svgz buttons in Aurorae themes. [Commit.](http://commits.kde.org/kde-gtk-config/cbfb855ac2f50f91fb39085a337e3151780c861a) Fixes bug [#432712](https://bugs.kde.org/432712)
+ KRunner: Handle escape key in history view more gracefully. [Commit.](http://commits.kde.org/plasma-workspace/c4db9bd1e960e3ec7b6c460441ee622c4a582fc9) Fixes bug [#433723](https://bugs.kde.org/433723)
