---
title: "Plasma 5.26"
subtitle: "All About the Widgets"
description: "Plasma 5.26 comes with new and tweaked widgets, improves the desktop experience leaps and bounds, and Plasma Big Screen's app family grows"
date: "2022-10-11"
images:
 - /announcements/plasma/5/5.26.0/fullscreen_with_apps.png
layout: plasma-5.26
scssFiles:
- /scss/plasma-5-26.scss
authors:
- SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2022 Aniqa Khokhar <aniqa.khokhar@kde.org>
SPDX-License-Identifier: CC-BY-4.0
highlights:
  title: Highlights
  items:
  - header: Dark Themes > Dark Wallpapers
    text: When you change themes, so does your wallpaper
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.26/01_light_dark_wallpapers_02_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.26/01_light_dark_wallpapers_02_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.26/01_light_dark_wallpapers_02_720p.webm
    hl_class: overview
  - header: Animated Wallpapers
    text: Make it move!
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.26/02_animated_wallpapers_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.26/02_animated_wallpapers_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.26/02_animated_wallpapers_720p.webm
    hl_class: hl-krunner
  - header: Big Screen
    text: Two more apps for your Plasma (on a) TV
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Plasma/5.26/03_big_screen_720p.png
      mp4: https://cdn.kde.org/promo/Announcements/Plasma/5.26/03_big_screen_720p.mp4
      webm: https://cdn.kde.org/promo/Announcements/Plasma/5.26/03_big_screen_720p.webm
    hl_class: fingerprint
draft: false
---
{{< container class="text-center" >}}

Even with a bare-bones installation, Plasma lets you customize your desktop a lot. If you want more, there is always Plasma's vast ecosystem of **widgets**. Widgets add features and utilities to the Plasma desktop and today you can find out all the stuff you can do and what's new for widgets in Plasma 5.26.

Widgets are not the only thing to look forward to in Plasma 5.26: check out all the new stuff landing on the desktop designed to make using Plasma easier, more accessible and enjoyable, as well as the two new utilities for Plasma Big Screen, KDE's interface for smart TVs.

{{< youtube id="tTM3s1Zl8p0" >}}

{{< /container >}}

{{< highlight-grid >}}

{{< container >}}

## What's New

### Widgets

*Addons*, *widgets*, *plasmoids*... They all refer to the same thing. The clock and calendar in your panel, the notifier, your KDE Connect monitor, the volume control; these are all widgets, and all can be added elsewhere, moved around, removed and, in true Plasma fashion, modified to an extreme degree.

You could always resize, and even rotate, your widgets floating on your desktop...

{{< alert color="success" icon="tip" >}}

Try it!: click and hold on any widget on your desktop and the controls will appear. Pull the handles to resize, grab and pull the bar to rotate.

{{< /alert >}}

... But now you can also resize the widgets you have in your panel. Click on the digital clock, for example, hover your cursor over the edges or corners of the calendar, click, hold and pull until it is the size you want. The same goes for network manage, volume control, clipboard, etc.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.26/resize_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.26/resize_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.26/resize_720p.png" fig_class="mx-auto max-width-800" >}}

There's a brand new **Timer** widget that ships with Plasma 5.26. Drop it onto your desktop, set the time for your boiled egg, coding session, or cup of tea and it will pop up a notification when it is done.

You can also add an action! Add

```
killall -s HUP firefox
```

to the *Execute command* text box, you will find in the *Advanced* tab in the settings, set the timer to 30 minutes, and **Timer** will stop you from spending all your day on Reddit! Or use SOX's `play` command to alert you with an audio tone when the time is up.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.26/timer_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.26/timer_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.26/timer_720p.png" fig_class="mx-auto max-width-800" >}}

The **Dictionary** widget now lets you select more than one dictionary when looking for a definition or a translation of a word,  and other widgets, like the *Sticky notes*, *User switcher* and *Media Player* all incorporate new features or tweaks to make them easier for the user.

You can also add widgets created by users, like the new (and very cool-looking) **Control Centre**, which gives you a brief overview of several services in your system, like the volume, media being played, KDE Connect, and more. Just right click on your panel, pick *Add widgets...* and look for it in *Get new widgets...*.

{{< /container >}}

{{< twilight >}}

### Workspace

Of the more eye-catching novelties in 5.26, checkout the two new applications landing in [**Plasma Big Screen**](https://plasma-bigscreen.org/), KDE's Plasma for smart TVs — or dumb TVs with a Raspberry Pi attached.

**Aura** is a browser specially designed for Big Screen. Its big, clearly-labeled tiles allow you to navigate the world wide web using your remote control all the way over from your sofa. You can use a compatible remote control (and Big Screen supports more and more remotes with every new version) similar to a laser pointer to "click" on links and surf the web.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.26/aura_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.26/aura_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.26/aura_720p.png" fig_class="mx-auto max-width-800" >}}

The second app is **Plank Player**, a simple and easy-to-use media player which will allow you to play videos from a storage device you plug into your TV.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.26/plank_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.26/plank_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.26/plank_720p.png" fig_class="mx-auto max-width-800" >}}

On regular desktop Plasma, things have also been moving hard and fast. Developers push to make Plasma excellent on the next generation display protocol has hit a new landmark and scaled XWayland apps now look beautiful, sharp and clear on Wayland.

And now, it is not only the windows decorations that change when you move from a light theme to a dark theme:  the wallpaper will change as well! Check out the highlight video above to see how this spectacular improvement looks like.

Talking of light and dark, you have been able to adjust the warmth of the colors of your desktop for some time now, using warmer, reddish colors at night (better for your eyes) and having Plasma adjust the color automatically when the sun goes down. New in Plasma 5.26 is that you can now do that for the day time too.

On the whole, the Plasma 5.26 workplace gets more pleasant (and safer) to use with every new version. The "15-minute bugs" project (that aims to solve the bugs and paper-cuts a new user may encounter in the first 15 minutes of using Plasma) is paying off, and things like animating calendar components and the virtual desktop pager, or showing the title of a playing song in the panel makes it easier to follow what is going on.

Being able to dismiss notifications popups just by clicking on them with the middle button and forcing the activation of a virtual keyboard in any application improves usability and accessibility; and warning you before you open an executable file boosts safety, as it can stop you from running a hazardous program unintentionally.

{{< /twilight >}}

{{< container class="text-center" >}}

### ... And then there is this:

* In the stuff-I-didn't-know-I-wanted-to-change-but-now-I-do department, you can now change the font size of the **digital clock**, and configure the volume step in the **volume controller**.

* More things you can change: remove the icon from the application launcher/main menu/**Kickoff** widget and make it use a text (say, *Applications*) instead... Or make it use both, your choice. This is Plasma after all! A text only label for the launcher looks especially good when used in a top panel and can be helpful for new users having trouble finding their way around.

* Finally, talking of **Kickoff**, you can now easily navigate the *All Applications* section easily using an alphabetized index.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.26/launcher_index_720p.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.26/launcher_index_720p.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.26/launcher_index_720p.png" fig_class="mx-auto max-width-800" >}}

... And there's much more going on. If you would like to see the full list of changes, [check out the changelog for Plasma 5.26](/announcements/changelogs/plasma/5/5.25.5-5.26.0).

{{< /container >}}
