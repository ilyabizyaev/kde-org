---
date: 2021-04-06
changelog: 5.21.3-5.21.4
layout: plasma
youtube: ahEWG4JCA1w
asBugfix: true
draft: false
---

+ KScreen: Prefer "21:9" over "64:27" aspect ratio. [Commit.](http://commits.kde.org/kscreen/20cf8b99a2b23dbbb535a67dce3f8c4e8ffb729f) 
+ Fix broken keyboard configurations with single layout on Wayland. [Commit.](http://commits.kde.org/plasma-desktop/ff875141a263321ba41a38a45ff0f57c6d63535f) 
+ Fix color scheme preview. [Commit.](http://commits.kde.org/plasma-workspace/286edc6768e90d07947fad50db911f0c72e3759f) Fixes bug [#434493](https://bugs.kde.org/434493)
