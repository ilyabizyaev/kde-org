---
aliases:
- ../../kde-frameworks-5.2.0
date: '2014-09-12'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- reimplementation of the file item plugin for linking files to activities

### KArchive

- fix handling of uncompressed files

### KConfigWidgets

- fix missing default shortcuts for standard actions, leading
  to many runtime warnings
- better support for QGroupBox in KConfigDialogManager

### KCoreAddons

- Mark KAboutData::setProgramIconName() as deprecated, it did not
  do anything. Use QApplication::setWindowIcon(QIcon::fromTheme("...")) instead.
- new classes Kdelibs4ConfigMigrator and KPluginMetaData

### KDeclarative

- added org.kde.kio component.

### KImageFormats

- disable the DDS and JPEG-2000 plugins when Qt version is 5.3
  or later

### KIO

- now follows the mime-apps spec, for better interoperability
  with gio when it comes to the user's preferred and default apps.
- new classes EmptyTrashJob and RestoreJob.
- new functions isClipboardDataCut and setClipboardDataCut.

### KNewStuff

- installing "stuff" works again (porting bug)

### KWidgetsAddons

- new class KColumnResizer (makes it easy to vertically align widgets across groups)

### KWindowSystem

- New method KWindowSystem::setOnActivities

### KXmlGui

- KActionCollection::setDefaultShortcuts now makes the shortcut
  active too, to simplify application code.

### Threadweaver

- The maximum worker count will now decrease if a lower value is set
  after workers have been created. Previously, workers would remain active
  once they have been created.
- Examples from the previous ThreadWeaverDemos Github repository are
  being merged into the KF5 ThreadWeaver repo.
- The maximum worker count can now be set to zero (the previous minimum
  was 1). Doing so will effectively halt processing in the queue.
- Documentation of various aspects of ThreadWeaver use is becoming part
  of the KDE Frameworks Cookbook. Parts of it is located in the examples/
  directory.

### Buildsystem changes

- Support for relative libexec dir.

### Frameworkintegration

- the file dialog now remembers its size correctly, and works better with remote URLs.

## Getting started
You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
