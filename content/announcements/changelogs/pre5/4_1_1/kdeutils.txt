------------------------------------------------------------------------
r837189 | scripty | 2008-07-24 07:54:57 +0200 (Thu, 24 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r837424 | kossebau | 2008-07-24 18:47:02 +0200 (Thu, 24 Jul 2008) | 1 line

changed: add listing of all projects with links to their homepage
------------------------------------------------------------------------
r839591 | kossebau | 2008-07-30 13:35:55 +0200 (Wed, 30 Jul 2008) | 1 line

updated: next version is 0.1.1
------------------------------------------------------------------------
r839627 | kossebau | 2008-07-30 14:41:43 +0200 (Wed, 30 Jul 2008) | 3 lines

backport of 839596: fixed: all inserts, like from clipboard or tools, in overwrite mode did not place the cursor at the end of the inserted data


------------------------------------------------------------------------
r839903 | scripty | 2008-07-31 06:51:03 +0200 (Thu, 31 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r840118 | kossebau | 2008-07-31 13:58:14 +0200 (Thu, 31 Jul 2008) | 2 lines

backport of 840116: fixed: on revert the applied changesdata _de_creases, not increases

------------------------------------------------------------------------
r840175 | kossebau | 2008-07-31 17:01:20 +0200 (Thu, 31 Jul 2008) | 2 lines

backport of 840173: fixed: removing the last part of a single piece has filled garbage in the undo data

------------------------------------------------------------------------
r840212 | kossebau | 2008-07-31 18:59:55 +0200 (Thu, 31 Jul 2008) | 2 lines

backport of 840211: fixed: overwriting in value column starts with wrong shadow number of inserted digits

------------------------------------------------------------------------
r840241 | kossebau | 2008-07-31 20:30:19 +0200 (Thu, 31 Jul 2008) | 2 lines

backport of 840240: fixed: undo of the remove of the end of a piece inserted this two times

------------------------------------------------------------------------
r840269 | kossebau | 2008-07-31 21:27:52 +0200 (Thu, 31 Jul 2008) | 2 lines

backport of 840268: fixed: changemetrics of groups were not reverted if needed

------------------------------------------------------------------------
r840361 | mueller | 2008-08-01 00:48:40 +0200 (Fri, 01 Aug 2008) | 1 line

fix link reduction fallout
------------------------------------------------------------------------
r841697 | aacid | 2008-08-03 23:38:53 +0200 (Sun, 03 Aug 2008) | 2 lines

Backport r841696 kgpg/trunk/KDE/kdeutils/kgpg/conf_ui2.ui: make the first tab be the shown one to user

------------------------------------------------------------------------
r842166 | dakon | 2008-08-04 22:16:19 +0200 (Mon, 04 Aug 2008) | 1 line

fix logging file encryption errors
------------------------------------------------------------------------
r842191 | dakon | 2008-08-04 23:01:59 +0200 (Mon, 04 Aug 2008) | 1 line

another bunch of "do not delete objects in slots they called" fixes
------------------------------------------------------------------------
r842468 | kossebau | 2008-08-05 13:17:19 +0200 (Tue, 05 Aug 2008) | 1 line

fixed: never disconnected from old view (due to misplaced assignement)
------------------------------------------------------------------------
r842701 | dakon | 2008-08-05 21:55:31 +0200 (Tue, 05 Aug 2008) | 4 lines

Fix showing key generate window after running wizard. Now it is shown if there is no public key available instead of only when one or more are there.

BUG:167959

------------------------------------------------------------------------
r842744 | dakon | 2008-08-05 23:00:00 +0200 (Tue, 05 Aug 2008) | 2 lines

You already know them: some more "do not delete objects in slots they called" fixes

------------------------------------------------------------------------
r843156 | kossebau | 2008-08-06 17:14:09 +0200 (Wed, 06 Aug 2008) | 2 lines

SVN_SILENT mark unused variable as such

------------------------------------------------------------------------
r843158 | kossebau | 2008-08-06 17:19:39 +0200 (Wed, 06 Aug 2008) | 2 lines

backport of 843157: fixed: detection of need for scrolling by drag

------------------------------------------------------------------------
r843594 | mlaurent | 2008-08-07 13:48:19 +0200 (Thu, 07 Aug 2008) | 2 lines

Allow to show doc in khelpcenter

------------------------------------------------------------------------
r844052 | metellius | 2008-08-08 16:47:04 +0200 (Fri, 08 Aug 2008) | 1 line

Backport of 836960, Added progress feedback to libarchivehandler
------------------------------------------------------------------------
r844057 | metellius | 2008-08-08 16:54:45 +0200 (Fri, 08 Aug 2008) | 1 line

Backport of 838056, will attempt to use unrar-free if unrar is not available
------------------------------------------------------------------------
r844063 | metellius | 2008-08-08 17:22:42 +0200 (Fri, 08 Aug 2008) | 1 line

Backport of 841290, Rar plugin properly handles utf8 filenames. Patch from Vladimir Kulev
------------------------------------------------------------------------
r844064 | metellius | 2008-08-08 17:24:39 +0200 (Fri, 08 Aug 2008) | 1 line

Backport of 841291, UTF handling for libarchivehandler. Patch by Vladimir Kulev
------------------------------------------------------------------------
r844104 | dakon | 2008-08-08 21:17:19 +0200 (Fri, 08 Aug 2008) | 6 lines

Reorder key trust values

TRUST_UNKNOWN is not the least possible trust value, rendering the default trust filter level non-intuitive as it hides most freshly imported keys.

BUG:168389

------------------------------------------------------------------------
r844154 | metellius | 2008-08-09 01:40:26 +0200 (Sat, 09 Aug 2008) | 12 lines

Backport of the following commits from ark trunk:
841303, BUG: 168042"
842035, CCBUG: 166986"
842292, Be sure to delete it"
842549, CCBUG: 166986"
842855, Set columns to autoresize"
842856, BUG: 168414"
842913, BUG: 167018"
843389, A small bit of reordering in header files to avoid compile error"
843390, Fixed libzip plugin not extracting if not listed first."


------------------------------------------------------------------------
r844371 | dakon | 2008-08-09 16:23:38 +0200 (Sat, 09 Aug 2008) | 1 line

hide key groups if "show only secret keys" is selected
------------------------------------------------------------------------
r844409 | dakon | 2008-08-09 18:34:46 +0200 (Sat, 09 Aug 2008) | 1 line

some more "do not delete objects in slots they called" fixes (will there ever be an end?)
------------------------------------------------------------------------
r844528 | dakon | 2008-08-09 23:26:07 +0200 (Sat, 09 Aug 2008) | 1 line

do not delete KGpgTransaction in slot called from it
------------------------------------------------------------------------
r844661 | scripty | 2008-08-10 07:03:39 +0200 (Sun, 10 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r845032 | scripty | 2008-08-11 07:03:31 +0200 (Mon, 11 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r845454 | dakon | 2008-08-11 19:49:15 +0200 (Mon, 11 Aug 2008) | 2 lines

do not add " (Default)" to a keyserver that is already the default one

------------------------------------------------------------------------
r847997 | dakon | 2008-08-16 17:02:20 +0200 (Sat, 16 Aug 2008) | 1 line

and some more "do not delete objects from slots they called"
------------------------------------------------------------------------
r848099 | dakon | 2008-08-17 01:02:16 +0200 (Sun, 17 Aug 2008) | 4 lines

When keys are signed clear the list of items to refresh. After next keysigning the old items would be tried to refresh again.

BUG:169111

------------------------------------------------------------------------
r849474 | dakon | 2008-08-19 17:57:05 +0200 (Tue, 19 Aug 2008) | 1 line

remove unnecessary platform ifdef
------------------------------------------------------------------------
r849630 | dakon | 2008-08-19 22:23:13 +0200 (Tue, 19 Aug 2008) | 1 line

work around GnuPG weirdness that prevents decryption of signed and decrypted at the same time
------------------------------------------------------------------------
r850164 | dakon | 2008-08-20 23:30:03 +0200 (Wed, 20 Aug 2008) | 1 line

krazy fixes
------------------------------------------------------------------------
r852263 | brandybuck | 2008-08-25 19:00:22 +0200 (Mon, 25 Aug 2008) | 3 lines

Removed HAVE_L_FUNC and dependent macros. They were being used incorrectly and
resulted in reduced accuracy. Backport from trunk.

------------------------------------------------------------------------
r852751 | metellius | 2008-08-26 16:13:53 +0200 (Tue, 26 Aug 2008) | 12 lines

Backport of the following fixes:
844294, Implemented adding files for rar plugin, ass well as reading percentage off stdout
844569, Figured it was about to add myself as the maintainer in the about dialog :)
847692, The mainwindow will open archive files dropped onto it
847705, BUG: 168020
847716, Made the icon in the preview dialog draggable, for viewing in your favorite app
847723, BUG: 162362
847739, Added checking for mimetype by looking at file content, used if mimetype by path does not succeed.
848127, BUG: 168020
849401, BUG: 155220


------------------------------------------------------------------------
r853717 | aucahuasi | 2008-08-28 08:54:44 +0200 (Thu, 28 Aug 2008) | 4 lines

BUG: 140630

Closed, I'm backporting the change to KDE 4.1.1 ...

------------------------------------------------------------------------
