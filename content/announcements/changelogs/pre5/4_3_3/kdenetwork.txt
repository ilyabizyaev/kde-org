------------------------------------------------------------------------
r1030890 | mzanetti | 2009-10-03 12:38:42 +0000 (Sat, 03 Oct 2009) | 4 lines

backport of bugfix commit 1030885 (patch by Florian Reinhard)
Fixes file transfers in OTR sessions


------------------------------------------------------------------------
r1031772 | rkcosta | 2009-10-05 23:57:56 +0000 (Mon, 05 Oct 2009) | 4 lines

Backport r1021725.

SVN_SILENT compile

------------------------------------------------------------------------
r1033346 | mzanetti | 2009-10-09 22:28:12 +0000 (Fri, 09 Oct 2009) | 2 lines

backport of bugfix commit 1033343

------------------------------------------------------------------------
r1033390 | mattr | 2009-10-10 03:08:23 +0000 (Sat, 10 Oct 2009) | 5 lines

Fix setting user mood to 'None'

Patch by Romain GUINOT. Thanks!
This is the KDE 4.3 version of the patch.
CCBUG: 111329
------------------------------------------------------------------------
r1033860 | mfuchs | 2009-10-11 11:48:23 +0000 (Sun, 11 Oct 2009) | 4 lines

Backport of r1023319
Always add the trailing slash to download folder locations when changed in the group settings dialog.

CCBUG: 204666
------------------------------------------------------------------------
r1034343 | pali | 2009-10-12 13:42:06 +0000 (Mon, 12 Oct 2009) | 4 lines

fix quit skype
fix moving contact to root group at start
fix web action handler

------------------------------------------------------------------------
r1040315 | scripty | 2009-10-26 04:17:32 +0000 (Mon, 26 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1041838 | bbigras | 2009-10-28 16:14:30 +0000 (Wed, 28 Oct 2009) | 6 lines

Backport r1039125 by bbigras from trunk to the 4.3 branch:

Fix a bug with WLM that new groups were not created on the server
so contacts were not able to be moved into those groups
CCBUG: 190353

------------------------------------------------------------------------
r1041845 | bbigras | 2009-10-28 16:35:09 +0000 (Wed, 28 Oct 2009) | 4 lines

Backport r993471 by bbigras from trunk to the 4.3 branch:

Fix the bug : Kopete can't fetch the vcard the first time a gtalk contact is added

------------------------------------------------------------------------
r1042519 | rjarosz | 2009-10-30 00:15:33 +0000 (Fri, 30 Oct 2009) | 8 lines

Backport commit 1042448

Ignore index lookup for account internal myself contact because it isn't in contact list.
In rare cases we may get message event for it and we don't want to assert.

CCBUG: 209248


------------------------------------------------------------------------
r1042520 | rjarosz | 2009-10-30 00:16:13 +0000 (Fri, 30 Oct 2009) | 6 lines

Backport commit 1042494.
Fix bug 209671: Kopete crashes on reconnect.
 
CCBUG: 209671


------------------------------------------------------------------------
r1042522 | rjarosz | 2009-10-30 00:18:00 +0000 (Fri, 30 Oct 2009) | 5 lines

Partially backport commit 1042486
Fix bug 212058: Kopete crashes when adding msn contact while offline.
 
CCBUG: 212058

------------------------------------------------------------------------
r1042552 | scripty | 2009-10-30 04:03:55 +0000 (Fri, 30 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
