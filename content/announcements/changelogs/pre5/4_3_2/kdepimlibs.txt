------------------------------------------------------------------------
r1017646 | scripty | 2009-08-31 03:01:47 +0000 (Mon, 31 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019800 | winterz | 2009-09-04 12:17:24 +0000 (Fri, 04 Sep 2009) | 8 lines

Backport r1019799 by winterz from trunk to the 4.3 branch:

Fix "kmail: Kiosk-based [$e] config variables are not evaluated for file emailidentities"
Thanks for the patch Jörg!
CCBUG: 197523
MERGE: e4,4.3


------------------------------------------------------------------------
r1021325 | mkoller | 2009-09-08 21:05:24 +0000 (Tue, 08 Sep 2009) | 7 lines

Backport r1021324 by mkoller from trunk to the 4.3 branch:

CCBUG: 206417

Be more tolerant and decode illegal chars in the given encoding


------------------------------------------------------------------------
r1024435 | tmcguire | 2009-09-16 16:05:31 +0000 (Wed, 16 Sep 2009) | 7 lines

Backport r1024422 by tmcguire from trunk to the 4.3 branch:

Escape URLs when converting to HTML, to prevent text from disappearing.

Add a unit tests for the unit test commit message from David.


------------------------------------------------------------------------
r1024447 | scripty | 2009-09-16 16:13:58 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024464 | winterz | 2009-09-16 16:20:06 +0000 (Wed, 16 Sep 2009) | 22 lines

backport r1024408 | tmcguire | 2009-09-16 10:58:25 -0400 (Wed, 16 Sep 2009) | 20 lines

SVN_MERGE
Merged revisions 1023454 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

................
  r1023454 | winterz | 2009-09-14 21:10:31 +0200 (Mon, 14 Sep 2009) | 12 lines

  Merged revisions 1023445 via svnmerge from
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim

  ........
    r1023445 | winterz | 2009-09-14 14:58:14 -0400 (Mon, 14 Sep 2009) | 5 lines

    when printing month view, only print events on the days specified by the range.
    kolab/issue3683

    MERGE: e4,trunk,4.3
  ........


------------------------------------------------------------------------
r1026636 | scripty | 2009-09-22 03:10:18 +0000 (Tue, 22 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1026793 | winterz | 2009-09-22 16:18:41 +0000 (Tue, 22 Sep 2009) | 8 lines

Backport r1019799 by winterz from trunk to the 4.3 branch:

Fix "kmail: Kiosk-based [$e] config variables are not evaluated for file emailidentities"
Thanks for the patch Jörg!
CCBUG: 197523
MERGE: e4,4.3


------------------------------------------------------------------------
