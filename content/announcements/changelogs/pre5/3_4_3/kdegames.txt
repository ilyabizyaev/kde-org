2005-08-02 12:55 +0000 [r442356]  coolo

	* branches/KDE/3.4/kdegames/libkdegames/configure.in.in: don't
	  break kdegames by default

2005-08-06 17:39 +0000 [r443589]  davec

	* branches/KDE/3.4/kdegames/kshisen/app.cpp: Fix crash on exit
	  (patch by Tommi Rantala) BUG: 94718

2005-08-25 21:21 +0000 [r453331]  hadacek

	* branches/KDE/3.4/kdegames/kmines/solver/adviseFast.cpp,
	  branches/KDE/3.4/kdegames/kmines/status.cpp,
	  branches/KDE/3.4/kdegames/kmines/CHANGELOG,
	  branches/KDE/3.4/kdegames/kmines/version.h: backport fixes for
	  bug #111304 and #100163

2005-09-01 20:29 +0000 [r455994]  hadacek

	* branches/KDE/3.4/kdegames/libksirtet/common/Makefile.am,
	  branches/KDE/3.4/kdegames/ksirtet/ksirtet/Makefile.am,
	  branches/KDE/3.4/kdegames/libksirtet/lib/wizard.cpp: - fix
	  dependencies - "fix" bug #65488 by disabling network games

2005-09-01 20:52 +0000 [r456004]  hadacek

	* branches/KDE/3.4/kdegames/ksirtet/CHANGELOG,
	  branches/KDE/3.4/kdegames/ksirtet/ksirtet/main.cpp: new version +
	  changelog

2005-10-05 13:47 +0000 [r467517]  coolo

	* branches/KDE/3.4/kdegames/kdegames.lsm: 3.4.3

