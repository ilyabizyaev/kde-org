------------------------------------------------------------------------
r1042883 | rjarosz | 2009-10-30 23:28:25 +0000 (Fri, 30 Oct 2009) | 8 lines

Backport commit 1042879.
Never save contact list after protocols were unloaded.

CCBUG: 187311
CCBUG: 206837
CCBUG: 202061


------------------------------------------------------------------------
r1043065 | rjarosz | 2009-10-31 17:26:54 +0000 (Sat, 31 Oct 2009) | 6 lines

Backport commit 1042833.

Fix crash when removing current style.
CCBUG: 199599


------------------------------------------------------------------------
r1043067 | rjarosz | 2009-10-31 17:29:04 +0000 (Sat, 31 Oct 2009) | 5 lines

Backport commit 1042843.
Fix crash when sendMessage or m_activeView is null.
CCBUG: 209994


------------------------------------------------------------------------
r1043069 | rjarosz | 2009-10-31 17:31:59 +0000 (Sat, 31 Oct 2009) | 6 lines

Backport commit 1042888.
Don't crash if source contact index is invalid.

CCBUG: 201832


------------------------------------------------------------------------
r1043070 | rjarosz | 2009-10-31 17:33:17 +0000 (Sat, 31 Oct 2009) | 5 lines

Backport commit 1042968.
Don't show user info twice otherwise we will lose pointer to it.

CCBUG: 212475

------------------------------------------------------------------------
r1043074 | rjarosz | 2009-10-31 17:39:56 +0000 (Sat, 31 Oct 2009) | 6 lines

Backport commit 1043026.
Cancel file transfers on exit or when contact is deleted.

CCBUG: 212055


------------------------------------------------------------------------
r1043399 | lappelhans | 2009-11-01 12:42:26 +0000 (Sun, 01 Nov 2009) | 2 lines

Compile again, use the tag of a stable btcore, not the branch...

------------------------------------------------------------------------
r1043443 | rjarosz | 2009-11-01 15:17:27 +0000 (Sun, 01 Nov 2009) | 6 lines

Backport commit 1043442.
Don't set AIM away message in ICQ because it will break DND and othere statuses.

CCBUG: 198785


------------------------------------------------------------------------
r1043458 | rjarosz | 2009-11-01 15:54:49 +0000 (Sun, 01 Nov 2009) | 6 lines

Backport commit 1043454 + Busy fix
Set correct category for Extended away status

CCBUG: 198785


------------------------------------------------------------------------
r1043547 | rjarosz | 2009-11-01 19:56:09 +0000 (Sun, 01 Nov 2009) | 2 lines

Revert part of last commit, Busy is only in trunk.

------------------------------------------------------------------------
r1043551 | rjarosz | 2009-11-01 20:19:32 +0000 (Sun, 01 Nov 2009) | 4 lines

Add new client to old error code.
CCBUG: 210066


------------------------------------------------------------------------
r1043998 | rjarosz | 2009-11-02 20:05:01 +0000 (Mon, 02 Nov 2009) | 5 lines

Backport commit 1043996.
If client sends invalid/unknown status then assume that the client is online.

CCBUG: 210558

------------------------------------------------------------------------
r1044484 | rjarosz | 2009-11-03 21:47:37 +0000 (Tue, 03 Nov 2009) | 9 lines

Backport commit 1044089.

Don't show "You are not allowed to add yourself to the contact list" after login \
because user can't do anything about it from Kopete. If user tries to add own contact \
30 seconds after login to protocols which doesn't support it or it wasn't implemented \
yet we show the error.

CCBUG: 212704

------------------------------------------------------------------------
r1044534 | rjarosz | 2009-11-03 23:43:34 +0000 (Tue, 03 Nov 2009) | 7 lines

Backport commit 1044526.
Disconnect signals on reset so we don't get signals twice on reconnect.
Check if d->bs is not null.

CCBUG: 188042


------------------------------------------------------------------------
r1044690 | vhaertel | 2009-11-04 13:47:30 +0000 (Wed, 04 Nov 2009) | 12 lines

Backport of chatmemberslistmodel optimization

- Changed the chatmemberlistmodel so that it sorts the contacts by
  onlinestatus-weight/nickname (important for IRC).
- chatmemberlistmodel manages it's own sorted list of contacts
- implemented the "FIXME"s so that the add/remove slots do not call
  "reset()" anymore, which takes a lot of time for big irc channels
- added signal/slot for nickname-change to chatsession (model needs to
  know for resorting)
- skip some unimportant things (which really cost much time ~ 150 ms per
  contact added) in chatwindow for chatrooms with more members 

------------------------------------------------------------------------
r1046123 | uwolfer | 2009-11-07 15:54:00 +0000 (Sat, 07 Nov 2009) | 6 lines

Backport:
SVN commit 1038919 by cfeck:

Compile with latest WebKitKDE

BUG:213243
------------------------------------------------------------------------
r1048523 | bbigras | 2009-11-13 13:40:51 +0000 (Fri, 13 Nov 2009) | 3 lines

Prevent krdc from crashing when clicking on buttons after closing the startpage

BUG: 212976
------------------------------------------------------------------------
r1051243 | scripty | 2009-11-19 04:16:04 +0000 (Thu, 19 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1052214 | scripty | 2009-11-21 04:21:23 +0000 (Sat, 21 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
