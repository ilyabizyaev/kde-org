2008-09-25 20:37 +0000 [r864887]  aacid <aacid@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/shortcuts/kcheckaccelerators.cpp:
	  Make DrKlash work

2008-09-26 16:54 +0000 [r865093]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/directorysizejob.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/jobtest.cpp: backport fix for
	  DirectorySizeJob's subjob being deleted twice.

2008-09-26 18:02 +0000 [r865105]  laidig <laidig@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/widgets/kcharselect.cpp: backport
	  of r865104: fix opening of links when search mode is active

2008-09-26 18:55 +0000 [r865114]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kurlcompletion.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kurlcompletiontest.cpp: Fix
	  completion of file:///foo/bar/subdir/ showing as
	  file:///foo/bar/subdir%2F due to appending '/' after subdir
	  before the QUrl::toPercentEncoding. Quite some code shuffling was
	  necessary to do the percent-encoding before appending '/' for
	  dirs. I think this fixes the problems that r793612 (reverted in
	  r827306) was about. CCMAIL: Jeff Mitchell
	  <kde-dev@emailgoeshere.com>

2008-09-26 20:06 +0000 [r865137]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/docbook/xsl/common/l10n.xsl,
	  branches/KDE/4.1/kdelibs/kdoctools/docbook/xsl/common/en.xml,
	  branches/KDE/4.1/kdelibs/kdoctools/customization/kde-navig.xsl,
	  branches/KDE/4.1/kdelibs/kdoctools/docbook/xsl/common/de.xml:
	  backport of translatable footer to stable translators: To benefit
	  from the attached patch, the you have to copy the 3 new items
	  footer-doc-comment, footer-doc-feedback and footer-doc-teamname
	  from en.xml to your lang.xml and translate the texts If the 3 new
	  items don't exist in lang.xml, you get a lot of compile warnings
	  like 'No de localization of footer-doc-comment exists; using en'
	  and the footer is in english CCMAIL:kde-i18n-doc@kde.org

2008-09-26 21:24 +0000 [r865166-865156]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirmodel.cpp: Backport error
	  message improvement

	* branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.cpp,
	  branches/KDE/4.1/kdelibs/kio/kio/kdirlister.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.h,
	  branches/KDE/4.1/kdelibs/kio/kio/kdirlister_p.h: Backport
	  KDirLister fixes for the case of concurrent dirlisters using the
	  cache.

2008-09-27 13:20 +0000 [r865394]  pino <pino@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirlister.cpp: printDebug only
	  when debugging the cache

2008-09-28 12:49 +0000 [r865637]  laidig <laidig@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/widgets/kcharselect.cpp: backport
	  of r865635: wrap combining diacritical marks in spaces to
	  minimize display errors CCBUG:53083

2008-09-28 16:32 +0000 [r865686]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  Partial port of the entities from 3.5 and 4.0

2008-09-28 16:38 +0000 [r865690]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  Added kdm-narzednik

2008-09-28 20:13 +0000 [r865732]  trueg <trueg@localhost>:

	* branches/KDE/4.1/kdelibs/nepomuk/core/ontology/class.cpp:
	  Backported fixed sub and super class recursion

2008-09-28 21:38 +0000 [r865762]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/editing/jsediting.cpp: Merged
	  revision 865761: Fixed potential out-of-bounds access.

2008-09-28 21:46 +0000 [r865764]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/khtmlview.cpp: Merged revision
	  865763: Fixed position of break statement. Reported by Christop
	  B.

2008-09-28 21:57 +0000 [r865767]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/xml/dom_nodeimpl.cpp: Merged
	  revision 865765: As I understand this code is dead. Still,
	  prevent it from crashing.

2008-09-29 06:37 +0000 [r865820]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kfile/kpropertiesdialogplugin.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-09-29 09:36 +0000 [r865848]  ilic <ilic@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/docbook/xsl/common/sr_latin.xml,
	  branches/KDE/4.1/kdelibs/kdoctools/docbook/xsl/common/sr.xml:
	  Footer strings for sr, sr_latin. (bport: 865847)

2008-09-29 10:30 +0000 [r865857]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/chmodjob.cpp: Same fix as in
	  DirectorySizeJob: remove subjob also on error, to fix crash (e.g.
	  when using NetAccess, like kio_trash does)

2008-09-29 10:42 +0000 [r865861]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/job.cpp: Another instance of
	  dangling subjob.

2008-09-29 13:13 +0000 [r865925-865924]  gateau <gateau@localhost>:

	* branches/KDE/4.1/kdelibs/kfile/kfilefiltercombo.cpp: Fix broken
	  selection of current filter: 'i' could sometimes be set to an
	  incorrect value if the code reached the "continue;" line earlier
	  in the loop. It's simpler and safer to use count(). BUG:171430
	  Backported https://svn.kde.org/home/kde/trunk/KDE/kdelibs@865771

	* branches/KDE/4.1/kdelibs/kfile/kfilewidget.cpp:
	  currentMimeFilter() sometimes returns wrong value This is because
	  d->mimetypes does not always contain the same mimetype list as
	  d->filterWidget. This can happen if a mimetype got filtered out
	  in KFileFilterCombo::setMimeFilter() Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdelibs@865842

2008-09-29 13:53 +0000 [r865935]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/tests/jobtest.cpp,
	  branches/KDE/4.1/kdelibs/kio/kio/deletejob.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/jobtest.h: Backport 865917:
	  crash fix and unittests.

2008-09-29 18:31 +0000 [r865995]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirmodel.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.h: sync with
	  trunk: fix for kio_svn (bug 169346) and for #ref (bug 171117).

2008-10-01 09:44 +0000 [r866550]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/global.cpp: Fix /foo/foo in the
	  error message instead of just /foo, due to wrong KUrl::path()
	  usage. Fix hackish (and no-op due to false instead of true!)
	  detection of "internet protocols"

2008-10-01 09:49 +0000 [r866553]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirmodel.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.cpp,
	  branches/KDE/4.1/kdelibs/kio/kio/kdirlister.cpp,
	  branches/KDE/4.1/kdelibs/kio/kio/job.h,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.h,
	  branches/KDE/4.1/kdelibs/kio/kio/kdirlister_p.h: Backport
	  r866552: fix for bug 160057 (fonts://foo/bar), 53397 (chmod on
	  current dir), and fix unit tests.

2008-10-01 11:58 +0000 [r866591]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/khtml_part.cpp: Backport kDebug
	  fixes (remove method names, remove endl, remove spurious spaces)
	  [so that future backports don't fail due to different contexts].

2008-10-01 14:51 +0000 [r866644]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/cmake/modules/FindStrigi.cmake: sync
	  with the trunk version

2008-10-01 19:06 +0000 [r866719]  abizjak <abizjak@localhost>:

	* branches/KDE/4.1/kdelibs/kdecore/config/kconfig.cpp: backport
	  revision 866718

2008-10-01 19:27 +0000 [r866732]  grossard <grossard@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/docbook/xsl/common/fr.xml:
	  added footer for french docs

2008-10-01 20:20 +0000 [r866750]  grossard <grossard@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/fr/user.entities:
	  added &configurationDuSysteme; entity for &systemsettings;

2008-10-02 09:42 +0000 [r866909]  abizjak <abizjak@localhost>:

	* branches/KDE/4.1/kdelibs/kdecore/config/kconfig.cpp,
	  branches/KDE/4.1/kdelibs/kdecore/config/kconfigini.cpp: backport
	  revision 866908

2008-10-02 14:33 +0000 [r866969]  tmcguire <tmcguire@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/widgets/ktextedit.cpp,
	  branches/KDE/4.1/kdelibs/kdeui/widgets/ktextedit.h: Backport
	  r866967 by tmcguire from trunk to the 4.1 branch: Make the
	  "clear" action undo-aware. PLEASEFIXTHEBUGKEYWORD: 172031

2008-10-02 19:20 +0000 [r867092]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirmodel.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.cpp,
	  branches/KDE/4.1/kdelibs/kio/kio/deletejob.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.h: Backport
	  867090 + 867091 (fix for crash #171721)

2008-10-02 23:42 +0000 [r867166]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/kjs/nodes.cpp: Fix eval-injection of
	  functions into local scope. Fixes the widely used s_code.js
	  statistics(?) script, and potentially tons of other eye-gouging
	  JS. BUG:160466

2008-10-03 00:03 +0000 [r867171]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_image.cpp:
	  Significantly improve the efficiency of how we paint images when
	  full-page scaling, by doing scaling directly with imload. This
	  avoids a lot of expensive operations (a drawPixmap on a scaled
	  QPainter isn't pretty) and lets us take advantage of imload's
	  internal caching infrastructure. Unfortunately, this doesn't help
	  with background images; those also need a lot of thought on how
	  to fully take advantage of imload, including unifying of cache
	  and pre-tile management, etc.

2008-10-03 00:26 +0000 [r867176]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/xml/dom_elementimpl.cpp,
	  branches/KDE/4.1/kdelibs/khtml/ecma/kjs_dom.cpp,
	  branches/KDE/4.1/kdelibs/khtml/html/html_formimpl.cpp,
	  branches/KDE/4.1/kdelibs/khtml/html/html_inlineimpl.h,
	  branches/KDE/4.1/kdelibs/khtml/xml/dom_elementimpl.h,
	  branches/KDE/4.1/kdelibs/khtml/ecma/kjs_dom.h,
	  branches/KDE/4.1/kdelibs/khtml/ecma/kjs_html.cpp,
	  branches/KDE/4.1/kdelibs/khtml/html/html_formimpl.h,
	  branches/KDE/4.1/kdelibs/khtml/ecma/kjs_html.h,
	  branches/KDE/4.1/kdelibs/khtml/html/html_inlineimpl.cpp: Support
	  focus/blur on all elements (likely mozilla extension), used by
	  Youtube quicklist BUG: 169988

2008-10-03 08:34 +0000 [r867254]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/cmake/modules/FindStrigi.cmake: sync
	  with trunk

2008-10-03 11:08 +0000 [r867378]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kurlcompletion.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kurlcompletiontest.cpp:
	  Backport fix for #163098 crash

2008-10-03 16:43 +0000 [r867496]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/jobs/kabstractwidgetjobtracker_p.h:
	  header-protection (for enable-final)

2008-10-03 22:04 +0000 [r867600]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/html/html_canvasimpl.cpp: Merged
	  revision 867598: Save calls to non-inline QImage::width(). About
	  0.25% CPU cycles less
	  http://www.nihilogic.dk/labs/javascript_canvas_fractals/

2008-10-03 22:13 +0000 [r867602]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/nepomuk/core/ontology/ontology.cpp,
	  branches/KDE/4.1/kdelibs/nepomuk/core/ontology/desktopontologyloader.cpp,
	  branches/KDE/4.1/kdelibs/nepomuk/core/ontology/entity.cpp,
	  branches/KDE/4.1/kdelibs/nepomuk/core/ontology/entity_p.h,
	  branches/KDE/4.1/kdelibs/nepomuk/core/ontology/property.cpp,
	  branches/KDE/4.1/kdelibs/nepomuk/core/resourcefiltermodel.cpp,
	  branches/KDE/4.1/kdelibs/nepomuk/core/ontology/class.cpp,
	  branches/KDE/4.1/kdelibs/nepomuk/core/ontology/ontologymanager.cpp,
	  branches/KDE/4.1/kdelibs/nepomuk/core/resourcedata.cpp: Fix
	  compilation with enable-final

2008-10-04 11:17 +0000 [r867730]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/html/html_canvasimpl.h,
	  branches/KDE/4.1/kdelibs/khtml/html/html_canvasimpl.cpp,
	  branches/KDE/4.1/kdelibs/khtml/ecma/kjs_context2d.cpp: Merged
	  revision 867728: Set color components directly on the scan line
	  rather than reading and setting QColor values with pixel() and
	  setPixel(). Saved about 4-6% of cpu cycles on a heavy duty
	  example.

2008-10-04 19:59 +0000 [r867879]  jferrer <jferrer@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/docbook/xsl/common/ca.xml: Add
	  the translatable footer stuff

2008-10-04 22:16 +0000 [r867909]  mpyne <mpyne@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/help.protocol: Backport fix
	  for broken help: kioslave icon to 4.1

2008-10-05 10:10 +0000 [r868031]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_form.cpp: Merged
	  revision 868030: Stop combo boxes not updating themselves anymore
	  after first use.

2008-10-05 21:08 +0000 [r868264]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  Added entry for Kleopatra

2008-10-05 21:35 +0000 [r868286]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/ecma/kjs_css.cpp: Merged revision
	  868284: Readability.

2008-10-06 18:26 +0000 [r868602]  shaforo <shaforo@localhost>:

	* branches/KDE/4.1/kdelibs/solid/solid/backends/hal/halstorageaccess.cpp:
	  backport trunk progress

2008-10-06 19:13 +0000 [r868614]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/ecma/kjs_window.cpp: Make sure
	  that the hash is %-decoded; fixes the W3C slide tool (reported on
	  kfm-devel)

2008-10-06 20:17 +0000 [r868642]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/editing/editing_p.h,
	  branches/KDE/4.1/kdelibs/khtml/khtml_part.cpp: Fix selection
	  granularity not being initialized, at least when one initially
	  started over a URL or image.

2008-10-07 06:13 +0000 [r868783]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdelibs/knotify/tests/knotifytest.notifyrc:
	  SVN_SILENT made messages (.desktop file)

2008-10-08 01:26 +0000 [r869062]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/ecma/kjs_window.cpp: Merged
	  revision 869061: Fake window.scrollbars a bit better. Page
	  checking for a .visible property will always get undefined
	  (false) for now but this is enough to make the tree menu of
	  http://bugs.kde.org/172334 work. Will take care of a better
	  implementation later.

2008-10-08 18:50 +0000 [r869318]  vtokarev <vtokarev@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/ecma/kjs_dom.cpp: backport fix for
	  google maps

2008-10-09 06:33 +0000 [r869473]  mlaurent <mlaurent@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/khtml_settings.cc: Backport: add
	  white list url in good list

2008-10-09 13:21 +0000 [r869615]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kinit/proctitle.cpp: Fix setproctitle
	  corruption (investigated by me, fixed by Dirk)

2008-10-12 16:25 +0000 [r870500]  shaforo <shaforo@localhost>:

	* branches/KDE/4.1/kdelibs/solid/solid/backends/hal/halstorageaccess.cpp:
	  backport patch for BSD mounting

2008-10-12 18:13 +0000 [r870548]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  New entries for Kalarm

2008-10-13 09:10 +0000 [r870759]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/shortcuts/kdedglobalaccel.cpp:
	  Backport mjansen's fix (r866736) for the big kded startup crash
	  in 4.1.2 (bug 171870)

2008-10-13 18:26 +0000 [r871006]  iastrubni <iastrubni@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/util/kwordwrap.cpp: This fixes
	  (the worst of) the problems in week view in KOrganizer (and
	  probably other places). It makes the dimmed characters paint in
	  the correct place, and stops character shuffling. This leaves
	  minor display problems, like alignment and icon placement, which
	  are of much lower priority. Patch by Shai berger. Backport for
	  4.1. BUG: 161181 CCMAIL: Shai Berger <shai@platonix.com>

2008-10-13 20:05 +0000 [r871049]  hasso <hasso@localhost>:

	* branches/KDE/4.1/kdelibs/solid/solid/backends/hal/halstorageaccess.cpp:
	  Compile.

2008-10-13 21:43 +0000 [r871102]  ereslibre <ereslibre@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/dialogs/kedittoolbar.cpp:
	  Backport: Fix the problem of a toolbar being hidden if
	  adding/removing actions on another toolbar. Just have to disable
	  temporarily the auto save setting of the main window while we
	  regenerate the interface. It happens that in the case that auto
	  save is enabled, the main window is listening to appearance
	  changes. After removing and readding all the containers of the
	  window, KToolBar::loadState() was being called (on
	  createContainer()), which was triggering a save on the rc file of
	  the app, saving the settings that XMLGUI has just set, and
	  overriding the real user set ones. This disables this
	  temporarily, and sets auto save again back when the rebuild has
	  finished.

2008-10-14 09:28 +0000 [r871243]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/cmake/modules/FindStrigi.cmake: Backport
	  the better configure check for the strigi api screwup.

2008-10-14 11:01 +0000 [r871259]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirlister.cpp,
	  branches/KDE/4.1/kdelibs/kio/kio/deletejob.cpp,
	  branches/KDE/4.1/kdelibs/kio/kio/kdirlister.h: Backport 871146,
	  fix for 109181 (deleting a symlink to a directory over FTP,
	  deletes the target directory)

2008-10-14 23:08 +0000 [r871465]  ereslibre <ereslibre@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/widgets/kmainwindow.cpp,
	  branches/KDE/4.1/kdelibs/kdeui/widgets/kmainwindow_p.h: Backport:
	  Initial patch by Stefan Becker (stefan.becker nokia com). David
	  Faure gave good input on the process of the patch review. CCBUG:
	  172042

2008-10-18 00:20 +0000 [r872732]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/widgets/ktabwidget.cpp: Backport:
	  Don't open an empty tab when double-clicking in the top part of a
	  khtmlview and the tabbar is hidden in konqueror (!) BUG: 165580

2008-10-18 01:06 +0000 [r872745]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirlister.cpp: Backport: Fix
	  crash when renaming a directory which was listed previously (the
	  KFileItem* became dangling). BUG 172945.

2008-10-18 16:19 +0000 [r873038]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.h,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_form.h,
	  branches/KDE/4.1/kdelibs/khtml/xml/dom_docimpl.cpp: Backport
	  Germain's r865830 which fixed CHANGE_EVENT for lineedits...

2008-10-18 16:24 +0000 [r873040]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/css/css_renderstyledeclarationimpl.cpp:
	  Merged revision 871476: Use computedStyle() without directly
	  asking the render object. There are still some cases where it is
	  a hard requirement.

2008-10-18 16:37 +0000 [r873059]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/html/html_formimpl.cpp,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_form.cpp: Rework
	  how we do onchange for input/checkboxes, unbreaking it and
	  simplifying things. As we only want to fire it in response to
	  user events, this just does it in defaultEventHandler. Fixes
	  kde-look wallaper settings, and along with Germain's previous fix
	  this covers #165607, #170451. This also doesn't suffer from
	  #148118 BUG:148118 BUG:165607 BUG:170451

2008-10-18 17:15 +0000 [r873068]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.1/kdelibs/khtml/css/html4.css: Fixed disabled
	  <button> dispatching events and getting pressed-down. Probably
	  want to tweak the CSS some more to grey out the font, though.
	  BUG:170159

2008-10-18 20:47 +0000 [r873119]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.cpp:
	  Merged revision 873117: As we do our own event transporting we
	  have to do our own bubbling for nested QWidgets embedded as from
	  controls. Fixes the click on the clear button of the the
	  KUrlRequester control used for the file input element.

2008-10-18 22:12 +0000 [r873152]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.cpp:
	  Merged revision 873150: Set keyboard focus on the KLineEdit of
	  file input elements when clicking on it.

2008-10-19 01:26 +0000 [r873196]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.cpp:
	  Merged revision 873194: Deliver key events to a widget's focus
	  widget. This might be different than the main widget in the case
	  of KUrlRequester (<input type="file">) which has an KLineEdit
	  embedded into it. Fixes typing into file upload elements once the
	  completion box pops up. Not sure whether the case of an invisible
	  box is working as it should.

2008-10-19 15:29 +0000 [r873497]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/css/css_renderstyledeclarationimpl.cpp:
	  Merged revision 873496: Fixing width, height and padding unit and
	  values for visible as well as invisible elements. Fixes some
	  jquery test suite failures. Will commit tests to
	  css/computed.html shortly.

2008-10-19 16:16 +0000 [r873514]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/css/css_renderstyledeclarationimpl.cpp:
	  Merged revision 873511: Fixed margin values on invisible
	  elements. Also fixed computed value of percentage value.

2008-10-19 18:48 +0000 [r873587]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/html/html_formimpl.h,
	  branches/KDE/4.1/kdelibs/khtml/xml/dom_docimpl.cpp:
	  DocumentImpl::unsubmittedChanges actually relied on modified
	  things having 'M' at the end of their state serialization, which
	  broke notification of changes when the N/null flag got added. Fix
	  this by adding an appropriate abstraction. BUG:173159

2008-10-19 19:38 +0000 [r873600]  osterfeld <osterfeld@localhost>:

	* branches/KDE/4.1/kdelibs/kate/utils/kateglobal.cpp: backport
	  873599: don't crash when the kate part is deleted while it's
	  config editor is open CCBUG:56828

2008-10-19 20:54 +0000 [r873635]  osterfeld <osterfeld@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/khtmlview.cpp: backport: don't
	  crash when the khtmlpart is deleted while a print dialog is open
	  CCBUG:144940

2008-10-19 21:44 +0000 [r873657]  porten <porten@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.cpp:
	  Merged revision 873656: Track the event target for all buttons.
	  Fixes pasting. Okayed by Germain.

2008-10-19 22:27 +0000 [r873680-873667]  ggarand <ggarand@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/xml/xml_tokenizer.cpp,
	  branches/KDE/4.1/kdelibs/khtml/html/html_imageimpl.cpp,
	  branches/KDE/4.1/kdelibs/khtml/html/html_baseimpl.cpp,
	  branches/KDE/4.1/kdelibs/khtml/xml/dom_selection.cpp: manually
	  merged r865829 address remaining remarks by C.Bartoscheck except
	  the fall through comments in bidi.cpp, which would deserve
	  further investigation.

	* branches/KDE/4.1/kdelibs/khtml/css/parser.y,
	  branches/KDE/4.1/kdelibs/khtml/css/parser.cpp: automatically
	  merged revision 867194: do not skip empty CSS rules - they must
	  appear in the cssRules array. digg.com, for instance, needs this
	  to perform some unspeakable CSS hacks. BUG: 170411, 165734

	* branches/KDE/4.1/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.cpp,
	  branches/KDE/4.1/kdelibs/khtml/khtml_part.cpp,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_block.cpp,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_layer.cpp:
	  automatically merged revision 869761: - fix incomplete repaints
	  happening from time to time when jumping early during page load.
	  (#166413) - various RTL layout application (-reverse cmdline
	  option) fixes . implement RTL scrollbars on CSS containers . fix
	  iframes scrollbars in RTL mode . fix wrong direction when
	  scrolling horizontally (#172258) . fix grey block on left of view
	  (#170679) - avoid smooth scrolling during early stage of layout
	  if the option has the WhenEfficient value BUG: 166413, 172258,
	  170679

	* branches/KDE/4.1/kdelibs/khtml/html/html_baseimpl.h,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_frames.cpp,
	  branches/KDE/4.1/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_frames.h,
	  branches/KDE/4.1/kdelibs/khtml/ecma/kjs_window.cpp,
	  branches/KDE/4.1/kdelibs/khtml/html/html_baseimpl.cpp:
	  automatically merged revision 869762: HTML Frames fixes - make
	  frame resizing dynamic - eliminate most frame flicker by cleaning
	  up RenderWidget's resizing code. - prepare for some more work

	* branches/KDE/4.1/kdelibs/khtml/khtmlview.cpp: automatically
	  merged revision 869763: disabling smooth scroll just for the
	  parsing phase, and not for the entire !complete phase, should be
	  fine.

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.cpp,
	  branches/KDE/4.1/kdelibs/khtml/test_regression.cpp,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.h,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_table.cpp:
	  automatically merged revision 869765: argh. Forgotten files -
	  part of "HTML Frame fixes" commit.

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.cpp,
	  branches/KDE/4.1/kdelibs/khtml/rendering/render_replaced.h:
	  automatically merged revision 871068: fix static positioning of
	  positioned inline replaced elements. BUG: 172487

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_object.cpp:
	  automatically merged revision 873659: fix determination of
	  painted surface for containers with text-only children

	* branches/KDE/4.1/kdelibs/khtml/khtmlview.cpp: automatically
	  merged revision 873660: Partly fix the scroll event dispatching,
	  so that window.onscroll works. There are still some aspects of
	  the dispatching that don't work as they should though. (fix /.'s
	  floating comment box)

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_block.cpp:
	  automatically merged revision 873661: bring our preferred min max
	  width calculation algorithm for floats closer to Gecko 1.9+ and
	  WebCore. We may give up on trying to emulate in-layout
	  calculations as Gecko has now switched to methods independent of
	  layout. As a result the interoperability with Gecko 1.9+ is
	  stunning, but somewhat worse with IE/Opera. Fix e.g
	  extremetech.com articles layout.

2008-10-20 12:48 +0000 [r873976]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/job.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/jobtest.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/jobtest.h: Backport 873972,
	  crashfix for deleting a job before it starts

2008-10-20 18:55 +0000 [r874090-874089]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdelibs/nepomuk/core/ontology/entity.cpp: fix on
	  msvc

	* branches/KDE/4.1/kdelibs/kwallet/backend/kwalletbackend.cc,
	  branches/KDE/4.1/kdelibs/kwallet/backend/CMakeLists.txt: backport
	  r868070 (kwallet windows cryptapi)

2008-10-20 19:00 +0000 [r874092]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdelibs/kdecore/io/kurl.cpp: backport r872668
	  (fix for Bug 156159)

2008-10-20 19:06 +0000 [r874100]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/krun.cpp: backport r869705 (fix
	  running native executables from dolphin a.k.a. bug 158034)

2008-10-20 23:41 +0000 [r874220]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirlister.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirlistertest.cpp: Merge
	  874217 - Don't emit a redirection without reason - this code was
	  meant to fix local urls with a host, only.

2008-10-21 00:21 +0000 [r874228]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.cpp,
	  branches/KDE/4.1/kdelibs/kio/kio/kdirlister.cpp,
	  branches/KDE/4.1/kdelibs/kio/tests/kdirmodeltest.h: Backport SVN
	  commit 874227, fix stale jobs in the jobData map.

2008-10-21 04:34 +0000 [r874269]  ggarand <ggarand@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/render_object.cpp:
	  quiet..

2008-10-21 06:49 +0000 [r874283]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdelibs/kdecore/all_languages.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-21 13:39 +0000 [r874393]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kdecore/localization/kencodingdetector.cpp:
	  Skip '=' after finding it, makes sense indeed. Thanks to
	  Thanomsub Noppaburana for the patch. (BUG 158663)

2008-10-22 10:26 +0000 [r874697]  mueller <mueller@localhost>:

	* branches/KDE/4.1/kdelibs/kde3support/kdeui/k3popupmenu.cpp: fix
	  compile with strict flags operator (Qt 4.5)

2008-10-22 17:32 +0000 [r874881]  uwolfer <uwolfer@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/itemviews/kextendableitemdelegate.cpp,
	  branches/KDE/4.1/kdelibs/kdeui/itemviews/kextendableitemdelegate.h:
	  Backport: SVN commit 858415 by ahartmetz: - Do not treat
	  about-to-be-deleted extenders as still present, hopefully fixing
	  hard to reproduce bugs in KGet. - Properly document that the
	  delegate takes ownership of extenders. CCBUG:170598

2008-10-22 22:03 +0000 [r874965]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/rendering/table_layout.cpp: Fix
	  crash when fixed-layout tables (wow, someone actually uses
	  those?) specify all % width as 0 (affects new ebay myebay
	  version) BUG: 172557

2008-10-24 00:25 +0000 [r875314]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/ecma/kjs_window.cpp: Don't return
	  -1 for location.port when no port is set (regression due to
	  change in KUrl semantics). Fixes recent quotes on google finance
	  page. BUG:172371

2008-10-24 19:16 +0000 [r875541]  woebbe <woebbe@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/itemviews/kextendableitemdelegate.cpp:
	  compile

2008-10-24 20:51 +0000 [r875569]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  narzednik for Konqueror

2008-10-24 22:10 +0000 [r875606]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/xmlgui/kxmlguiwindow.cpp,
	  branches/KDE/4.1/kdelibs/kdeui/tests/kxmlgui_unittest.cpp,
	  branches/KDE/4.1/kdelibs/kdeui/tests/kxmlgui_unittest.h: Backport
	  r875600, fix for <ToolBar hidden="true"> with unit test.

2008-10-24 22:19 +0000 [r875612]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  Added entities needed to generate Userguide

2008-10-24 23:19 +0000 [r875628]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/xmlgui/kxmlguifactory.cpp,
	  branches/KDE/4.1/kdelibs/kdeui/xmlgui/kxmlguiwindow.cpp,
	  branches/KDE/4.1/kdelibs/kdeui/tests/kxmlgui_unittest.cpp,
	  branches/KDE/4.1/kdelibs/kdeui/xmlgui/kxmlguifactory.h,
	  branches/KDE/4.1/kdelibs/kdeui/xmlgui/kxmlguiwindow.h,
	  branches/KDE/4.1/kdelibs/kdeui/dialogs/kedittoolbar.cpp: Backport
	  875625: more central fix for "saving while loading" problems.

2008-10-25 18:11 +0000 [r875819]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/ecma/kjs_html.cpp,
	  branches/KDE/4.1/kdelibs/khtml/ecma/kjs_window.cpp,
	  branches/KDE/4.1/kdelibs/khtml/khtml_part.cpp,
	  branches/KDE/4.1/kdelibs/khtml/ecma/kjs_window.h: Fix how we find
	  existing frames for the purpose of form target= submission. This
	  fixes file upload for drupal 6/jquery.form --- its submit was
	  incorrectly blocked as a popup attempt As we refactored this
	  method, also use it for window.open, fixing us blocking opens to
	  _self along the way. While testing this, I noticed that we can
	  crash konq if we do window.open and one of the frames doesn't yet
	  have a part yet. Added a workaround suggested by dfaure for now,
	  as the proper fix is too risky and should be done on unstable
	  branch only. BUG:172073 BUG:102044

2008-10-26 13:51 +0000 [r876097]  pino <pino@localhost>:

	* branches/KDE/4.1/kdelibs/kdeui/util/kwhatsthismanager.cpp:
	  Backport: give the whatsthis a parent widget so the "clicked"
	  event is sent. CCBUG: 164414

2008-10-26 17:15 +0000 [r876172]  pino <pino@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kfile/kfilemetapreview.cpp:
	  Backport: do not override the audio/video preview widget for
	  mimetypes that have already an image preview. CCBUG: 169716

2008-10-26 17:49 +0000 [r876182]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kfile/kfiledialog.cpp: backport
	  r862361: Fix set mime type filter via KFileDialog ctor on windows
	  CCMAIL: kraplax@mail.ru

2008-10-27 00:13 +0000 [r876287]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/khtml/khtml_part.cpp: Fix signature
	  mis-match here, unbreaks loading of accessibility/user
	  stylesheet, and also crash on exit when one is set. BUG:167268
	  BUG:164796

2008-10-27 12:52 +0000 [r876473]  dfaure <dfaure@localhost>:

	* branches/KDE/4.1/kdelibs/kio/kio/kdirmodel.cpp: Backport r876460,
	  fix for assert when browsing applications:/, BUG 172508

2008-10-27 21:14 +0000 [r876703]  whiting <whiting@localhost>:

	* branches/KDE/4.1/kdelibs/knewstuff/knewstuff2/ui/itemsviewdelegate.cpp:
	  backport of crash fix to 4.1 branch, fixes BUG: 172877

2008-10-27 21:22 +0000 [r876709]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  Entities for kaddressbook

2008-10-28 09:13 +0000 [r876846]  mleupold <mleupold@localhost>:

	* branches/KDE/4.1/kdelibs/kio/misc/kwalletd/kwalletd.cpp:
	  Backport: Eliminate all ways a wallet could possibly be saved
	  after the password has already been removed. BUG:172448

2008-10-28 22:10 +0000 [r877220]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  Sonnet entries (for kmail)

2008-10-28 22:42 +0000 [r877234]  mrybczyn <mrybczyn@localhost>:

	* branches/KDE/4.1/kdelibs/kdoctools/customization/pl/user.entities:
	  Entity DBus (for konsole)

2008-10-29 03:52 +0000 [r877278]  orlovich <orlovich@localhost>:

	* branches/KDE/4.1/kdelibs/kjs/value.h,
	  branches/KDE/4.1/kdelibs/kjs/JSImmediate.h: Fix the
	  immediate-value path of getNumber nor enforcing, uhm, Numberness.
	  Fixes optimized versions of some comparisons confusing things
	  like null with numbers. BUG:173202

2008-10-29 15:03 +0000 [r877445]  abizjak <abizjak@localhost>:

	* branches/KDE/4.1/kdelibs/solid/solid/backends/hal/halstorageaccess.cpp:
	  backport ntfs locale fix from revision 877443

2008-10-30 11:29 +0000 [r877785]  mueller <mueller@localhost>:

	* branches/KDE/4.1/kdelibs/CMakeLists.txt: KDE 4.1.3 preparations

2008-10-30 11:58 +0000 [r877802-877800]  mueller <mueller@localhost>:

	* branches/KDE/4.1/kdelibs/nepomuk/core/resourcefiltermodel.h,
	  branches/KDE/4.1/kdelibs/nepomuk/core/resourcefiltermodel.cpp:
	  fix build with Qt 4.5 snapshots

	* branches/KDE/4.1/kdelibs/khtml/xml/dom_position.cpp,
	  branches/KDE/4.1/kdelibs/khtml/xml/dom_selection.cpp,
	  branches/KDE/4.1/kdelibs/kjs/bytecode/opcodes.cpp.in: various
	  pedantic/Wformat fixes

