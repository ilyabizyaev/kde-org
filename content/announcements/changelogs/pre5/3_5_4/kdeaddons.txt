2006-06-10 15:04 +0000 [r549989]  lueck

	* branches/KDE/3.5/kdeaddons/doc/konq-plugins/babel/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/imgallery/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/xmltools.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/kuick/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/insertcommand.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/konq-plugins/index.docbook,
	  branches/KDE/3.5/kdeaddons/doc/kate-plugins/xmlcheck.docbook:
	  documentation backport from trunk CCMAIL:kde-doc-english@kde.org
	  CCMAIL:kde-i18n-doc@kde.org

2006-06-16 04:43 +0000 [r551943]  binner

	* branches/KDE/3.5/kdeaddons/konq-plugins/searchbar/searchbar.cpp:
	  never show unknown/"broken" icon

2006-06-19 10:03 +0000 [r552855]  binner

	* branches/KDE/3.5/kdeaddons/konq-plugins/searchbar/searchbar.cpp:
	  FEATURE: hint selected search provider (too many Google icons in
	  there ;-)

2006-06-19 11:51 +0000 [r552890]  binner

	* branches/KDE/3.5/kdeaddons/konq-plugins/searchbar/searchbar.cpp:
	  looks better with most resolutions

2006-06-23 13:13 +0000 [r554183]  coolo

	* branches/KDE/3.5/kdeaddons/konq-plugins/imagerotation/jpegorient,
	  branches/KDE/3.5/kdeaddons/konq-plugins/imagerotation/Makefile.am:
	  take internal tools out of the $PATH (exif.py is pretty old, but
	  as we only use it to get orientation we don't have to show
	  everyone)

2006-06-25 12:30 +0000 [r554811]  mlaurent

	* branches/KDE/3.5/kdeaddons/konq-plugins/webarchiver/archivedialog.cpp:
	  Add missing i18n

2006-07-11 15:17 +0000 [r560971]  kling

	* branches/KDE/3.5/kdeaddons/kate/tabbarextension/plugin_katetabbarextension.cpp:
	  Removing an unneeded workaround from r451090 that isn't needed
	  anymore AFAICT, and only causes more trouble than good. CCBUG:
	  110642

2006-07-23 14:01 +0000 [r565463]  coolo

	* branches/KDE/3.5/kdeaddons/kdeaddons.lsm: preparing KDE 3.5.4

