---
aliases:
- /announcements/plasma-5.16.0-5.16.1-changelog
hidden: true
plasma: true
title: Plasma 5.16.1 Complete Changelog
type: fulllog
version: 5.16.1
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- Re-read color palettes when application color changes. <a href='https://commits.kde.org/breeze/9d6c7c7f3439941a6870d6537645297683501bb0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408416'>#408416</a>. See bug <a href='https://bugs.kde.org/382505'>#382505</a>. See bug <a href='https://bugs.kde.org/355295'>#355295</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21646'>D21646</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Flatpak: Indicate that updates are being fetched. <a href='https://commits.kde.org/discover/67b313bdd6472e79e3d500f8b32d0451c236ce84'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408608'>#408608</a>
- --verbose. <a href='https://commits.kde.org/discover/2c8eaf6b30b2d5cf0870cd61dfc6e78df039ef50'>Commit.</a>
- Kns (mostly): Don't show weird padding if the thumbnails are too small. <a href='https://commits.kde.org/discover/cf8606eb89f70011edc170a5a2ec5a699933cfcf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408109'>#408109</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a>

- Move various properties away from qapp and to kaboutdata. <a href='https://commits.kde.org/drkonqi/b428aa049ee62481790e4da4961ea59933d8e601'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21802'>D21802</a>
- Actually set kaboutdata as application aboutdata. <a href='https://commits.kde.org/drkonqi/bd205d4d2b75164794de834eab38ab7b71377b66'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383842'>#383842</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21799'>D21799</a>
- The Developer Information tab now correctly resizes to fit the content. <a href='https://commits.kde.org/drkonqi/bf5d10ab09461516cd5230c6dc12ec67e074c15d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406748'>#406748</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21642'>D21642</a>
- Saving backtraces now overwrites files when necessary. <a href='https://commits.kde.org/drkonqi/0d35055d867acd8c2a7db4d13cf9bd13d18fe558'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405597'>#405597</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21645'>D21645</a>
- Error message when failing to save backtraces are more helpful. <a href='https://commits.kde.org/drkonqi/c8f8b9d962eb7c0b638fbe6ded725a46cb5bbf84'>Commit.</a> See bug <a href='https://bugs.kde.org/405597'>#405597</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21644'>D21644</a>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

- Update default Samba log path. <a href='https://commits.kde.org/kinfocenter/6243a402c926ad92f641a1c03f68037f48024f18'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21552'>D21552</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Really use the translations. Patch by Victor Ryzhykh <victorr2007@yandex.ru>. <a href='https://commits.kde.org/kwin/b24aa90af7fb5f8c71035dc368734325377715fb'>Commit.</a>

### <a name='milou' href='https://commits.kde.org/milou'>Milou</a>

- Enforce use of the reset timer for clearing the model. <a href='https://commits.kde.org/milou/9d5921fb21c7aa4d1cd81255bbebf3fd4c7161af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21670'>D21670</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [desktoppackage] Re-add spacing between inline message and first UI element. <a href='https://commits.kde.org/plasma-desktop/c11527c04b1ae96389667e7cc4f404efb955c6c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408712'>#408712</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21810'>D21810</a>
- Doc: Write docbook date in standard format. <a href='https://commits.kde.org/plasma-desktop/5d0ba9313167276d76afd88e88bb7daa1fa9260a'>Commit.</a>
- Replace the excludeRange mode setting when already available. <a href='https://commits.kde.org/plasma-desktop/08ec9a036bf5b3ee102938ad332f624adc1060b9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/244857'>#244857</a>. Fixes bug <a href='https://bugs.kde.org/408415'>#408415</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21748'>D21748</a>
- [minimzeall] Change panel icon size to smallMedium. <a href='https://commits.kde.org/plasma-desktop/fff556af1939d17453540f4388b27650359258dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21685'>D21685</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Handle Esc properly when focus is in searchbox. <a href='https://commits.kde.org/plasma-nm/5b45ac0edc140518769eda0b7a7ff59b936a5027'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408290'>#408290</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21620'>D21620</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [Powerdevil runner] Make Sleep/Suspend command work again. <a href='https://commits.kde.org/plasma-workspace/6a5a38f1281f630edc8fda18523fe9dceef22377'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408735'>#408735</a>
- PanelView: align setting of masks with how it's done for dialogs/tooltips. <a href='https://commits.kde.org/plasma-workspace/f65a0eee09dabc66d9d7acf6ddda6bcb03888794'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21803'>D21803</a>
- [sddm-theme] Style the session and keyboard layout selectors to be more Breeze. <a href='https://commits.kde.org/plasma-workspace/16d3eb60a9849151aa92c2a100872ab12bb3855d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408702'>#408702</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21794'>D21794</a>
- Improved notification identification for Flatpak applications. <a href='https://commits.kde.org/plasma-workspace/9258ef586324a74d5333faa1429f6a2a80cafadc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21779'>D21779</a>

### <a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a>

- Ignore power management inhibition in battery critical timeout. <a href='https://commits.kde.org/powerdevil/82db1f446966a4a66324befbc60e6a0c0b7735f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408398'>#408398</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21648'>D21648</a>