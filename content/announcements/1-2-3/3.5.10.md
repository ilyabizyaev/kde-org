---
aliases:
- ../announce-3.5.10
date: '2008-08-26'
description: KDE Community Ships Tenth Maintenance Update for Fourth Major Version
  for Leading Free Software Desktop.
title: KDE 3.5.10 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE 3.5.10 Updates Kicker and KPDF
</h3>

<p align="justify">
  <strong>
    KDE 3.5.10 features translations in 65 languages, improvements to KPDF and Kicker, the 
    KDE3 Panel.
  </strong>
</p>

<p align="justify">
  The <a href="/">KDE
  Community</a> today announced the immediate availability of KDE 3.5.10,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. 
  KDE 3.5.10 sports changes in Kicker, the KDE3 panel and KPDF, the PDF viewer.
</p>

<p align="justify">
The KDE community has finalized yet another update to the 3.5 series. While not 
a very exciting release in terms of features, 3.5.10 brings a couple of nice 
bugfixes and translation updates to those who choose to stay with KDE 3.5. The 
<a href="/announcements/changelogs/changelog3_5_9to3_5_10">fixes</a> 
are thinly spread across KPDF with a number of crash fixes, KGPG and probably most 
interesting various fixes in kicker, KDE3's panel:

<ul>
  <li>Improved visibility on transparent backgrounds</li>
  <li>Themed arrow buttons in applets that were missing them</li>
  <li>Layout and antialiasing fixes in various applets</li>
</ul>

Note, as with every release, the changelog is not complete as our developers often forget to document their work. For users of KDE 3.5.9 it should be low-risk to upgrade to KDE 3.5.10 since the rules of what is to enter the KDE 3.5 branch are pretty strict at that point, it's in maintenance only mode.

</p>

<p align="justify">
  For a more detailed list of improvements since the
  <a href="/announcements/announce-3.5.9">KDE 3.5.9 release</a>
  on the 19th February 2008, please refer to the
  <a href="/announcements/changelogs/changelog3_5_9to3_5_10">KDE 3.5.10 Changelog</a>.
</p>

<p align="justify">
  KDE 3.5.10 ships with a basic desktop and fifteen other packages (PIM,
  administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more). KDE's award-winning tools and
  applications are available in <strong>65 languages</strong>.
</p>

<h4>
  Distributions shipping KDE
</h4>
<p align="justify">
  Most of the Linux distributions and UNIX operating systems do not immediately
  incorporate new KDE releases, but they will integrate KDE 3.5.10 packages in
  their next releases. Check 
  <a href="/distributions">this list</a> to see
  which distributions are shipping KDE.
</p>

<h4>
  Installing KDE 3.5.10 Binary Packages
</h4>
<p align="justify">
  <em>Package Creators</em>.
  Some operating system vendors have kindly provided binary packages of
  KDE 3.5.10 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  download server at
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.10/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/1-2-3/3.5.10">KDE 3.5.10 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.5.10
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.5.10 may be
  <a href="http://download.kde.org/stable/3.5.10/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.5.10
  are available from the <a href="/info/1-2-3/3.5.10">KDE
  3.5.10 Info Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> 
project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether it's
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>
