---
title: "Paypal Donations"
include_html: /community/donations/previousdonations.php
sassFiles:
- scss/previousdonations.scss
cdnJsFiles:
- https://cdn.kde.org/aether-devel/charjs.js
---

The following contributions have been generously made through PayPal to KDE using the <a href="../">donation</a> form. If you want to contribute in other ways, look at the overview page to see all the ways you can <a href="../">contribute</a>. We thank the donors listed below for their support!

Note this list does not include campaign specific donations like:

- <a href="/fundraisers/randameetings2014/">Randa Meetings 2014 Fundraising</a>
- <a href="/fundraisers/yearend2014/">KDE End of Year 2014 Fundraising</a>
- <a href="/fundraisers/kdesprints2015/">KDE Sprints 2015 Fundraising</a>
- <a href="/fundraisers/yearend2016/">KDE End of Year 2016 Fundraising</a>

Note: donations that have been made in the past in US$ are converted to Euro on Aug 8th 2011 with a current exchange rate of 1 US$ = 0.7002 Euro. The original amount is listed.
