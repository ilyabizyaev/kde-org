---
version: "5.17.2"
title: "KDE Plasma 5.17.2, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.17_Errata
    name: 5.17 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and other essential software for your computer.  Details in the [Plasma 5.17.2 announcement](/announcements/plasma-5.17.2).
