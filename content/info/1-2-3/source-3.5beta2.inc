<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/arts-1.4.92.tar.bz2">arts-1.4.92</a></td>
   <td align="right">928kB</td>
   <td><tt>e5ff7e478717ff9bb6174b2aa5dc6c2c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdeaccessibility-3.4.92.tar.bz2">kdeaccessibility-3.4.92</a></td>
   <td align="right">8.0MB</td>
   <td><tt>1f849f0b7472e23d3391701d03f383c4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdeaddons-3.4.92.tar.bz2">kdeaddons-3.4.92</a></td>
   <td align="right">1.5MB</td>
   <td><tt>bd445cb56cacd86f3e0628ca104902cb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdeadmin-3.4.92.tar.bz2">kdeadmin-3.4.92</a></td>
   <td align="right">2.0MB</td>
   <td><tt>2c8c8b0b4ea7f5eb29013f110f6460ff</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdeartwork-3.4.92.tar.bz2">kdeartwork-3.4.92</a></td>
   <td align="right">15MB</td>
   <td><tt>30aeba4fdfda31f1835f7be2e9771d6b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdebase-3.4.92.tar.bz2">kdebase-3.4.92</a></td>
   <td align="right">22MB</td>
   <td><tt>6d3fc9318786ecf38ac030ad1a9ed755</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdebindings-3.4.92.tar.bz2">kdebindings-3.4.92</a></td>
   <td align="right">6.8MB</td>
   <td><tt>cc2c4d8865f3c325bce1e5271d5a82d9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdeedu-3.4.92.tar.bz2">kdeedu-3.4.92</a></td>
   <td align="right">28MB</td>
   <td><tt>b07fdaab5850e08b9147b414c06811ea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdegames-3.4.92.tar.bz2">kdegames-3.4.92</a></td>
   <td align="right">9.8MB</td>
   <td><tt>5b71cca626e87b14d208e4c493c85205</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdegraphics-3.4.92.tar.bz2">kdegraphics-3.4.92</a></td>
   <td align="right">6.7MB</td>
   <td><tt>428599056f074fbb3bfa47907c97e390</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdelibs-3.4.92.tar.bz2">kdelibs-3.4.92</a></td>
   <td align="right">14MB</td>
   <td><tt>03605259028c1779a7ab7b7fe4f75514</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdemultimedia-3.4.92.tar.bz2">kdemultimedia-3.4.92</a></td>
   <td align="right">5.2MB</td>
   <td><tt>a0c0f8986b42993b405e35b9a4a59977</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdenetwork-3.4.92.tar.bz2">kdenetwork-3.4.92</a></td>
   <td align="right">7.0MB</td>
   <td><tt>db5718db87e358e484bd467786a1f831</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdepim-3.4.92.tar.bz2">kdepim-3.4.92</a></td>
   <td align="right">12MB</td>
   <td><tt>8c85a282622264607bd8248d42c840bf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdesdk-3.4.92.tar.bz2">kdesdk-3.4.92</a></td>
   <td align="right">4.6MB</td>
   <td><tt>4d4f0b96fd1ab8fda4c5e4e73b80b79b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdetoys-3.4.92.tar.bz2">kdetoys-3.4.92</a></td>
   <td align="right">3.0MB</td>
   <td><tt>4f00858b1054cb707d7a1105a9bb925f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdeutils-3.4.92.tar.bz2">kdeutils-3.4.92</a></td>
   <td align="right">2.8MB</td>
   <td><tt>1466d4087ab31947d6cfe5c3ef4ba02c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdevelop-3.2.92.tar.bz2">kdevelop-3.2.92</a></td>
   <td align="right">7.7MB</td>
   <td><tt>88c471e8b86b275f949d0d4eee055f91</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.5-beta2/src/kdewebdev-3.4.92.tar.bz2">kdewebdev-3.4.92</a></td>
   <td align="right">5.7MB</td>
   <td><tt>f2757ef4c5db23f1c879297c2136ec22</tt></td>
</tr>

</table>
