---
version: "5.5.4"
title: "Plasma 5.5.4 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.5_Errata
    name: 5.5 Errata
type: info/plasma5
---

This is a bugfix release of Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the <a
href="/announcements/plasma-5.5.4">Plasma 5.5.4 announcement</a>.
